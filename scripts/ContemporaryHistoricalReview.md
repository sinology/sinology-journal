# Scripts For Contemporary Historical Review

**當代史學** (Contemporary Historical Review) 是由 [香港浸會大學][hkbu] [歷史系](http://histweb.hkbu.edu.hk) 主辦的學術期刊 (1998.09~2012.09)，共11卷43期。

官方網站主頁 [Contemporary Historical Review 《當代史學》](http://histweb.hkbu.edu.hk/contemporary/contem.html)。

數據展示見 [Contemporary Historical Review](/journals/ContemporaryHistoricalReview.md)。

## Script
### Contemporary Historical Review

```bash
download_tool='wget -qO-' # curl -fsSL
target_url='http://histweb.hkbu.edu.hk/contemporary/contem.html'

$download_tool "${target_url}" | sed -r -n '/<td[^>]*>/,/<\/td>/{s@^[[:space:]]*@@g;p}' | sed ':a;N;$!ba;s@\n@@g;' | sed -r -n 's@<br[^>]*>@\n@g;s@<\/a>@&\n@g;p' | sed -r -n '/Vol\./{s@.*href="([^"]+)"[^>]*>[[:space:]]*.*(Vol.*)$@'"${target_url%/*}"'/\1|\2@g;s@[[:space:]]*<[^>]*>[[:space:]]*@@g;s@[[:space:]]+([[:digit:]]+)[[:punct:][:space:]]+[[:space:]]*@\1|@g;p}' | sort -t\| -k2,2n -k3,3n | awk -F\| 'BEGIN{printf("Vol|No. (Date)|HTML|PDF\n---|---|---|---\n")}{if($1~/.pdf$/){printf("%s|%s (%s)||[PDF](%s)\n",$2,$3,$4,$1)}else{printf("%s|%s (%s)|[HTML](%s)|\n",$2,$3,$4,$1)}}'
```

###　Journal of the History of Christianity in Modern China

```bash
download_tool='wget -qO-' # curl -fsSL
target_url='https://library.hkbu.edu.hk/sca/ahc_ref.html'

$download_tool "${target_url}" | sed -r -n '/href=.*Vol/{s@\|@\n@g;p}' | sed -r -n '/Vol/{s@.*href="([^"]+)">([^<]+)<.*$@* [\2]('"${target_url%/*}"'/\1)@g;p}'
```



## Change Log
* Mar 09, 2019 15:55 Sat -0500 EST
    * 初稿

[hkbu]:https://www.hkbu.edu.hk

<!-- End -->
