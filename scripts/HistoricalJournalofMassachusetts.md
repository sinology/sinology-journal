# Scripts For Historical Journal of Massachusetts

數據展示見 [Historical Journal of Massachusetts](/journals/HistoricalJournalofMassachusetts.md)。

## TOC
1. [Script](#script)  
1.1 [Article Archives](#article-archives)  
1.2 [questia](#questia)  
2. [Change Log](#change-log)  


## Script
### Article Archives
頁面 [Article Archives](http://www.westfield.ma.edu/historical-journal/article-index-1976-2013/) 提供PDF格式文件下載。


```bash
download_tool='wget -qO-' # curl -fsSL
target_url='http://www.westfield.ma.edu/historical-journal/article-index-1976-2013/'

$download_tool "${target_url}" | sed ':a;N;$!ba;s@<br\/>\n@ @g' | sed -r -n  '/class="vc_tta-panels-container"/,/<\/main>/{s@<\/?em>@@g;s@\&amp;@\&@g;/vc_custom_heading/{s@.*vc_custom_heading@<@g;s@[[:space:]]*<[^>]*>[[:space:]]*@---@g;s@(2018, Vol. )45@\146@g;p};/class="wpb_wrapper"/,/<\/div>/{s@<\/?strong>@@g;/<p>/{s@<\/a><a[^>]*>@@g;/href=/{s@.*href="([^"]+)">([^<]*).*$@\1|\2@g;};/<p>/{s@[[:space:]]*<[^>]*>[[:space:]]*@@g;};s@\&#8243;@\&#8221;@g;s@&#8220;@“@g;s@&#8221;@”@g;s@&#8216;@‘@g;s@&#8217;@’@g;s@&#8211;@–@g;s@Murder by@Murderby@g;s@[[:space:]]+by[[:space:]]+@|@g;s@Murderby@Murder by@g;/(^|\|)“/{s@[“”]@@g};/”/{/“/!s@”@@g};s@[“”]@\"@g;p}}}' | awk -F\| '{
    if($0~/^---/){gsub("---","",$0);aa=$0}else{
        if($0~/No issue published/){
            printf("%s||%s|\n",aa,$1)
        }else if($0~/Book Reviews/){
            if($0!~/https?:/){printf("%s||%s|\n",aa,$1)}
        }else{
            if($0~/https?:/){
                printf("%s|%s|%s|%s\n",aa,$1,$2,$3)
            }else{
                printf("%s||%s|%s\n",aa,$1,$2)
            }
        }
    }
}' | awk -F\| 'BEGIN{printf("Date|Title|Author|PDF\n---|---|---|---\n")}{
    if($2!=""){
        gsub("\\(","%28",$2)
        gsub("\\)","%29",$2)
        printf("%s|%s|%s|[PDF](%s)\n",$1,$3,$4,$2)
    }else{
        printf("%s|%s|%s|\n",$1,$3,$4)
    }
}'
```


### questia
[Questia][questia] 提供 [Historical Journal of Massachusetts](https://www.questia.com/library/p62471/historical-journal-of-massachusetts)自1998年起的文章目錄。

```bash
download_tool='wget -qO-' # curl -fsSL

questia_site='https://www.questia.com'
target_url="${questia_site}/library/p62471/historical-journal-of-massachusetts"

vol_no_list=$($download_tool "${target_url}" | sed -r -n '/<article>/,/\/article/{p}' | sed ':a;N;$!ba;s@\n@@g' | sed 's@<\/li>@&\n@g;' | sed -r -n '/<li>/{s@.*href="([^"]+)"[^>]*>[[:space:]]*@'"${questia_site}"'\1|@g;s@[[:space:]]*<[^>]*>[[:space:]]*@@g;s@(No. [[:digit:]]+).*@\1@g;p}')
# https://www.questia.com/library/p62471/historical-journal-of-massachusetts/i4425735/vol-47-no-1-winter|Vol. 47, No. 1

save_path=$(mktemp -t XXXXXX)
> "${save_path}"

echo "${vol_no_list}" | while IFS="|" read -r link vol;do
    $download_tool "${link}" | sed -r -n '/class="li-title"/{s@.*href="([^"]+)"[^>]*>[[:space:]]*([^<]*)<.*$@'"${vol}"'|'"${questia_site}"'\1|\2@g;s@&quot;@"@g;s@&amp;@\&@g;s@&#39;@'\''@g;s@/library/journal/@/read/@g;p}' >> "${save_path}"
    # https://www.questia.com/read/1P4-2166309165/american-tempest-how-the-boston-tea-party-sparked|American Tempest: How the Boston Tea Party Sparked a Revolution
    # sleep 1
done

# output format
awk -F\| 'BEGIN{printf("Vol|No.|Title\n---|---|---\n")}{gsub(",","|",$1);printf("%s|[%s](%s)\n",$1,$3,$2)}' "${save_path}"
```


## Change Log
* Mar 09, 2019 14:47 Sat -0500 EST
    * 初稿

[historical-journal]:https://www.westfield.ma.edu/historical-journal/
[questia]:https://www.questia.com "Online Research Library: Questia"

<!-- End -->
