# Scripts For The Sinological Research Society Of East Asia

[東亞漢學研究學會][dongyahanxue] 由 [長崎大學][nagasaki]刊發，官方頁面見 [YANG Xiaoan(楊　暁安)](http://xiaoan.web.fc2.com/)。

數據展示見 [The Sinological Research Society Of East Asia](/journals/TheSinologicalResearchSocietyOfEastAsia.md)。


## TOC
1. [Script](#script)  
1.1 [Article List](#article-list)  
2. [Change Log](#change-log)  


## Script
### Article List
刊物[列表頁](http://xiaoan.web.fc2.com/dongyahanxue/paper/menu.html)


```bash
download_tool='wget -qO-' # curl -fsL
target_url='http://xiaoan.web.fc2.com/dongyahanxue/paper/menu.html'

# menu list pdf format
$download_tool "${target_url}" | sed -r -n '/href=/{s@.*href="([^"]+)".*$@'"${target_url%/*}"'/\1@g;s@.html$@.pdf@g;p}' > /tmp/bbb

tee /tmp/aaa 1> /dev/null <<EOF
回顧與展望|2010年7月|長崎
創刊號|2011年6月|西安
第2號|2012年4月|臺北
特別號|2013年3月|西安
第3號|2013年9月|廈門
第4號|2014年5月|北京
特別號|2014年12月|東京
第5號|2015年5月|澳門
特別號|2016年2月|京都
第6號|2016年4月|臺灣淡江
特別號|2017年2月|長崎
第7號|2017年4月|臺灣臺中
第8號|2018年9月|日本札幌
特別號|2018年12月|韓國全州
特別號|2019年06月|中國桂林
第9號|2019年11月|日本名古屋
EOF

# paste -d \| /tmp/aaa /tmp/bbb
paste -d \| /tmp/aaa /tmp/bbb | sed '=' | sed 'N;s@\n@|@g'

# 1|回顧與展望|2010年7月|長崎|http://xiaoan.web.fc2.com/dongyahanxue/paper/no1/papers/index.pdf
# 2|創刊號|2011年6月|西安|http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/index.pdf
# 3|第2號|2012年4月|臺北|http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/index.pdf
# 4|特別號|2013年3月|西安|http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/index.pdf
# 5|第3號|2013年9月|廈門|http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/indexs.pdf
# 6|第4號|2014年5月|北京|http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/index.pdf
# 7|特別號|2014年12月|東京|http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/index.pdf
# 8|第5號|2015年5月|澳門|http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/index.pdf
# 9|特別號|2016年2月|京都|http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/menu.pdf
# 10|第6號|2016年4月|臺灣淡江|http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/menu.pdf
# 11|特別號|2017年2月|長崎|http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/menu.pdf
# 12|第7號|2017年4月|臺灣臺中|http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/menu.pdf
# 13|第8號|2018年9月|日本札幌|http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/menu.pdf
# 14|特別號|2018年12月|韓國全州|http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/index.pdf
# 15|特別號|2019年06月|中國桂林|http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/index.pdf
# 16|第9號|2019年11月|日本名古屋|http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/menu.pdf


# 下載各期目錄
paste -d \| /tmp/aaa /tmp/bbb | sed '=' | sed 'N;s@\n@|@g' | cut -d\| -f1,2,5 | while IFS="|" read -r no title url;do $download_tool "$url" > "${no}-${title}.${url##*/}"; sleep 1; done


# 複製各期目錄pdf文件提取目錄信息到文件 /tmp/menu 需手動調整不合規的條目
menu_url='http://xiaoan.web.fc2.com/dongyahanxue/paper/no1/papers/index.pdf'

cat /tmp/menu | sed -r -n '/^・/d;s@[[:space:]]*(…|\.)+[[:space:]]*@|@g;s@\|{2,}@\|@g;p' | awk -F\| '{if($0~/[[:digit:]iⅲ]+$/){print aa$0;aa=""}else{aa=aa$0}}' | sed -r -n 's@\|?[[:space:]]*([[:digit:]i]+)$@|\1@g;p' | awk -F\| '{if(NF==2){printf("%s||%s\n",$1,$NF)}else{print}}' | sed '=' | sed 'N;s@\n@|@g' | while IFS="|" read -r no title author page;do
    echo "[${title}](${menu_url%/*}/${no}.${menu_url##*.})|${author}|${page}"
    # echo "${title}|${author}|[${page}](${menu_url%/*}/${no}.${menu_url##*.})"
done
```


## Change Log
* Oct 15, 2019 22:03 Tue -0400 EST
    * 初稿，添加腳本說明


[nagasaki]: http://www.nagasaki-u.ac.jp "長崎大学トップページ"
[dongyahanxue]:http://xiaoan.web.fc2.com/dongyahanxue/dyhx.html "東亞漢學研究學會"

<!-- End -->
