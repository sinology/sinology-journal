# The Sinological Research Society Of East Asia

[東亞漢學研究學會][dongyahanxue] 由 [長崎大學][nagasaki]刊發，官方頁面見 [YANG Xiaoan(楊　暁安)](http://xiaoan.web.fc2.com/)。

數據提取腳本見 [Sehll Script](/scripts/TheSinologicalResearchSocietyOfEastAsia.md)。


## TOC
1. [Menu List](#menu-list)  
2. [Journal](#journal)  
2.1 [回顧與展望 2010年7月](#回顧與展望-2010年7月)  
2.2 [創刊號 2011年6月](#創刊號-2011年6月)  
2.3 [第2號 2012年4月](#第2號-2012年4月)  
2.4 [特別號 2013年3月](#特別號-2013年3月)  
2.5 [第3號 2013年9月](#第3號-2013年9月)  
2.6 [第4號 2014年5月](#第4號-2014年5月)  
2.7 [特別號 2014年12月](#特別號-2014年12月)  
2.8 [第5號 2015年5月](#第5號-2015年5月)  
2.9 [特別號 2016年2月](#特別號-2016年2月)  
2.10 [第6號 2016年4月](#第6號-2016年4月)  
2.11 [特別號 2017年2月](#特別號-2017年2月)  
2.12 [第7號 2017年4月](#第7號-2017年4月)  
2.13 [第8號 2018年9月](#第8號-2018年9月)  
2.14 [特別號 2018年12月](#特別號-2018年12月)  
2.15 [特別號 2019年06月](#特別號-2019年06月)  
2.16 [第9號 2019年11月](#第9號-2019年11月)  


## Menu List
刊物[列表頁](http://xiaoan.web.fc2.com/dongyahanxue/paper/menu.html)

刊號|日期|地點
---|---|---
回顧與展望|2010年7月|長崎
創刊號|2011年6月|西安
第2號|2012年4月|臺北
特別號|2013年3月|西安
第3號|2013年9月|廈門
第4號|2014年5月|北京
特別號|2014年12月|東京
第5號|2015年5月|澳門
特別號|2016年2月|京都
第6號|2016年4月|臺灣淡江
特別號|2017年2月|長崎
第7號|2017年4月|臺灣臺中
第8號|2018年9月|日本札幌
特別號|2018年12月|韓國全州
特別號|2019年06月|中國桂林
第9號|2019年11月|日本名古屋


## Journal
### 回顧與展望 2010年7月
[回顧與展望 2010年7月 長崎](http://xiaoan.web.fc2.com/dongyahanxue/paper/no1/papers/index.pdf)

Title|Author|Page
---|---|---
[發刊辭](http://xiaoan.web.fc2.com/dongyahanxue/paper/no1/papers/1.pdf)|連清吉|1
[論朱子思想的定位問題－以陳榮捷、牟宗三為中心－](http://xiaoan.web.fc2.com/dongyahanxue/paper/no1/papers/2.pdf)|高柏園|2
[秦愚黔首非本於老子](http://xiaoan.web.fc2.com/dongyahanxue/paper/no1/papers/3.pdf)|江淑君|21
[吉藏對《法華經》聲聞授記的詮釋](http://xiaoan.web.fc2.com/dongyahanxue/paper/no1/papers/4.pdf)|李幸玲|41
[基督教的中国化](http://xiaoan.web.fc2.com/dongyahanxue/paper/no1/papers/5.pdf)|李貴森|58
[林中路：《四庫全書總目》文學思想研究芻議](http://xiaoan.web.fc2.com/dongyahanxue/paper/no1/papers/6.pdf)|曽守正|74
[四書｢官學化｣進程：《四書大全》纂修及其體例](http://xiaoan.web.fc2.com/dongyahanxue/paper/no1/papers/7.pdf)|陳逢源|87
[論賈寶玉的雙重異化](http://xiaoan.web.fc2.com/dongyahanxue/paper/no1/papers/8.pdf)|賈三強|103
[中国家族文化与紅楼夢之家族叙事](http://xiaoan.web.fc2.com/dongyahanxue/paper/no1/papers/9.pdf)|王建科|119
[清官難逃猾吏手－《杜騙新書》中的衙役故事探析](http://xiaoan.web.fc2.com/dongyahanxue/paper/no1/papers/10.pdf)|馮藝超|133
[晚清小說《新中國未來記》、《新石頭記》的大旅行敘事與新中國想像](http://xiaoan.web.fc2.com/dongyahanxue/paper/no1/papers/11.pdf)|張恵珍|145
[多幅面孔的魯迅](http://xiaoan.web.fc2.com/dongyahanxue/paper/no1/papers/12.pdf)|劉應爭|167
[中國現當代作家論書法文化](http://xiaoan.web.fc2.com/dongyahanxue/paper/no1/papers/13.pdf)|李継凱|173
[戰前留日中・韓・台學生之唯美主義文學活動考](http://xiaoan.web.fc2.com/dongyahanxue/paper/no1/papers/14.pdf)|周國強|191
[楚地出土材料中的紀年](http://xiaoan.web.fc2.com/dongyahanxue/paper/no1/papers/15.pdf)|張連航|207
[中国歴史上的文化大變革](http://xiaoan.web.fc2.com/dongyahanxue/paper/no1/papers/16.pdf)|王 暉|219
[太平天國敘事中的親歷文本](http://xiaoan.web.fc2.com/dongyahanxue/paper/no1/papers/17.pdf)|高桂恵|234
[近代華北村政實態分析](http://xiaoan.web.fc2.com/dongyahanxue/paper/no1/papers/18.pdf)|祁建民|247
[“公共領域＂、紳商社會與國家性－兼談現代中國城市文學研究的本土化－](http://xiaoan.web.fc2.com/dongyahanxue/paper/no1/papers/19.pdf)|張鴻聲|260
[國際漢字規範化的艱難歷程](http://xiaoan.web.fc2.com/dongyahanxue/paper/no1/papers/20.pdf)|陳学超|268
[語義理解與語音弱化－漢日“N1 的(の)N2＂結構的韻律考察－](http://xiaoan.web.fc2.com/dongyahanxue/paper/no1/papers/21.pdf)|楊曉安|275
[線性概念前置句式的描寫性特徵](http://xiaoan.web.fc2.com/dongyahanxue/paper/no1/papers/22.pdf)|雷桂林|290
[宮崎市定的東洋史觀－素樸主義民族與文明主義社會的交替循環－](http://xiaoan.web.fc2.com/dongyahanxue/paper/no1/papers/23.pdf)|連清吉|299
[武内義雄的《論語》研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/no1/papers/24.pdf)|呉 鵬|311
[吉川幸次郎的中國文學論](http://xiaoan.web.fc2.com/dongyahanxue/paper/no1/papers/25.pdf)|孟 偉|323
[日本近代的扶桑傳說研究評述](http://xiaoan.web.fc2.com/dongyahanxue/paper/no1/papers/26.pdf)|賀 南|336
[19 世紀初期旅居澳門・廣東傳教士對日語的研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/no1/papers/27.pdf)|宮澤眞一|346
[長崎中國學會會則](http://xiaoan.web.fc2.com/dongyahanxue/paper/no1/papers/28.pdf)||353
[編後記](http://xiaoan.web.fc2.com/dongyahanxue/paper/no1/papers/29.pdf)|楊曉安|355
[執筆者一覽](http://xiaoan.web.fc2.com/dongyahanxue/paper/no1/papers/30.pdf)||357


### 創刊號 2011年6月
[創刊號 2011年6月 西安](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/index.pdf)

Title|Author|Page
---|---|---
[卷首語](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/1.pdf)|陳學超|1
[先秦儒學成德之教的現代詮釋與商榷－兼論儒學如何與卄一世紀人類文明接榫](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/2.pdf)|袁保新|3
[論近代日本學界之荀子名學研究一種初步考察](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/3.pdf)|李哲賢|18
[文化的共命慧 — 以儒家王道哲學為中心](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/4.pdf)|吳進安|31
[老子之學非獨任虛無─以薛蕙《老子集解》為觀察之核心](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/5.pdf)|江淑君|43
[語默之間：戲論、卮言以及默然](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/6.pdf)|李幸玲|54
[宮崎市定的東洋近世論―宋代是中國文藝復興時代―](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/7.pdf)|連清吉|65
[近現代華北村落中的宗族與村政](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/8.pdf)|祁建民|76
[文獻研究的時序](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/9.pdf)|周彥文|87
[世園會與西安可持續發展](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/10.pdf)|陳學超|97
[論元雜劇的本色派](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/11.pdf)|賈三強|102
[書法文化視域中的魯迅與日本](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/12.pdf)|李繼凱|115
[“詩的散文美”與艾青的探索實踐](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/13.pdf)|程國君|125
[新故鄉還是他鄉？─張友漁《西貢小子》中的身分追尋與成長論述](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/14.pdf)|戴華萱|134
[試論《林蘭香》家庭宅院的空間隱喻](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/15.pdf)|林偉淑|145
[知與趣的遇合－論《閒情偶寄》草木世界的文人趣味](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/16.pdf)|黃培青|156
[從「農家類」論中國古農書文獻的發展](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/17.pdf)|吳麗雯|169
[朱文穎小說中的烏托邦意象](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/18.pdf)|楊俊國|182
[故鄉想像—解析魯迅之獨特話語](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/19.pdf)|馮 鴿|188
[從個性及審美的角度看杜牧和李商隱對晚唐社會的不同感知](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/20.pdf)|王美玲． 張鴻雁|196
[從“藝（埶、蓺）”與 culthure 的本義比較說到“文化”概念的來源](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/21.pdf)|王 暉|202
[疑問句類型辨別的韻律特徵](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/22.pdf)|楊曉安|209
[社會認同與中文教學—香港的“兩文三語”問題](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/23.pdf)|侍建國． 卓瓊妍|223
[海內外互動與海外華文教育發展](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/24.pdf)|趙學清|237
[關於長崎方言中的外來語彙](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/25.pdf)|周國強|246
[《史記‧楚世家》與新出清華簡<楚居>篇王名校讀](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/26.pdf)|張連航|258
[漢語單元音時長與音高的關係](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/27.pdf)|楊曉安． 高 芳|270
[點性事象、線性事象與兩種數量成分前置句式](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/28.pdf)|雷桂林|277
[態勢語在對外漢語教學中的作用與運用策略](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/29.pdf)|劉 惠|287
[《爾雅》同訓詞語釋讀](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/30.pdf)|李鳳蘭|292
[吉川幸次郎的《論語》研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/31.pdf)|呉 鵬|300
[日本神話中雉的文化意蘊─中國經典對「天若日子」神話的影響─](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/32.pdf)|賀 南|311
[森槐南《杜詩講義》的特色](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/33.pdf)|王 琨|322
[西安的城市文脈與城市色彩](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/34.pdf)|陳 靜|333
[從埃及學的發展看西安學](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/35.pdf)|王 樂|343
[古都文化再生－以京都為例－](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/36.pdf)|黃 婕|346
[論跨文化交際對象的直覺判斷對交際者的素質要求](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/37.pdf)|白玉波|358
[民族文化中的糾葛—以回族文學中少年形象為例的考察](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/38.pdf)|王繼霞|365
[試析”動詞+施事賓語”的句法發展—以“曬太陽”為例](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/39.pdf)|錢 珍|372
[對外漢語教學中跨文化交際障礙及化解途徑](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/40.pdf)|王曉音|381
[Contents](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/41.pdf)||390
[執筆者一覽](http://xiaoan.web.fc2.com/dongyahanxue/paper/no2/papers/42.pdf)||392


### 第2號 2012年4月
[第2號 2012年4月 臺北](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/index.pdf)

Title|Author|Page
---|---|---
[卷首語](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/1.pdf)|戴華萱 ⅰ張治道嘉靖二十三年詩作考論|賈三強|1
[《金瓶梅》時空敘事所展現的抒情視域](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/2.pdf)|林偉淑|12
[癖花賞色─由《閒情偶寄》論李漁「香草美人」觀的建構](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/3.pdf)|黃培青|23
[萬曆中後期楚地文人詩學中的楚地思維](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/4.pdf)|謝旻琪|37
[由忠義傳到烈士傳：殉國傳記的書寫傳統](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/5.pdf)|鄭尊仁|48
[苦難與救贖：女性神話的建立與幻滅](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/6.pdf)|余婉兒|60
[談南宮搏歷史小說的情愛書寫](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/7.pdf)|蔡造珉|73
[論鍾肇政《濁流三部曲》台灣意識的書寫策略](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/8.pdf)|戴華萱|85
[鍾文音《傷歌行》的家國想像與族群意識](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/9.pdf)|劉依潔|95
[論文字性符號、非文字性符號、“文字畫”與原始文字之別](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/10.pdf)|王 暉|105
[中日名詞性獨詞疑問句語義比較](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/11.pdf)|楊曉安|115
[從言意之辨到語義解構—對錢鐘書“字名論”的闡釋](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/12.pdf)|方環海|124
[韓語中一些源於上古漢語日、疑母字的鼻音聲母](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/13.pdf)|嚴翼相|134
[唐通事與日本近代漢語教育](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/14.pdf)|高 芳|143
[海外華語教材的文化“內化”](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/15.pdf)|陳學超|156
[《齊民要術》製麴工法中宗教儀式研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/16.pdf)|諸葛俊元|162
[莊子之「智」的涵義](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/17.pdf)|鄧城鋒|173
[沈一貫《莊子通》「以儒解莊」思想探論](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/18.pdf)|李懿純|183
[徹悟禪師「般若淨土兩門大義」發隱](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/19.pdf)|陳劍鍠|196
[論皮錫瑞（1850-1908）的經學正統觀](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/20.pdf)|宋惠如|207
[通人事以致用─試論孫奇逢的「實學」思想](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/21.pdf)|張曉芬|218
[梁啟超的《新民說》—如何評價〈論私德〉—](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/22.pdf)|木山愛莉|227
[陳寅恪與 20 世紀漢學](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/23.pdf)|劉克敵|239
[中共資本主義觀的形成及其演變](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/24.pdf)|祁建民|250
[近現代書刊中的北京記述（1900－1949）](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/25.pdf)|張鴻聲|261
[興觀群怨：香港粵語流行曲詞的文學與本土文化意識](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/26.pdf)|謝家浩|275
[賴際熙的港大歲月](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/27.pdf)|方 駿|282
[借古鑑今：飲食禮儀](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/28.pdf)|陳淑貞|294
[《制造革命的日本人─評傳・梅屋莊吉》與中・台譯本的比較](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/29.pdf)|周國強|302
[洋腔洋調與純正地道─加拿大大學生漢語學習策略與漢語水準的調查與分析](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/30.pdf)|何麗芳|311
[中國常常“錯失”世界大獎的理性思考](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/31.pdf)|李貴森|318
[《左傳》中鬼的寓意探釋](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/32.pdf)|林溫芳|327
[韓愈文集「雜文」類中的〈毛穎傳〉](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/33.pdf)|沈秀蓉|339
[試論金代「中州」與「國朝文派」定義](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/34.pdf)|陳蕾安|350
[救亡圖存─程憬建立中國神話體系的初衷](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/35.pdf)|趙惠瑜|361
[「詩法」東傳─樂善堂本《詩法纂論》及《詩法纂論續編》考述](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/36.pdf)|邱怡瑄|371
[後現代城市美學視野下的西咸新區建設理念芻議](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/37.pdf)|陳 靜|382
[吉川幸次郎著作《元雜劇研究》的經緯及特色](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/38.pdf)|孟 偉|391
[東漢時代洛陽的文化象徵和歷史風土](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/39.pdf)|黃 婕|404
[東亞漢學研究學會章程](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/40.pdf)||416
[第一屆東亞漢學研究學會理事會名單](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/41.pdf)||418
[《東亞漢學研究》投稿要求與撰稿格式](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/42.pdf)||419
[Contents](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/43.pdf)||422
[執筆者一覽](http://xiaoan.web.fc2.com/dongyahanxue/paper/no3/papers/44.pdf)||424


### 特別號 2013年3月
[特別號 2013年3月 西安](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/index.pdf)

Title|Author|Page
---|---|---
[卷首語](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/1.pdf)|侍建國|i
[《論語》內外的時空考論](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/2.pdf)|楊 義|1
[「土龍安能而致雨？」─論王充對董仲舒災異說的省察](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/3.pdf)|宋惠如|20
[漢代「更受命」說研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/4.pdf)|諸葛俊元|32
[宋人論莊子與道教](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/5.pdf)|簡光明|42
[王真的兵戰論述](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/6.pdf)|江淑君|53
[新疆佛教石窟燃燈佛授記圖像初探](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/7.pdf)|李幸玲|65
[武關古址及其遷徙時代考](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/8.pdf)|王晖‧高芳|76
[中國關於“普世價值”爭論的核心問題及其後果](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/9.pdf)|祁建民|83
[溪水悠悠春自來－－論柳宗元「愚溪」諸詠](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/10.pdf)|黃培青|94
[唐傳奇「空間結構」之構寫技法與義蘊](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/11.pdf)|林淑貞|106
[非獨予親戚，亦朋友也—論王適與蘇軾、蘇轍之交遊與詩文酬唱](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/12.pdf)|郭玲妦|121
[论朱淑眞之民俗詞](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/13.pdf)|金賢珠‧李秀珍|133
[明代陝西戲曲創作與表演述論](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/14.pdf)|賈三強|140
[近百年來“花兒”研究概述](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/15.pdf)|趙學清‧曹強|151
[傳統與現代的交替敘事——以《新法螺先生譚》為例](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/16.pdf)|馮 鴿|158
[權力的游弋與移動的江南書寫—談雙紅堂藏清末四川唱本《鈹羅當遊江南》](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/17.pdf)|丁淑梅|164
[獨特的「扒灰」笑話──兼論民間故事的翁媳關係](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/18.pdf)|鹿憶鹿|172
[通俗與閑適﹕二十世纪九十年代中國散文潮流](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/19.pdf)|陳學超|185
[新文學初期上海城市現代性表達中的日本因素](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/20.pdf)|張鴻聲|195
[彰顯生命“正能量”—以《山川記》和《鹽道》的人物形象塑造為例](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/21.pdf)|李繼凱|206
[問題設計與大二中國當代文學閱讀教學](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/22.pdf)|陳思廣|214
[真理與救贖：李昂的寓言小說探析](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/23.pdf)|戴華萱|224
[是醒覺？還是痴迷？—試論《紅樓夢》中「恥情而覺」的尤三姐及其身體意識](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/24.pdf)|林偉淑|235
[戰船與沉船](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/25.pdf)|李其霖|246
[論明代小說戲曲中的尋親主題](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/26.pdf)|王建科|263
[人生地獄的巡禮——大江健三郎《人生的親戚》論](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/27.pdf)|霍士富|274
[殖民語境裂痕與“滿映”娛民片的複雜裝置及認識視角](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/28.pdf)|逄增玉|283
[語義指向消歧之韻律特徵－一個有關副詞“才”的聲學實驗](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/29.pdf)|楊曉安|293
[論民族語的口語](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/30.pdf)|侍建國|306
[西方漢學與漢語量詞類型特徵研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/31.pdf)|方環海 ‧葉祎琳|317
[漢英句子功能中心與虛主語的使用](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/32.pdf)|張 螢|327
[漢英假設標記比較](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/33.pdf)|董秀英|335
[“港味普通話”現象的若干思考](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/34.pdf)|錢 芳|342
[台灣儒學的回顧與前瞻](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/35.pdf)|吳進安|350
[論日本漢學研究之意義](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/36.pdf)|李哲賢|362
[華僑傳統文化在長崎](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/37.pdf)|王 維|373
[吉川幸次郎的中國精神史論](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/38.pdf)|連清吉|385
[英語漢學界宋元話本小說研究述評](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/39.pdf)|鄧駿捷‧曾嘉文|397
[李商隱櫻桃詩的詮釋問題](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/40.pdf)|謝旻琪|407
[新世紀台灣現代小說中的「被污辱與被損害的」](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/41.pdf)|黃文倩|420
[副詞“最”總括義和程度義的來源](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/42.pdf)|錢 珍|430
[感歎詞的篇章功能及其實驗研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/43.pdf)|劉惠‧鄧宏麗|438
[語言景觀中的澳門多語狀況](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/44.pdf)|張媛媛|454
[唐代士人的政治理念和轉型分化—以“致君堯舜”為中心](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/45.pdf)|蔣金珅|468
[張弘靖、李德裕太原唱和群體考論](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/46.pdf)|胡秋妍|477
[《總目》宋別集提要中的「韓柳之波」](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/47.pdf)|賴位政|487
[從文史敘事整合的角度論述黃仁宇《萬曆十五年》](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/48.pdf)|楊閩威|498
[“語言特區”理論下現當代詩歌語法的動態考察](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/49.pdf)|劉 穎|509
[在日朝鮮人的族群認同與民族想象](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/50.pdf)|金英明|521
[日本正倉院藏《王勃詩序》的特色與價值](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/51.pdf)|胡凌燕|533
[日中における『三国志演義』の「三絶」評価について](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/52.pdf)|陳卓然・南誠|545
[中国の環境影響評価における公衆参加に関する考察―厦門 PX事件を通じて](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/53.pdf)|郭雲亮|557
[五等爵制的形成―左氏諸侯爵制說之考證(下)](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/54.pdf)|小川茂樹 原作高芳‧王暉譯|569
[中國文化價值與國際漢學研究——東亞漢學研究學會第五屆國際學術會議綜述](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/55.pdf)|逄增玉‧樂琦|579
[東亞漢學研究學會章程](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/56.pdf)||587
[第二屆東亞漢學研究學會理事會名單](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/57.pdf)||589
[《東亞漢學研究》投稿要求與撰稿格式](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/58.pdf)||590
[Contents](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/59.pdf)||593
[執筆者一覽](http://xiaoan.web.fc2.com/dongyahanxue/paper/xa/60.pdf)||596


### 第3號 2013年9月
[第3號 2013年9月 廈門](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/indexs.pdf)

Title|Author|Page
---|---|---
[卷首語](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/1.pdf)|鄭通濤|i
[論對外漢語教師的素質](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/2.pdf)|鄭通濤|1
[西方漢學中的漢語詞類及其特徵意識](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/3.pdf)|方環海、胡榮、崔丹丹、林文琪|10
[從孔子的語言實踐談對外漢語教學](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/4.pdf)|曾小紅|20
[從傳統漢學、現代漢學到新漢學的嬗變](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/5.pdf)|陳榮嵐|29
[多義消歧中的重要韻律手段](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/6.pdf)|楊曉安|35
[《論語》“仁愛”涵義考釋](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/7.pdf)|常大群|46
[朱得之《老子通義》中的心學論述](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/8.pdf)|江淑君|53
[發覆道真、釐定老莊—釋性通《南華發覆》解莊立場析論](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/9.pdf)|李懿純|63
[法藏「如來林菩薩偈」之心識思想研究—以「水波喻」及空性為解讀線索](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/10.pdf)|陳紹聖|73
[論孫奇逢「道統觀」之建立及其作用](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/11.pdf)|陳昇輝|83
[簡論中國先秦編年體史書在世界古代史學中的地位](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/12.pdf)|王 暉|94
[《逸周書》<皇門解>的年代標記](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/13.pdf)|張連航|100
[綏遠事變與全民抗戰局面的形成](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/14.pdf)|祁建民|108
[《陝西戲曲史》引言](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/15.pdf)|賈三強|117
[嘉靖壬午本《三國志通俗演義》底本新探](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/16.pdf)|趙望秦|123
[從台閣體到復古派:政治視閾下的文學話語轉型](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/17.pdf)|楊遇青|128
[唐代藩鎮地理特徵研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/18.pdf)|黨 斌|138
[由唐代詩人論文學史的書寫](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/19.pdf)|周彥文|147
[《四庫全書總目》中的「詩史」](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/20.pdf)|曾守正|158
[古農書文獻中蘊含之飲食文化探究 ─以《齊民要術》為例](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/21.pdf)|吳麗雯|167
[中古五言詩之上尾、鶴膝考論](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/22.pdf)|胡 旭|178
[偽齊時期散文及其繫年考](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/23.pdf)|陳蕾安|188
[許學夷對元和時期詩歌的看法](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/24.pdf)|謝旻琪|198
[最是橙黃橘綠時—論蘇軾元祐時期詩歌特色與生命治理(1)(2)](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/25.pdf)|賴靜玫|209
[明代詞選對夢窗詞的選錄與評點](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/26.pdf)|普義南|220
[唐通事唐話教本中的文化教育管窺](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/27.pdf)|高 芳|230
[論《紅樓夢》的場景設計一以第二十七回為例](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/28.pdf)|董淑玲|241
[王國維超功利的文學觀](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/29.pdf)|陳學超|250
[吉川幸次郎的中日近代中國學綜述](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/30.pdf)|連清吉|260
[貝塚茂樹的《論語》研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/31.pdf)|吳 鵬|269
[論魯迅“新三立”的人生境界](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/32.pdf)|李繼凱|279
[1950 年代『許地山選集』の編纂から―「読芝蘭與茉莉因而想及我底祖母」について](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/33.pdf)|松岡純子|290
[台灣女聲—七○年代台灣女性散文家的鄉土書寫](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/34.pdf)|戴華萱|299
[從小說到電影敘事視角的改變 ─以《風聲》為例 (1) (2)](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/35.pdf)|黃淑貞|309
[莫言在台灣被接受的典律意義](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/36.pdf)|黃文倩|320
[尋找記憶:白先勇《台北人》「不在場」之敘事策略](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/37.pdf)|林淑貞|330
[漢語親屬稱謂詞“姊”與“姐”的演變關系考](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/38.pdf)|楊彩賢|341
[先秦諸子家數論](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/39.pdf)|薛榕婷|348
[《玉光劍氣集》中的明末史事考述](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/40.pdf)|楊閩威|358
[夢了為覺,情了為佛――談湯顯祖的後二夢](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/41.pdf)|吳美幸|367
[以學問治詩:近藤光男(1921- )之《清詩選》與其清詩批評觀](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/42.pdf)|邱怡瑄|375
[陳夢家的神話研究 ─以水的神話為例](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/43.pdf)|趙惠瑜|384
[再論沈從文與書法文化](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/44.pdf)|孫曉濤、李繼凱|390
[《中華竹枝詞》、《歷代竹枝詞》補遺(1)(2)—兼及屈復《變竹枝詞》中的民俗史料](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/45.pdf)|杜學林|400
[“陝西地方文獻國際學術研討會”會議綜述](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/46.pdf)|王 珂|411
[東亞漢學研究學會章程](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/47.pdf)||417
[第一屆東亞漢學研究學會理事會名單](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/48.pdf)||419
[《東亞漢學研究》投稿要求與撰稿格式](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/49.pdf)||420
[Contents](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/50.pdf)||423
[執筆者一覽](http://xiaoan.web.fc2.com/dongyahanxue/paper/no4/papers/51.pdf)||425


### 第4號 2014年5月
[第4號 2014年5月 北京](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/index.pdf)

Title|Author|Page
---|---|---
[卷首語](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/1.pdf)|逄增玉|i
[短語結構消歧中的韻律特徵實驗研究─關於“沒有VP的NP”結構韻律的聲學分析](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/2.pdf)|楊曉安|1
[對外漢語多媒 體教材編 寫理論的 若干問題](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/3.pdf)|逄增玉、劉海燕|9
[多元化視角下 高級漢語 寫作課程 的設計與實 施](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/4.pdf)|包學菊|19
[漢語國際教育 中《中國 現代文學 》課程建設 思考](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/5.pdf)|樂琦|23
[現代漢語形容詞的語義功能及其相關問題](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/6.pdf)|劉妍|32
[關於日本“天 天中文” 漢語學習 網站的調查 報告](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/7.pdf)|張婧、崔 昊為|43
[網路口語教學中自主性學習能力培養現狀分析](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/8.pdf)|王樂|52
[由《隋書．經 籍志》論浮動 式分類法](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/9.pdf)|周彥文|59
[月令體農書與農民生活樣貌─以《農桑衣食撮要》為探究對象](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/10.pdf)|吳麗雯|70
[上古時期陝西地區戲劇類表演藝術述論](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/11.pdf)|贾三强|82
[陝西關中土改 中的清濁 河水利民 主改革](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/12.pdf)|祁建民|93
[初唐權力之爭與國史的篡改——李建成夫婦墓志相關問題](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/13.pdf)|党斌|103
[白居易的科考及科舉觀](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/14.pdf)|付興林|109
[畢沅《關中金石記》考述](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/15.pdf)|李向菲|124
[李庭訓及其《醯雞吟》考論](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/16.pdf)|趙望秦|132
[清華簡《 程寤》篇中的 和](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/17.pdf)|張連航|138
[《史記》「三 代循環說 」試探](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/18.pdf)|諸葛俊元|141
[《法華經》與雲岡石窟「三世佛 」造像](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/19.pdf)|李幸玲|151
[《金瓶梅》中性別所展現的身體空間及其文化意義— 以 武 大、武松、吳月娘、潘金蓮、李瓶兒為例](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/20.pdf)|林偉淑|165
[張岱修辭美學 初探— 以《瑯嬛文集》銘文舉隅為例[A ] [B ][ C]](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/21.pdf)|徐紀芳|175
[論李漁品花賞 卉的詩性 書寫](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/22.pdf)|黃培青|191
[論《封神演義》對儒家五倫的消解與重構](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/23.pdf)|王猛|204
[論朱自清與中國書法文化](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/24.pdf)|李继凯|212
[女子弄文— 論張漱菡五○年代的女性教育小說](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/25.pdf)|戴華萱|222
[異龍現，江湖變，靈劍泣，野火熄－ 論 鄭 丰武俠小說的自我超越](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/26.pdf)|蔡造珉|232
[從六觀法論施 蟄存〈薄暮的 舞女〉](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/27.pdf)|嚴紀華|243
[論《第二次握手》的文學性——以 1979 年版為中心](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/28.pdf)|鐘海波|254
[行走在“論語”與“聖經”之外—讀解劉震雲《一句頂一萬句》的一個視角](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/29.pdf)|楊俊國|267
[長崎唐話中對伊東走私事件敘述差異的探討江 戶 時 代唐通事養成教材研究之二](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/30.pdf)|林慶勳|273
[長崎離島的觀光資源與中國觀光客](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/31.pdf)|周國強|284
[華人節慶文化活動認知探究](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/32.pdf)|彭妮絲|294
[吉川幸次郎的「 讀書之學」](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/33.pdf)|連清吉|306
[內藤湖南螺旋循環史觀評析](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/34.pdf)|吳鵬、楊延峰|315
[試探圖像在古典文獻研究中的作用—以元刊日用類書《事林廣記》插圖為例 [A][B][C][D]](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/35.pdf)|王珂|326
[商代“亞”為內服職官考—以青銅器“亞羌”銘文釋義為中心](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/36.pdf)|武剛|340
[釋性通《南華發覆》解莊系統初探：以「 心 性」、「真宰」為解讀進路](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/37.pdf)|李懿純|353
[在普世與入世 間－ 7 0 年代後夏志清的現代文學批評](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/38.pdf)|黃文倩|363
[売茶翁の売茶活動に関する一考察－ 京 都 通仙亭開業までの空白の期間](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/39.pdf)|馬叢慧|373
[由意物關係論文學創作方法：陸機〈文賦〉之文學觀念抉發](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/40.pdf)|朱天|386
[漢學家的文化藝術史：雅士高羅佩及其《秘戲圖考》](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/41.pdf)|夏小雙|398
[清代女作家的西施接受研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/42.pdf)|張海燕|406
[論楊逵小說中的底層民眾形象](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/43.pdf)|宋颖慧|416
[中国語の新し い受身構 文「被 XX」と日本語へ の翻訳](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/44.pdf)|智暁敏|423
[“東亞漢學研究學會第四屆國際學術會議暨首屆新漢學國際學術研討會”綜述](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/45.pdf "東亞漢學的多元形態及其當代發展")|方環海|436
[東亞漢學研究學會章程](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/46.pdf)||443
[第二屆東亞漢學研究學會理事會名單](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/47.pdf)||445
[《東亞漢學研究》投稿要求與撰稿格式](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/48.pdf)||446
[Contents](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/49.pdf)||449
[執筆者一覽](http://xiaoan.web.fc2.com/dongyahanxue/paper/no5/papers/50.pdf)||452


### 特別號 2014年12月
[特別號 2014年12月 東京](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/index.pdf)

Title|Author|Page
---|---|---
[卷首語](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/01.pdf)|連清吉|i
[中國西北地區戲曲歌謠語言文化研究概述](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/1.pdf)|趙學清|1
[秦—六朝陝西戲劇類表演藝術述論](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/2.pdf)|贾三强|8
[秦腔研究述評](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/3.pdf)|王懷中|15
[近五年陝北民歌研究現狀及今後的研究取向](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/4.pdf)|李占平|25
[西北地區皮影戲研究情況概述](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/5.pdf)|劉琨|39
[論秦末漢初的門客及其辭賦創作](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/6.pdf)|高一農|46
[從「風格」到「作用」──論許學夷「正對階級」說的意義](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/7.pdf)|謝旻琪|57
[清末民國鼓詞文獻綜述](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/8.pdf)|孫宏亮|70
[文學教育與文學秩序：現代中國大學的“文學史”學科及其教育](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/9.pdf)|王榮|79
[1950 年代初中國文藝運動中的文學問題](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/10.pdf)|周燕芬|91
[自卑與“超越”—魯迅《高老夫子》的心理學解讀](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/11.pdf)|姜彩燕|102
[東方智慧：尋找“身體”的自由之境—老舍與村上春樹短篇小說中的“身體”意象之比較](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/12.pdf)|趙學勇|113
[囚禁的鹽屋—論李昂《花季》的成長困境](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/13.pdf)|戴華萱|126
[王厚之生平考述](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/14.pdf)|王曉鵑|138
[21 世紀中國對美國漢學界的中國小說研究之研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/15.pdf)|任虎軍|150
[“十七年文學”中的少數民族女性形象](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/16.pdf)|逄增玉・孫曉平|163
[周代近親廟制考](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/17.pdf)|王暉‧高芳|174
[秦嶺碑刻歷史發展述論](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/18.pdf)|吳敏霞|184
[毛鳳枝金石學著作三種述評](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/19.pdf)|李向菲|193
[《旅順博物館所藏甲骨》新綴廿一組](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/20.pdf)|林宏明|205
[漢日形容詞獨詞句語調形式與語義之比較](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/21.pdf)|楊曉安|215
[吉川幸次郎的「書不盡言」論](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/22.pdf)|連清吉|228
[「燃燈佛授記」敘事與圖像互文性之系譜考察──以漢譯佛典與犍陀羅佛教圖像為例](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/23.pdf)|李幸玲|239
[論郭熙《林泉高致》中的儒道思想](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/24.pdf)|陳昇輝|251
[晚明注《莊》思想流變研究：以釋德清到釋性通的「逍遙」為例](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/25.pdf)|李懿純|262
[「天數」、「人欲」的自然性與社會性—中國二十世紀初的莊、荀論述與治道](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/26.pdf)|周志煌|273
[試論唐三寺住持與長崎唐人的互動](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/27.pdf)|林慶勳|284
[中日文化交往源流論略](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/28.pdf)|梁瑜霞|297
[五等爵制的成立――左氏諸侯爵制度說之考證(上)](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/29.pdf)|小川茂樹<br/>高芳‧王暉譯|311
[東亞漢學研究學會章程](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/30.pdf)||321
[第二屆東亞漢學研究學會理事會名單](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/31.pdf)||323
[《東亞漢學研究》投稿要求與撰稿格式](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/32.pdf)||324
[Contents](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/33.pdf)||327
[執筆者一覽](http://xiaoan.web.fc2.com/dongyahanxue/paper/dj/papers/34.pdf)||329


### 第5號 2015年5月
[第5號 2015年5月 澳門](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/index.pdf)

Title|Author|Page
---|---|---
[卷首語](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/1.pdf)|侍建國|i
[《論語》內外的時空考論](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/2.pdf)|楊 義|1
[「土龍安能而致雨？」─論王充對董仲舒災異說的省察](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/3.pdf)|宋惠如|20
[漢代「更受命」說研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/4.pdf)|諸葛俊元|32
[宋人論莊子與道教](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/5.pdf)|簡光明|42
[王真的兵戰論述](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/6.pdf)|江淑君|53
[新疆佛教石窟燃燈佛授記圖像初探](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/7.pdf)|李幸玲|65
[武關古址及其遷徙時代考](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/8.pdf)|王晖‧高芳|76
[中國關於“普世價值”爭論的核心問題及其後果](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/9.pdf)|祁建民|83
[溪水悠悠春自來－－論柳宗元「愚溪」諸詠](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/10.pdf)|黃培青|94
[唐傳奇「空間結構」之構寫技法與義蘊](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/11.pdf)|林淑貞|106
[非獨予親戚，亦朋友也—論王適與蘇軾、蘇轍之交遊與詩文酬唱](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/12.pdf)|郭玲妦|121
[论朱淑眞之民俗詞](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/13.pdf)|金賢珠‧李秀珍|133
[明代陝西戲曲創作與表演述論](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/14.pdf)|賈三強|140
[近百年來“花兒”研究概述](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/15.pdf)|趙學清‧曹強|151
[傳統與現代的交替敘事——以《新法螺先生譚》為例](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/16.pdf)|馮 鴿|158
[權力的游弋與移動的江南書寫—談雙紅堂藏清末四川唱本《鈹羅當遊江南》](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/17.pdf)|丁淑梅|164
[獨特的「扒灰」笑話──兼論民間故事的翁媳關係](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/18.pdf)|鹿憶鹿|172
[通俗與閑適﹕二十世纪九十年代中國散文潮流](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/19.pdf)|陳學超|185
[新文學初期上海城市現代性表達中的日本因素](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/20.pdf)|張鴻聲|195
[彰顯生命“正能量”—以《山川記》和《鹽道》的人物形象塑造為例](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/21.pdf)|李繼凱|206
[問題設計與大二中國當代文學閱讀教學](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/22.pdf)|陳思廣|214
[真理與救贖：李昂的寓言小說探析](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/23.pdf)|戴華萱|224
[是醒覺？還是痴迷？—試論《紅樓夢》中「恥情而覺」的尤三姐及其身體意識](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/24.pdf)|林偉淑|235
[戰船與沉船](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/25.pdf)|李其霖|246
[論明代小說戲曲中的尋親主題](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/26.pdf)|王建科|263
[人生地獄的巡禮——大江健三郎《人生的親戚》論](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/27.pdf)|霍士富|274
[殖民語境裂痕與“滿映”娛民片的複雜裝置及認識視角](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/28.pdf)|逄增玉|283
[語義指向消歧之韻律特徵－一個有關副詞“才”的聲學實驗](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/29.pdf)|楊曉安|293
[論民族語的口語](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/30.pdf)|侍建國|306
[西方漢學與漢語量詞類型特徵研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/31.pdf)|方環海 ‧葉祎琳|317
[漢英句子功能中心與虛主語的使用](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/32.pdf)|張 螢|327
[漢英假設標記比較](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/33.pdf)|董秀英|335
[“港味普通話”現象的若干思考](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/34.pdf)|錢 芳|342
[台灣儒學的回顧與前瞻](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/35.pdf)|吳進安|350
[論日本漢學研究之意義](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/36.pdf)|李哲賢|362
[華僑傳統文化在長崎](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/37.pdf)|王 維|373
[吉川幸次郎的中國精神史論](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/38.pdf)|連清吉|385
[英語漢學界宋元話本小說研究述評](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/39.pdf)|鄧駿捷‧曾嘉文|397
[李商隱櫻桃詩的詮釋問題](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/40.pdf)|謝旻琪|407
[新世紀台灣現代小說中的「被污辱與被損害的」](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/41.pdf)|黃文倩|420
[副詞“最”總括義和程度義的來源](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/42.pdf)|錢 珍|430
[感歎詞的篇章功能及其實驗研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/43.pdf)|劉惠‧鄧宏麗|438
[語言景觀中的澳門多語狀況](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/44.pdf)|張媛媛|454
[唐代士人的政治理念和轉型分化—以“致君堯舜”為中心](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/45.pdf)|蔣金珅|468
[張弘靖、李德裕太原唱和群體考論](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/46.pdf)|胡秋妍|477
[《總目》宋別集提要中的「韓柳之波」](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/47.pdf)|賴位政|487
[從文史敘事整合的角度論述黃仁宇《萬曆十五年》](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/48.pdf)|楊閩威|498
[“語言特區”理論下現當代詩歌語法的動態考察](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/49.pdf)|劉 穎|509
[在日朝鮮人的族群認同與民族想象](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/50.pdf)|金英明|521
[日本正倉院藏《王勃詩序》的特色與價值](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/51.pdf)|胡凌燕|533
[日中における『三国志演義』の「三絶」評価について](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/52.pdf)|陳卓然・南誠|545
[中国の環境影響評価における公衆参加に関する考察―厦門 PX事件を通じて](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/53.pdf)|郭雲亮|557
[五等爵制的形成―左氏諸侯爵制說之考證(下)](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/54.pdf)|小川茂樹 原作<br/>高芳‧王暉譯|569
[中國文化價值與國際漢學研究——東亞漢學研究學會第五屆國際學術會議綜述](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/55.pdf)|逄增玉‧樂琦|579
[東亞漢學研究學會章程](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/56.pdf)||587
[第二屆東亞漢學研究學會理事會名單](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/57.pdf)||589
[《東亞漢學研究》投稿要求與撰稿格式](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/58.pdf)||590
[Contents](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/59.pdf)||593
[執筆者一覽](http://xiaoan.web.fc2.com/dongyahanxue/paper/no6/papers/60.pdf)||596


### 特別號 2016年2月
[特別號 2016年2月 京都](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/menu.pdf)

Title|Author|Page
---|---|---
[卷首語（一）](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/01.pdf)|段建軍|i
[卷首語（二）](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/02.pdf)||連清吉 ⅲ
[為歷史而煩—《白鹿原》的鄉土生命哲學及其敘事價值](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/1.pdf)|段建軍|1
[論創業文學與絲路文學](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/2.pdf)|李繼凱|14
[詩與真—論魯迅《朝花夕拾》的史料真實問題](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/3.pdf)|田 剛|25
[中國美學的詩性智慧及現代意義](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/4.pdf)|李西建|37
[人性書寫：表層的相異與深層的契合—沈從文與張愛玲比較論](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/5.pdf)|趙學勇|47
[論 20世紀 40年代“國統區”的延安文藝及其傳播](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/6.pdf)|王 榮|57
[20 世紀中國文學視野中的《創業史》研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/7.pdf)|周燕芬|72
[路遙作品在日本的傳播](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/8.pdf)|梁向陽 丁亞琴|80
[“紅色經典”情愛敍事的邊界、隱言與盲區](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/9.pdf)|惠雁冰|90
[現當代中國文學中的中醫形象及其價值承擔—對幾部中醫題材作品的抽樣分析](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/10.pdf)|逄增玉|101
[禁色的愛？—論李昂的同志小說](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/11.pdf)|戴華萱|112
[從演化論的視角看地方文化保護的意義](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/12.pdf)|葛岩 岳晨昕|124
[再論《論語》的編纂—兼評楊義先生《論語還原》](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/13.pdf)|楊遇青|133
[先秦天人思想之嬗變—“軸心突破”時期天人思想之分化](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/14.pdf)|段永昇|145
[敦煌文獻中的燃燈佛授記析論](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/15.pdf)|李幸玲|156
[王道《老子億》詮解《老子》首章的幾點觀察](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/16.pdf)|江淑君|168
[唐代宗室法律管理情況研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/17.pdf)|劉思怡|180
[唐《律書樂圖》輯考](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/18.pdf)|亓娟莉|191
[新見唐《獨孤瑛墓誌》小考](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/19.pdf)|党 斌|201
[宋初醫籍編撰史實考略—以太祖、太宗兩朝爲中心](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/20.pdf)|王 珂|208
[《知非齋劇本》小考](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/21.pdf)|敬曉慶|217
[論傳統類書中的互文性](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/22.pdf)|周彥文|226
[嚴如熤《三省山內風土雜識》述考](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/23.pdf)|贾三强 李雪峰|239
[陝西靖邊縣舊志述略](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/24.pdf)|高葉青|247
[陝西古籍整理的現狀與發展前景](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/25.pdf)|白寬犁|256
[陝西古代的音韻學研究—對《陝西省志·著述志》所錄音韻學書目的補正](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/26.pdf)|沈文君|263
[歷史敘事與文學敘事中的韓信—兼論漢水流域戲曲中的韓信戲](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/27.pdf)|王建科|270
[試論飲酒的文化功能—以陶淵明為中心的考察](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/28.pdf)|李紅岩|281
[《文心雕龍》在美國“中國文學史”書寫與“文選”編譯中的經典重構問題](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/29.pdf)|谷鵬飛|291
[白居易昭君詩所折射的仕宦心路歷程—兼論白居易昭君詩的文學史價值及其啟示意義](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/30.pdf)|付興林|304
[威嚴與逍遙：唐代華山詩的雙重主題](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/31.pdf)|邱 曉|316
[辛棄疾與陳亮“鵝湖之會”文學意義之重估](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/32.pdf)|張文利|325
[身處邊緣：論金元時期耶律楚材家族的尷尬處境](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/33.pdf)|和 談|335
[晚清文學中的上海](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/34.pdf)|張鴻聲 王文勳|346
[魯迅與福澤渝吉—以教育思想為中心的考察](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/35.pdf)|姜彩燕|355
[古文字形體與遠古住宅形制研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/36.pdf)|王 暉|367
[塗朱甲骨綴合八組](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/37.pdf)|林宏明|373
[論漢語背景下的字義與詞義](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/38.pdf)|趙小剛|381
[异質文化間的溝通—威廉·瓊斯的漢學研究及其價值](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/39.pdf)|于俊青|390
[顧炎武與清初金石學的復興](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/40.pdf)|李向菲|403
[日本古代漢詩中的長安](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/41.pdf)|郭雪妮|415
[遣唐使與唐長安文化](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/42.pdf)|楊曉安 高芳|421
[京都中國學者的學問意識―讀書求是、有心印而無雷同](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/43.pdf)|連清吉|430
[神田喜一郎の日本漢文学論](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/44.pdf)|沈日中|440
[吉川英治『三国志』における「三絶」の人物像について](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/45.pdf)|陳卓然|451
[東亞漢學研究學會章程](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/46.pdf)||463
[第三屆東亞漢學研究學會理事會名單](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/47.pdf)||465
[《東亞漢學研究》投稿要求與撰稿格式](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/48.pdf)||467
[Contents](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/49.pdf)||470
[執筆者一覽](http://xiaoan.web.fc2.com/dongyahanxue/paper/jd/50.pdf)||473


### 第6號 2016年4月
[第6號 2016年4月 臺灣淡江]|(http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/menu.pdf)

Title|Author|Page
---|---|---
[卷首語](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/01.pdf)|周彥文|i
[國學、漢學與華學](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/1.pdf)|陳學超|1
[The Confucian Sage and the Christian Saint:Two ParadigmaticFigures in World Religions](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/2.pdf)|Richard Shek|8
[漢郊祀歌〈帝臨〉章考釋](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/3.pdf)|殷善培|22
[《禮記․樂記》文藝思想芻議](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/4.pdf)|黃培青|31
[從毛鄭對《詩經》「興詩」的箋釋—論「詩性文化符碼」類型](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/5.pdf)|林菁菁|43
[唐詩宋詞誤讀與正辯](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/6.pdf)|付興林|58
[論韋莊詩中「遇酒且呵呵，人生能幾何」的憂傷情調─兼論以詩證詞之可能性](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/7.pdf)|謝旻琪|69
[晚清「時新小說」《花柳深情傳》研究─作為清代中期世情小說的對照座標](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/8.pdf)|胡衍南|79
[論晚清幻想小說中的疾病意象](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/9.pdf)|馮鴿|89
[《水滸傳》三寫元宵節的敘事意義](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/10.pdf)|林偉淑|98
[漢水流域三國戲中的曹操故事與曹操形象](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/11.pdf)|王建科|108
[陝北說書“研究範式”芻議](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/12.pdf)|趙學清 孫鴻亮|118
[楊爵詩歌的關學精神發微](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/13.pdf)|蔣鵬舉 吳亞媛|123
[雙紅堂藏清末四川唱本《一台戲他四種》研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/14.pdf)|丁淑梅|128
[亂世武俠的平凡英雄述寫―以張草《庖人誌》三部曲為探討範疇](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/15.pdf)|蔡造珉|139
[｢敦煌二十詠｣之文學地理學的考察](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/16.pdf)|金賢珠·李秀珍|151
[試論梁啟超關於朝鮮的兩首詩歌—《秋風斷藤曲》、《朝鮮哀詞》五律二十四首中所表現的對朝鮮的認識和感情](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/17.pdf)|崔亨旭|163
[梁啟超與《新中國未來記》的「進化論」色彩](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/18.pdf)|紀俊龍|179
[凝視歷史分界點的選擇─吳宓對傳統詩學擇取與承繼之意義](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/19.pdf)|林淑貞|189
[李昂飲食散文中的台灣歷史關懷與世界觀](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/20.pdf)|戴華萱|202
[旁觀者的介入與限度—以呂途與梁鴻的「打工」書寫為例](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/21.pdf)|黃文倩|213
[從人格成神的生成談台灣民間信仰傳播—以台中地區之廖添丁與王勳為例](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/22.pdf)|黃文成|223
[分析《許三觀賣血記》中的黑色幽默](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/23.pdf)|黃淑貞|234
[論于右任詩文與書法的關聯](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/24.pdf)|李繼凱 景輝|245
[全球化視野下中國內地兒童電視動畫片的創作與審美研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/25.pdf)|王利麗|256
[階級觀念在中國農村的形成（1947—1976）](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/26.pdf)|祁建民|264
[試論先秦天命思想之沿革](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/27.pdf)|諸葛俊元|274
[戰國秦漢時期的疾病療治信仰—以出土簡帛文獻為中心的考察](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/28.pdf)|呂亞虎|284
[《北溪大全集》中的理與禮](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/29.pdf)|李蕙如|296
[憨山德清注《莊》思想中的聖人觀—以〈齊物論〉為考察對象](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/30.pdf)|李懿純|305
[從邁克爾•斯洛特「移情」關懷論戴震「以情絜情」](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/31.pdf)|羅雅純|316
[「三教」融合與「因果」故事—絲路河西寶卷的文化形態、文體特徵與文化價值](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/32.pdf)|程國君|327
[支僧載及其《外國事》綜議](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/33.pdf)|陽清|337
[網帖標題的特點與應用](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/34.pdf)|周國強|346
[語音單位的連斷對句法語義關係的影響](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/35.pdf)|楊曉安|357
[美國漢學對漢語語音特徵的認識及研究—以衛三畏《中國總論》為中心](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/36.pdf)|方環海 沈玲 尹葉|366
[源于古羌語的兩個漢語單詞](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/37.pdf)|趙小剛|380
[助動詞「そうだ」「ようだ」所對應的中文表現](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/38.pdf)|賀南|386
[從篇章視角看口語教材編寫與教學](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/39.pdf)|劉惠|397
[西周金文字體演變三例](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/40.pdf)|王帥|404
[《逸周書・大匡解》的撰述年代](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/41.pdf)|張連航|416
[美、每、 為異字同源考](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/42.pdf)|邵英|422
[法國傳教士白晉（Joachim Bouvet）與《易》圖─以梵蒂岡圖書館館藏《易經》手稿為中心](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/43.pdf)|許維萍|429
[日本中國音樂的傳播與脈絡](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/44.pdf)|王維|442
[由象生趣，憑象詠情：論《滄浪詩話》中的「興趣」和「吟詠情性」](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/45.pdf)|朱天|452
[句子中心差異與空主語(pro)現象](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/46.pdf)|楊炎華|463
[特殊形制的青銅簋研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/47.pdf)|任雪莉|475
[《總目》的歷史圖像構成分析─以「蘇軾」為個案](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/48.pdf)|賴位政|490
[清末外來詞初探─以李伯元《文明小史》為例](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/49.pdf)|楊閩威|501
[論史華慈之《老子》與《莊子》神祕主義：〈道家之道〉的研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/50.pdf)|彭振利|513
[台湾新北市淡水区における景観と音景観の印象評価に関する研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/51.pdf)|陳萍|525
[中國思想文化與國際漢學研究——東亞漢學研究學會第六屆國際學術會議綜述](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/52.pdf)|侍建國 張律|538
[東亞漢學研究學會章程](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/53.pdf)||548
[第三屆東亞漢學研究學會理事會名單](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/54.pdf)||550
[《東亞漢學研究》投稿要求與撰稿格式](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/55.pdf)||552
[Contents](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/56.pdf)||555
[執筆者一覽](http://xiaoan.web.fc2.com/dongyahanxue/paper/no7/57.pdf)||558


### 特別號 2017年2月
[特別號 2017年2月 長崎](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/menu.pdf)

Title|Author|Page
---|---|---
[卷首語（一）](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/01.pdf)|付興林|i
[卷首語（二）](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/02.pdf)||曾守正 ⅲ
[內聖外王與人間佛教](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/03.pdf)|高柏園|1
[從史家到夫子：論蕭穎士人生轉向的社會史意義](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/04.pdf)|黃大宏|6
[以「知行一貫結構」論〈大學〉古本首章「原義」——儒家「成德之學」世界化的反本探索](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/05.pdf)|林碧玲|16
[「三經義疏」與聖德太子佛教主張](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/06.pdf)|莊兵|28
[狩野直喜於漢代公羊學的論述](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/07.pdf)|連清吉|39
[竹添光鴻『論語會箋』研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/08.pdf)|吳鵬|50
[哲思與玄想：博爾赫斯對芝諾悖論的文學演繹](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/09.pdf)|吳金濤|62
[歷代書目中對教育類文獻的處置及思維](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/10.pdf)|周彥文|72
[《陝西古代文獻集成》編纂劄記](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/11.pdf)|賈三強|84
[陝西碑刻的文字學研究價值](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/12.pdf)|吳敏霞|95
[韓邦奇詩詞曲的關學意蘊探析——兼談學術思想與文學寫作的交互影響](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/13.pdf)|蔣鵬舉|104
[華陰老腔劇本的語言特點](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/14.pdf)|劉琨|113
[《說文解字》重文聲符的異部變換與“合韻”論析](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/15.pdf)|劉忠華|121
[日本漢字音輔音與中古音聲母及現代西安方言的對應關係](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/16.pdf)|楊曉安|131
[小學與新國學研究芻議](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/17.pdf)|趙學清|140
[中國建築史回歸“地域性”的可能進路：以泰順蜈蚣木拱廊橋研究為例](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/18.pdf)|蕭百興|143
[如其所是和自己而然—現代性視野下的主體範式](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/19.pdf)|王保中|155
[傳統文化的生態智慧](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/20.pdf)|關嵩山|165
[秦嶺文化系統及文化資源開發研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/21.pdf)|蔡雲輝|175
[論《詩經》植物的藥用價值及文化內涵](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/22.pdf)|劉昌安|186
[詩教與詩諫說的理論溯源](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/23.pdf)|李宜蓬|198
[陶淵明《詠貧士》等詩的美學詮釋](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/24.pdf)|吳幸姬|209
[韋莊在杜詩接收史上的位置——以《又玄集》為中心](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/25.pdf)|王偉|222
[元人<白翎雀>樂舞詩初探](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/26.pdf)|高林廣|232
[抒情傳統與中國戲劇](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/27.pdf)|曹飛|243
[貴族時代與平民社會—《三國演義》形成的文化歷程](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/28.pdf)|梁中效|250
[一杯為品──《紅樓夢》的茶文化](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/29.pdf)|林素玟|260
[明代隋唐歷史題材小說的文體探索](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/30.pdf)|雷勇|274
[清代神韻與桐城「詩、學」論域的分歧──以王士禛、梅曾亮為討論對象](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/31.pdf)|曾守正|284
[陳忠實文學的當代意義與《白鹿原》的超越性價值](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/32.pdf)|韓偉|296
[建國後新文學家的舊體詩寫作轉向](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/33.pdf)|李仲凡|302
[王國維之“活文學”觀芻議](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/34.pdf)|孟偉|311
[解析《催眠大師》敘事結構三部曲](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/35.pdf)|黃淑貞|324
[「非虛構」的深度如何可能？─馬奎斯《百年孤寂》的一種會通反思](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/36.pdf)|黃文倩|335
[少女阿玉與街的變遷：作為成長小說的李渝「溫州街系列」](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/37.pdf)|戴華萱|347
[法藏〈華嚴經明法品內立三寶章〉的佛性思想探討](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/38.pdf)|釋天觀|358
[袾宏《阿彌陀經疏鈔》「通序大意」的譬喻所彰顯的淨業修行意涵](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/39.pdf)|釋養行|370
[吳澄《道德真經註》的詮解向度──以「道德」為討論中心](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/40.pdf)|楊筑|383
[以陰陽道之興盛看唐代國教道教之式微](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/41.pdf)|楊馨賢|394
[近代學派論爭之迴響──錢鍾書南北文學論之解構傾向](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/42.pdf)|賴位政|401
[曹操形象在宋詞中的接受研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/43.pdf)|史秀洋|413
[尺寸千里：王昌齡詩論對山水景象之探討](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/44.pdf)|曾令愉|424
[追憶、想像與認同：明代「後臺閣體」的詩歌書寫探析](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/45.pdf)|許逢仁|436
[《閱微草堂筆記》中的狐女形象](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/46.pdf)|田于庭|448
[《三國演義》中的說客及其遊說藝術](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/47.pdf)|羅曉芳|459
[北方謙三の『三国志』の創作視点について](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/48.pdf)|陳卓然|470
[淺析“祥子”和“高興”的城市生存悲劇](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/49.pdf)|侯旭|483
[文化驅動與社會行為——淺析《新教倫理與資本主義精神》](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/50.pdf)|李雪嬌|490
[抗爭與考驗—美狄亞故事與中國“難題求婚”型故事的比較](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/51.pdf)|陳禕滿|497
[『玉水物語』と「封三娘」の比較─影響関係に関する有無の再検討を中心に](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/52.pdf)|穆雪梅|505
[環境文学についての考察―『被遺忘的一九七九：台湾油症事件 30年』（2010年）を中心に](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/53.pdf)|金星|518
[東亞漢學研究學會章程](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/54.pdf)||530
[第三屆東亞漢學研究學會理事會名單](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/55.pdf)||532
[《東亞漢學研究》投稿要求與撰稿格式](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/56.pdf)||534
[Contents](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/57.pdf)||537
[執筆者一覽](http://xiaoan.web.fc2.com/dongyahanxue/paper/no8/58.pdf)||540


### 第7號 2017年4月
[第7號 2017年4月 臺灣臺中](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/menu.pdf)

Title|Author|Page
---|---|---
[卷首語（一）](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/1.pdf)|黃淑貞|i
[從西方學術理論看《詩經》的多元詮釋](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/2.pdf)|黃忠慎|1
[中國語境下身體美學問題研究新進展](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/3.pdf)|徐向陽|11
[蘇軾《續歐陽子朋黨論》作年論析](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/4.pdf)|余曆雄|24
[宇宙生成的圖式－－以王道《老子億》四十二章為論述之核心](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/5.pdf)|江淑君|34
[憨山德清注《莊》思想中的聖人觀—以〈大宗師〉為考察對象](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/6.pdf)|李懿純|47
[宋代士人古琴論述與「直溯三代」理想析探](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/7.pdf)|郭玲妦|59
[古代文學中的開封城市敘述](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/8.pdf)|張鴻聲王文勳|72
[電視劇中的城市想像](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/9.pdf)|王利麗|82
[《金瓶梅》 女性身體書寫的反思—以「性別政治」、「身體感知」為思考角度](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/10.pdf)|林偉淑|91
[周瘦鵑小說的底層關懷](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/11.pdf)|紀俊龍|102
[唐代「女報親仇」故事初探](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/12.pdf)|林溫芳|114
[“明清樂”在長崎的傳播與變遷](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/13.pdf)|王維|129
[“人民社會主義”論爭述評](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/14.pdf)|祁建民|141
[《白鹿原》：文學經典及其“未完成性”](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/15.pdf)|周燕芬|152
[人的教育還是公民教育？——魯迅與國家主義教育思想](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/16.pdf)|姜彩燕|159
[漢日否定疑問與反問的韻律區別特徵——對名詞中心語疑問句的聲學考察](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/17.pdf)|楊曉安|173
[《唐話纂要》中“話”作為漢語教學單位的韻律分析](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/18.pdf)|方環海 沈玲 黃莉萍|185
[二里頭文化及其同時期陶器文字性符號研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/19.pdf)|王暉 高芳|196
[論漢語言文字中的古代洪水記憶](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/20.pdf)|山本 周|208
[聞一多文化選擇的獨特性及其歷史價值](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/21.pdf)|逄增玉|219
[1940年代：沈從文的思想與創作](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/22.pdf)|趙學勇|230
[民國時期回族社會的鄭和闡釋——以回族報刊為中心的考察](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/23.pdf)|王繼霞|244
[簡媜散文虛實交錯的敘述策略析論──以《女兒紅》為例](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/24.pdf)|賴昭吟|252
[阿城小說與中國抒情傳統](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/25.pdf)|夏雪飛|263
[莫言在美國的傳播與接受](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/26.pdf)|任虎軍|274
[聚焦與縮影──黃春明〈死去活來〉所示現的隱喻意涵](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/27.pdf)|林淑貞|289
[許宣大改造：從物質研究視角重探〈白娘子永鎮雷峰塔〉](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/28.pdf)|林怡君|303
[〈五臧山經〉的標準動物與熟知動物──以《爾雅》作為參照對象的討論](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/29.pdf)|陳峻誌|315
[文津閣《四庫全書》影本所載謄錄生員資訊](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/30.pdf)|黃明理|330
[唐代的采詩制度](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/31.pdf)|陳正平|343
[錢澄之《田間易學》中的經世致用](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/32.pdf)|張曉芬|359
[乙未割臺戰爭詩中遭遇戰亂的切身經驗類型書寫](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/33.pdf)|張柏恩|371
[林紓古文中的婦女形象及其道德教化意涵](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/34.pdf)|黃雅雯|382
[《洛陽伽藍記》中文化融合現象的考察與探源](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/35.pdf)|黃婕|396
[日本九州與中國學‧東亞漢學會議的緣起](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/36.pdf)|連清吉|408
[劉孝標《世說注》徵引僧人別傳述微——兼論僧人別傳在中世僧傳文獻中的地位](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/37.pdf)|陽清 劉靜|412
[鄭板橋論藝術家的主體修養](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/38.pdf)|許松|423
[文獻的音類結構與時空擬推：以《諧聲韻學》為例](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/39.pdf)|呂昭明|432
[六朝天子「后」、「妃」死後神主入廟配食議論考](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/40.pdf)|陳燕梅|447
[內藤湖南近世文學地勢二元中心論](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/41.pdf)|陳凌弘|456
[從自治到統制：十七世紀長崎唐人管理變化](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/42.pdf)|呂品晶|469
[殖民主義陰影下的家園：時間模式與空間意象——對東北淪陷區文學中家族敘事的一種考察](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/43.pdf)|包學菊|482
[元儒胡一桂對朱熹易學的增補與匡正](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/44.pdf)|陳詠琳|490
[芳賀矢一の日本漢文学史論](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/45.pdf)|沈日中|501
[第七屆東亞漢學既第十六屆社會與文化國際學術研討會綜述](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/46.pdf)|賴位政|515
[東亞漢學研究學會章程](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/47.pdf)||525
[第三屆東亞漢學研究學會理事會名單](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/48.pdf)||527
[《東亞漢學研究》投稿要求與撰稿格式](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/49.pdf)||529
[Contents](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/50.pdf)||532
[執筆者一覽](http://xiaoan.web.fc2.com/dongyahanxue/paper/no9/51.pdf)||535


### 第8號 2018年9月
[第8號 2018年9月 日本札幌](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/menu.pdf)

Title|Author|Page
---|---|---
[卷首語](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/1.pdf)|連清吉|i
[關於民國初年以來舊文學的思考](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/2.pdf)|龔鵬程|1
[中國科幻小說的精英敘事——以王晉康的《生死平衡》為例](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/3.pdf)|馮 鴿|22
[說“寤生”—民俗學視野下的生育禁忌信仰探析](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/4.pdf)|吕亚虎|29
[魯迅“父範”命題的緣起及意義](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/5.pdf)|姜彩燕|37
[胡風、唐弢與魯迅遺產--以藏書為考察視角](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/6.pdf)|李明剛 張鴻聲|48
[幽默和諷刺之于賈平凹鄉土敘事的藝術貢獻](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/7.pdf)|周燕芬 李斌|59
[時空意識與老派市民家國觀念的更生和嬗變——以老舍小說《四世同堂》為中心](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/8.pdf)|逄增玉 孫曉平|69
[讀房偉的抗戰小説《中国野人》① ②](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/9.pdf)|塩旗伸一郎|79
[臺灣眷村電影的歷史流變與文化主題](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/10.pdf)|王利麗|91
[毛澤東的階級劃分戰略與中國傳統社會結構](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/11.pdf)|祁建民|100
[鏡像下的存在感受──帝國與民國更迭下的詩話書寫類型](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/12.pdf)|林淑貞|110
[新中國建國早期的勞動書寫美學與淵源初探──以《三里灣》、《創業史》及《山鄉巨變》為考察的起點](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/13.pdf)|黃文倩|123
[成長的凝視──論〈少女小漁〉的成長小說特質](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/14.pdf)|黃培青|134
[三毛旋風──以女性讀者的角度探析七 0 年代的首波三毛熱](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/15.pdf)|戴華萱|147
[沒有神所在的一道微光──韓愛姐在《金瓶梅》中的隱喻](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/16.pdf)|林偉淑|157
[草叢古塚臥秋風——論紀信名聲](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/17.pdf)|劉錦源|167
[林雲銘《莊子因》解莊特色及其立場探論](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/18.pdf)|李懿純|179
[《玉坡奏議》與明代正德嘉靖年間的朝廷政治](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/19.pdf)|賈三強|191
[唐玄宗《道德真經》注、疏的幾點觀察](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/20.pdf)|江淑君|202
[竺法維及其《佛國記》探賾](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/21.pdf)|陽 清|213
[臺灣敦煌寫卷《法華經義記》研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/22.pdf)|李幸玲|223
[南朝正史中蘊含的農業政策──以《宋書》為例](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/23.pdf)|吳麗雯|233
[理想譬喻世界的建立與經典詮釋──毛鄭說詩的手法舉隅](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/24.pdf)|林菁菁|244
[《周易•系辭下》所載發明創造的古文字驗證](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/25.pdf)|邵 英|259
[莫今文《尚書》中的話題結構及話題性](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/26.pdf)|周國強|269
[豔緣截斷與喜謔轉關—論雙紅堂藏清末四川唱本《花仙劍》兼北碚圖書館藏民國抄本](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/27.pdf)|丁淑梅|279
[近代文人的藏書目錄](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/28.pdf)|馮 佳|288
[陝西碑刻文獻數字化及其前景展望](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/29.pdf)|白寬犁|299
[漢語名詞獨詞句的調域變化](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/30.pdf)|楊曉安|307
[日本漢檢讀解文的話題構成分析——以2007年-2016年讀解文為例](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/31.pdf)|高 芳|319
[儒家經典於當代日本社會——作為「萬能藥」的人間《論語》學](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/32.pdf)|金培懿|329
[東亞「漢學」的問題與現代意義](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/33.pdf)|藤井倫明|340
[日本漢文學史的時代區分論](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/34.pdf)|沈日中|350
[韓國書藝所面臨的問題與「世界書藝全北雙年展」對解決問題的作用](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/35.pdf)|金炳基|361
[《大儀覺迷錄》與《闡義昭鑑》之比較](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/36.pdf)|李京勛|373
[朱熹《資治通鑑綱目》唐史卷名家論贊析評──以「守禮」、「寡慾」為討論主題](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/37.pdf)|姚彥淇|385
[情為何物──從「白石有格而無情」看《人間詞話》所論之「情」](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/38.pdf)|許嘉瑋|400
[政治美學的兩張面孔——論“翻身”敘事中文學與圖像的互文性](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/39.pdf)|李躍力|411
[都市经验与“漫游者”身份意识：论纪弦（路易士）的文学活动](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/40.pdf)|王小平|421
[掙扎·堅守·無奈——從謝冰瑩棄編《黃河》看國統區文化人的生存困境](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/41.pdf)|馮 超|430
[安徽竹友齋刊本《梨園集成》編選考](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/42.pdf)|李東東|442
[女鬼的「命運反叛」——「鬼妻化生型」故事研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/43.pdf)|劉亞惟|453
[論龔用卿《使朝鮮錄》中行旅地誌與外交儀制的互見書寫](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/44.pdf)|黃鈴棋|465
[井上哲次郎的伊藤仁齋論](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/45.pdf)|陳凌弘|476
[小川環樹の中国文学の風景論](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/46.pdf)|梁 雨|489
[東欧危機における中国共産党の関与と平和五原則](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/47.pdf)|杜世鑫|501
[朝鮮時期管仲評價研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/48.pdf)|許 寧|509
[《論語・學而》11 章解釋小考-以「其」字為中心](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/49.pdf)|朴晞娜|520
[第八屆東亞漢學國際學術研討會綜述](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/50.pdf)|陳燕梅|535
[東亞漢學研究學會章程](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/51.pdf)||537
[第四屆東亞漢學研究學會理事會名單](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/52.pdf)||539
[《東亞漢學研究》投稿要求與撰稿格式](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/53.pdf)||541
[Contents](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/54.pdf)||544
[執筆者一覽](http://xiaoan.web.fc2.com/dongyahanxue/paper/no10/55.pdf)||548


### 特別號 2018年12月
[特別號 2018年12月 韓國全州](http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/index.pdf)

Title|Author|Page
---|---|---
[卷首語](http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/1.pdf)|連清吉|i
4 차 産業革命時代의 藝術- 以前에 대한 省察과 以後의 代案 摸索|金炳基|1
[傳統時期 漢中 兩國의 逐疫의 메커니즘(mechanism)](http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/3.pdf)|鄭元祉|17
頤齋 黃胤錫의 佛敎觀과 活動|朴順哲|32
[中國高僧與書法文化窺探](http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/5.pdf)|李繼凱|45
“大鬧”：“熱鬧”的內在結構與文化編碼]|李永平|50
隋唐時期長安的戲劇類表演|賈三強|62
[論康海和王九思的自壽曲](http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/8.pdf)|張文利|72
[中國鄉村振興中的鄉賢——以河南蘭考、陝西西鄉、貴州普定三縣為例](http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/9.pdf)|李 銳|81
[白居易經行商州之因由及其商州詩之多維情懷與創作特色](http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/10.pdf)|付興林|90
[《文心雕龍》張華批評述論](http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/11.pdf)|高林廣|102
[流動的風景:民國中等國文教材中的巴金選篇](http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/12.pdf)|劉緒才|110
[論蒙古史詩的文學治療功能](http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/13.pdf)|王艷鳳|115
[處實行權：唐玄宗《道德真經》注、疏的權實思想](http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/14.pdf)|江淑君|123
西王母與伴隨的瑞獸珍禽――以明清《山海經》圖像為例|鹿憶鹿|133
地誌書寫與異族敘述：以魏濬《西事珥》、《嶠南瑣記》為主的考察|范宜如|143
[中國 70 後小說中的城鄉流動書寫初探](http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/17.pdf)|石曉楓|153
[日原利國的公羊傳俠勇論](http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/18.pdf)|連清吉|162
韻律區別手段的同異與主次關係--以兩例漢日韻律手段比較為例|楊曉安|170
[抗戰時期郭沫若和茅盾的文化活動](http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/20.pdf)|鐘海波|180
[提純與魅化：“紅色經典”的“文”“圖”互動](http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/21.pdf)|李躍力|188
[論地理文化環境對陸遊漢中詩風變遷之影響](http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/22.pdf)|宮臻祥|198
“白話”影像與啟蒙理念的視覺再現：一位第三世界現代主義者黎民偉的早期紀錄片實踐|張 穎|207
[唐代吳郡陸氏的遷徙流動](http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/24.pdf)|高淑君|217
《金瓶梅》後二十回的敘事概念與指涉意義|林偉淑|226
[大陸改革開放後文學批評對俄蘇文學正典的接受與會通作用──以洪子誠和王曉明的論著為例](http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/26.pdf)|黃文倩|235
“九七”前後香港女作家的家園意識－－以西西、黃碧雲為中心|鄭 貞|246
[論王弘撰的金石學成就及影響](http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/28.pdf)|李向菲|257
[《文季》系列重探——以尉天驄為觀察中心](http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/29.pdf)|戴華萱|264
[論日本漢學家白川靜的《說文》研究及其影響](http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/30.pdf)|黃庭頎|273
[淺論闢穀的養生作用](http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/31.pdf)|馬沂春|282
[翁方綱〈與姬川郎中論何、李書〉的漁洋話頭](http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/32.pdf)|賴位政|290
[陝甘寧文人書畫藝術](http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/33.pdf)|王 奎|300
喻鑒現世：論宋濂《燕書》的互文性書寫|許逢仁|308
[《西遊記》中的妖魔修道敘述及其意涵](http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/35.pdf)|洪恩敬|318
詞要清空：張炎《詞源》的佛教思維|趙雄健|328
[《熔裁》對留學生寫作教學的啟發及指導意義探究](http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/37.pdf)|馬英驍|337
《朴通事諺解》와《朴通事新釋諺解》중 속어의 사용오류 분석-‘狗有濺草之恩，馬有垂繮之報’와‘福不至萬事難’을 중심으로|劉 禹|347
蘇洵 <名二子說> 연구— 顧名思義的 作名 來歷과 二子의 「父訓」수용양상을 중심으로|朴晞娜|357
[井上哲次郎論伊藤仁齋在江戶儒學史上的地位](http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/40.pdf)|陳凌弘|371
[書院楹聯之當代校園文化建構參與性研究—以鵝湖、信江書院為例](http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/41.pdf)|方 雲|382
[漢語前輔音對一級元音的影響](http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/42.pdf)|楊 航|392
[從商業思想淺談韓非義利觀](http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/43.pdf)|黃 月|399
[東亞漢學研究學會章程](http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/44.pdf)||409
[第四屆東亞漢學研究學會理事會名單](http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/45.pdf)||411
[《東亞漢學研究》投稿要求與撰稿格式](http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/46.pdf)||413
[Contents](http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/47.pdf)||415
[執筆者一覽](http://xiaoan.web.fc2.com/dongyahanxue/paper/quanbei/papers/48.pdf)||418


### 特別號 2019年06月
[特別號 2019年06月 中國桂林](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/index.pdf)

Title|Author|Page
---|---|---
[卷首語](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/1.pdf)|陳學超|i
[魏晉南北朝時期華胡音樂交流及其機制](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/2.pdf)|吳大順|1
[西漢初期士不遇文學主題及士人遭遇探究](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/3.pdf)|王 璟|16
[孫一元及其《太白山人漫稿》版本考述](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/4.pdf)|趙望秦|25
[論量移忠州之於白居易的政治意義](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/5.pdf)|付興林|33
[歌與詩的交響──論夏宇詩詞交錯的愛情影像](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/6.pdf)|黃培青|45
[唐《于哲墓誌》考釋及所載民族邊疆事發微](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/7.pdf)|李勝振|60
[昭君故事演變及其文學情感價值論](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/8.pdf)|劉 偉|69
[桂臺地緣網路文化認同測度調查報告](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/9.pdf)|劉 惠 王瑞琪 鄭志發 羅婧|81
[漢水流域歷史劇劇目初探](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/10.pdf)|王建科|94
[等差与亲情——“礼”的原理再论](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/11.pdf)|祁建民|102
[人,不能真正逃出他的故鄉—王鼎鈞《左心房漩渦》的鄉愁美學與藝術創](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/12.pdf)|程國君|110
[解析電影《七月與安生》三大交錯手法的改編策略](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/13.pdf)|黃淑貞|117
[謝霜天《梅村心曲》的客家女性形象研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/14.pdf)|戴華萱|127
[美國與中國霍桑研究比較](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/15.pdf)|李瑞春|137
[日原利國的公羊傳王霸論](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/16.pdf)|連清吉|145
[20 世紀 80 年代以後的日本寶卷研究俯瞰―以國會圖書館館藏文獻和日本知網文獻為對象—](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/17.pdf)|何 彬|154
[東去的語脈(一)——關於日本近世黃檗宗唐音](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/18.pdf)|楊春宇|165
[史前南方地區陶器文字性符號研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/19.pdf)|王 暉|177
[古文“字”字形義構建解析](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/20.pdf)|呂亞虎|190
[疑問副詞“怎麼”的時長變化與語義轉換](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/21.pdf)|楊曉安|201
[漢語味覺類詞語的文化意義](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/22.pdf)|邵 英|212
[美國在漢學傳播中的影響和作用](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/23.pdf)|馬曉華|219
[漢日並列結構順序的制約因素](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/24.pdf)|雷桂林|230
[從邏輯的角度看漢語否定詞位置偏誤](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/25.pdf)|何 偉 王海燕|244
[中國語言資源保護視閾下省際毗鄰區域方言研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/26.pdf)|張 璐|249
[從語言認同視角看閩南方言的海外傳播](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/27.pdf)|王 曦|257
[文化磨合视野中的“文學终结论”](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/28.pdf)|孫 旭|266
[中國漢字與西方字母文字起源與認知差別研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/29.pdf)|楊 琳|275
[「日本文化とは何ぞや」から内藤湖南の日本文化史論を探る](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/30.pdf)|劉 璐|285
[横浜、神戸、長崎における中国獅子舞 −−−伝承の実態とその役割−−−](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/31.pdf)|凉松 育子|295
[21 世紀漢語課堂遊戲教學法研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/32.pdf)|石 旭 俞燕君|308
[“X 裡 X 氣”構式研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/33.pdf)|馬沙木嘎|314
[再論網絡語言的修辭特點](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/34.pdf)|刘自晖|321
[語言模因視域下的“佛系 XX”流行現象](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/35.pdf)|于樂琪|326
[從“維權談判”看論辯語境下的篇章話題結構](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/36.pdf)|蔡宗翰|336
[論蘇軾詞中的空間敘事](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/37.pdf)|羅浩春|346
[《全清詞》收錄女性詞勘誤、補遺](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/38.pdf)|徐 夢|354
[東亞漢學研究學會章程](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/39.pdf)||364
[第四屆東亞漢學研究學會理事會名單](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/40.pdf)||366
[《東亞漢學研究》投稿要求與撰稿格式](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/41.pdf)||368
[Contents](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/42.pdf)||370
[執筆者一覽](http://xiaoan.web.fc2.com/dongyahanxue/paper/2019guilin/43.pdf)||372


### 第9號 2019年11月
[第9號 2019年11月 日本名古屋](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/menu.pdf)

Title|Author|Page
---|---|---
[卷首語](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/1.pdf)|李 繼 凱|i
[在文化磨合中建構中國近代文體](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/2.pdf)|李 繼 凱|1
[廣州題材電視劇中的城市空間與文化意義](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/3.pdf)|王利麗|7
[1960 年代農村幹部的“階級”話語](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/4.pdf)|祁建民|15
[疑問副詞“怎麼”的語義轉換與音高變化](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/5.pdf)|楊曉安|24
[中國學習者應注意的日語漢字問題](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/6.pdf)|張 樺|34
[“說得上話”與“說不上話”——《一句頂一萬句》中楊百順的話語交際特點](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/7.pdf)|劉 惠辛儒靖|40
[香港社會語言生態的歷史嬗變](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/8.pdf)|錢 芳|49
[《金瓶梅詞話》與明朝萬曆年間豔情小說性描寫比較研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/9.pdf)|胡衍南|55
[《醋葫蘆》的世情書寫](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/10.pdf)|林偉淑|65
[陳舜臣『秘本三国志』の創作視点について](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/11.pdf)|陳卓然|75
[君臣人身依附契約關係的建立:西周冊命禮性質研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/12.pdf)|王 晖|85
[虛極至道:唐玄宗《道德真經》注、疏的妙本義](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/13.pdf)|江淑君|95
[漢代隱逸文化與政治、社會評價的「互動關係」](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/14.pdf)|朱錦雄|105
[浙西詞派初期之詞體認知與模習典範探賾](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/15.pdf)|許嘉瑋|115
[清代以來沈德符《萬曆野獲編》研究的回顧與思考](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/16.pdf)|杜學林|125
[論中國當代首批蒙古族作家的生成](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/17.pdf)|孫 靜|135
[農業文明視角下的城市意象—以賈平凹小說為例](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/18.pdf)|陳 靜馬春燕|144
[“牙門、渠門與牙旗”辨釋](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/19.pdf)|閆 艷|153
[中國京劇選本的副文本廣告](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/20.pdf)|李東東|162
[西安地區出土商周金文資料瑣談](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/21.pdf)|王 帥|171
[林紓筆記小說中的臺灣地景](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/22.pdf)|黃雅雯|177
[論《日瓦戈醫生》的勞動書寫與美感](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/23.pdf)|黃文倩|190
[謝霜天《梅村心曲》中的客家女性日常探析](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/24.pdf)|戴華萱|199
[故宮藏楊守敬赴日所蒐宋、元版史籍及其遞藏脈絡](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/25.pdf)|許媛婷|209
[龜井昭陽論《左傳》「《春秋》之稱」 —兼論竹添光鴻《左氏會箋》之承與變](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/26.pdf)|宋惠如|219
[井上哲次郎之荻生徂徠政治論的哲學思考](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/27.pdf)|陳凌弘|230
[漢語語言多樣性的保持及傳播途徑研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/28.pdf)|張 璐|240
[歐陽修《六一詩話》文學地位的確認](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/29.pdf)|宮臻祥|247
[東亞譯者對 William Le Queux `Recounts the mystery of a front door`的翻譯與改造](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/30.pdf)|詹宜穎|258
[青年學者獎候選人小說研究的“內”與“外”——評小南一郎《唐代傳奇小說論》](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/31.pdf)|李彥姝|269
[隱匿的太伯:六朝吳地太伯廟考察](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/32.pdf)|何維剛|276
[論《三國演義》105 回後「魏之忠臣」人物及其敍事意義](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/33.pdf)|曾世豪|285
[博士生論壇陜北民歌語言口頭程式研究——語詞程式](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/34.pdf)|張文倩|293
[吳宓的“影因”](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/35.pdf)|王 奎|305
[新疆曲子戲詞匯特徵研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/36.pdf)|馬 靜|312
[馬禮遜《華英字典》中文化負載詞譯介研究](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/37.pdf)|楊 琳|324
[為情造文:論溫庭筠〈菩薩蠻〉十四首的政治解讀](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/38.pdf)|趙雄健|331
[從種姓制度到眾生無別—以漢譯佛典「種姓起源神話」論述為核心](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/39.pdf)|劉鐔靖|339
[新舊交替中的藝術人生觀:宗白華「意境」論與王國維「境界」論之關係](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/40.pdf)|張 豔|348
[斯波六郎の中国文学の孤独感について](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/41.pdf)|梁 雨|357
[第九屆東亞漢學國際學術研討會綜述](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/42.pdf)|賈三強許嘉瑋|368
[東亞漢學研究學會章程](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/43.pdf)||372
[第四屆東亞漢學研究學會理事會名單](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/44.pdf)||374
[《東亞漢學研究》投稿要求與撰稿格式](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/45.pdf)||376
[Contents](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/46.pdf)||378
[執筆者一覽](http://xiaoan.web.fc2.com/dongyahanxue/paper/no11/47.pdf)||381



[nagasaki]: http://www.nagasaki-u.ac.jp "長崎大学トップページ"
[dongyahanxue]:http://xiaoan.web.fc2.com/dongyahanxue/dyhx.html "東亞漢學研究學會"

<!-- End -->
