# Historical Journal of Massachusetts


>The [Historical Journal of Massachusetts][historical-journal] (HJM) is a peer-reviewed journal published twice a year by the History Department at [Westfield State University](https://www.westfield.ma.edu). As the only scholarly journal devoted exclusively to the history of Massachusetts, it fills an important role. Since 1972, the journal has published articles by well-known historians, along with cutting-edge research by graduate students, middle and high school history teachers, amateur history buffs, and countless local historians who have labored to uncover and rediscover the fascinating history of a pioneering state. **Due to the generous support of Westfield State University, an annual subscription is only $12.00.**

## TOC
1. [Article Archives](#article-archives)  
2. [Questia](#questia)  
3. [Change Log](#change-log)  

數據提取腳本見 [Sehll Script](/scripts/HistoricalJournalofMassachusetts.md)。


## Article Archives
頁面 [Article Archives](http://www.westfield.ma.edu/historical-journal/article-index-1976-2013/) 提供PDF格式文件下載。

Date|Title|Author|PDF
---|---|---|---
Summer 2018, Vol. 46, No. 2|Editor’s Choice: "Boston’s New Immigrants and New Economy, 1965-2015"|Marilynn S. Johnson|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/11/2018-Summer-Johnson-Boston’s-New-Immigrants-and-New-Economy-1965-2015.pdf)
Summer 2018, Vol. 46, No. 2|Romancing the Stone: Invented Irish and Native American Memories in Northampton|Robert E. Weir|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/11/2018-Summer-Weir-Romancing-the-Stone.pdf)
Summer 2018, Vol. 46, No. 2|When the Chinese Came to Massachusetts: Representations of Race, labor, religion, and Citizenship in the 1870 Press|Mary M. Cronin|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/11/2018-Summer-Cronin-When-the-Chinese-Came-to-Massachusetts.pdf)
Summer 2018, Vol. 46, No. 2|Hatfield’s Forgotten Past: The Porter-McLeod Machine Tool Company and the Connecticut Valley Industrial Economy, 1870-1970|Robert Forrant|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/11/2018-Summer-Forrant-Hatfield’s-Forgotten-Industrial-Past.pdf)
Winter 2018, Vol. 46, No. 1|Photo Essay: "The Rise and Demise of the Connecticut River Valley’s Industrial Economy"|Robert Forrant|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/11/2018-Winter-Forrant-The-Rise-and-Demise-of-the-Connecticut-River-Valley’s-Industrial-Economy.pdf)
Winter 2018, Vol. 46, No. 1|Editor’s Choice: "Dissenting Puritans: Anne Hutchinson and Mary Dyer"|Francis J. Bremer|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/11/2018-Winter-Bremer-Dissenting-Puritans.pdf)
Winter 2018, Vol. 46, No. 1|‘This Would Be a Ghost Town’: Urban Crisis and Latino Migration in Lawrence, 1945-2000|Llana Barber|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/11/2018-Winter-Barber-‘This-Would-Be-a-Ghost-Town’.pdf)
Winter 2018, Vol. 46, No. 1|New Bedford’s Infamous 1983 Rape Case: Defending the Portuguese-American Community|Mia Michael|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/11/2018-Winter-Michael-New-Bedfords-Infamous-1983-Rape-Case.pdf)
Winter 2018, Vol. 46, No. 1|John Adams, Political Moderation, and the 1820 Massachusetts Constitutional Convention: A Reappraisal|Arthur Scherr|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/11/2018-Winter-Scherr-John-Adams-Political-Moderation-and-the-1820-Massachusetts-Constitutional-Convention.pdf)
Winter 2018, Vol. 46, No. 1|Teaching Resource: "New England Beginnings: Commemorating the Cultures that Shaped New England"|Francis J. Bremer|
Summer 2017, Vol. 45, No. 2|Editor’s Choice: "Lexington, Worcester, and the American Revolution: Debunking the Myth of the Shot Heard ‘Round the World"|Ray Raphael|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/11/2017-Summer-Raphael-Lexington-Worcester-and-the-American-Revolution.pdf)
Summer 2017, Vol. 45, No. 2|‘The Unity of the Republic and the Freedom of an Oppressed Race’: Fitchburg’s Civil War Soldiers’ Monument, 1874|Darren Barry|
Summer 2017, Vol. 45, No. 2|Mabel Loomis Todd: The Civic Impulses and Civic Engagement of an Accidental Activist|Julie Dobrow|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/11/2017-Summer-Dobrow-Mabel-Loomis-Todd.pdf)
Summer 2017, Vol. 45, No. 2|The Great Depression in the Northern Berkshires: The New Deal, Textile Union Organizing, and a Pro-Labor Mayor|Maynard Seider|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/11/2017-Summer-Seider-The-Great-Depression-in-the-North-Berkshires.pdf)
Summer 2017, Vol. 45, No. 2|William Pynchon, the Agawam Indians, and the 1636 Deed for Springfield|David M. Powers|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/11/2017-Summer-Powers-William-Pynchon-the-Agawam-Indians-and-the-1636-Deed-for-Springfield.pdf)
Winter 2017, Vol. 45, No. 1|Coming soon!||
Summer 2016, Vol. 44, No. 2|Editor’s Choice: "The Migration of Former Slaves to Worcester: Hopes and Dreams Deferred, 1862-1800"|Janette Thomas Greenwood|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/11/2016-Summer-Greenwood-The-Migration-of-Former-Slaves-to-Worcester.pdf)
Summer 2016, Vol. 44, No. 2|Photo Essay: "Yankee Brutalism: Concrete Architecture in new England, 1957-1977"|Brian M. Sirman|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/11/2016-Summer-Sirman-Yankee-Brutalism.pdf)
Summer 2016, Vol. 44, No. 2|The Making of an Irish and a Jewish Boston, 1820-1900|Meaghan Dwyer-Ryan|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/11/2016-Summer-Dwyer-Ryan-The-Making-of-an-Irish-and-a-Jewish-Boston-1820-1900.pdf)
Summer 2016, Vol. 44, No. 2|Hannah Packard James, Newton Librarian: Economic Motives of Nineteenth-Century Professional Women|Bernadette A. Lear|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/11/2016-Summer-Lear-Hannah-Packard-James-Newton-Librarian.pdf)
Summer 2016, Vol. 44, No. 2|The Pathos of Distance: Memory and Revision in S. N. Behrman’s The Worcester Account|Kent p. Ljungquist|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/11/2016-Summer-Ljungquist-The-Pathos-of-Distance.pdf)
Summer 2016, Vol. 44, No. 2|Captives on the Move: Tracing the Transatlantic Movements of Africans from the Caribbean to Colonial New England by Kerima M. Lewis||[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/11/2016-Summer-Lewis-Captives-on-the-Move.pdf)
Summer 2016, Vol. 44, No. 2|Book Reviews||
Winter 2016, Vol. 44, No. 1|Editor’s Choice: "The New Boston: A People’s History"|Jim Vrabel|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/11/2016-Winter-Vrabel-The-New-Boston.pdf)
Winter 2016, Vol. 44, No. 1|Photo Essay: "Sculptor Theodora Alice Ruggles Kitson: ‘A Woman Genius'"|Christine C. Neal|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/11/2016-Winter-Neal-Sculptor-Theodora-Alice-Ruggles-Kitson.pdf)
Winter 2016, Vol. 44, No. 1|‘Hospitality Is the Best Form of Propaganda’: German Prisoners of War in Western Massachusetts, 1944-1946|John C. Bonafilia|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/11/2016-Winter-Bonafilia-Hospitality-Is-the-Best-Form-of-Propaganda.pdf)
Winter 2016, Vol. 44, No. 1|Stricken: The Impact of Disease on Two Massachusetts Families, 1911-50|Anita C. Danker|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/11/2016-Winter-Danker-Stricken-The-Impact-of-Disease-on-Two-Massachusetts-Families-1911-50.pdf)
Winter 2016, Vol. 44, No. 1|The Women of Hopedale Sewing Circle, 1848-63|Linda H. Hixon|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/11/2016-Winter-Hixon-The-Women-of-the-Hopedale-Sewing-Circle-1848-63.pdf)
Winter 2016, Vol. 44, No. 1|The Dilemma of Interracial Marriage: The Boston NAACP and the National Equal Rights League, 1912-1927|Zebulon Miletsky|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/11/2016-Winter-Miletsky-The-Dilemma-of-Interracial-Marriage.pdf)
Winter 2016, Vol. 44, No. 1|Book Reviews||
Summer 2015, Vol. 43, No. 2|Editor’s Choice: "Protest Politics: Liberal Activism in Massachusetts, 1974-1990"|Robert Surbrug Jr.|
Summer 2015, Vol. 43, No. 2|Photo Essay: "The People’s Schools for Teachers of the People"|Mary-Lou Breitborde and Kelly Kolodny|
Summer 2015, Vol. 43, No. 2|The Puerto Rican Community of Western Massachusetts, 1898-1960|Joseph Carvalho III|
Summer 2015, Vol. 43, No. 2|Phillis Wheatley: Researching a Life|Vincent Carretta|
Summer 2015, Vol. 43, No. 2|Splitting the Vote in Massachusetts: Father Charles E. Coughlin, the Union Party, and Political Divisions in the 1936 Presidential and Senate Elections|Michael C. Connolly|
Summer 2015, Vol. 43, No. 2|Naked Quakers Who Were Not So Naked: Seventeenth-Century Quaker Women in the Massachusetts Bay Colony|Heather E. Barry|
Summer 2015, Vol. 43, No. 2|Book Reviews||
Winter 2015, Vol. 43, No. 1|Photo Essay: "‘Something of the Character Within’: Children of the Swift River Valley"|Sheila Damkoehler|
Winter 2015, Vol. 43, No. 1|Editor’s Choice: "Remembering Massachusetts State Normal Schools: Pioneers in Teacher Education"|Mary-Lou Breitborde and Kelly Kolodny|
Winter 2015, Vol. 43, No. 1|Three Olmsted ‘Parks’ That Weren’t: The Unrealized Emerald Necklace and Its Consequences|Gregory Kaliss|
Winter 2015, Vol. 43, No. 1|Shays’ Rebellion: Reclaiming the Revolution|Tom Goldscheider|
Winter 2015, Vol. 43, No. 1|‘With Good Will Doing Service’: The Charitable Irish Society of Boston (1737-1857)|Catherine B. Shannon|
Winter 2015, Vol. 43, No. 1|The Congregational Way Assailed: The Reverend Thomas Goss in Revolutionary Massachusetts|Robert E. Cray|
Winter 2015, Vol. 43, No. 1|Book Reviews||
Summer 2014, Vol. 42, No. 2|Photo Essay: "A Brief History of Fenway Park"|Kevin Jones and L. Mara Dodge|
Summer 2014, Vol. 42, No. 2|Editor’s Choice: "Remaking Boston, Remaking Massachusetts"|Brian M. Donahue|
Summer 2014, Vol. 42, No. 2|Constructing Legends: Pumpsie Green, Race, and the Boston Red Sox|Robert E. Weir|
Summer 2014, Vol. 42, No. 2|Classicism for the Masses? The Social Dimensions of Revolutionary Boston’s Popular Imagination|Jonathon Derek Awtrey|
Summer 2014, Vol. 42, No. 2|A Fraternity of Patriarchs: The Gendered Order of Early Puritan Massacusetts|Matthew J. Reardon|
Summer 2014, Vol. 42, No. 2|Book Reviews||
Winter 2014, Vol. 42, No. 1|Photo Essay: "Northampton Silk Threads: The Asia Connection"|Marjorie Senechal and Stan Sherer|
Winter 2014, Vol. 42, No. 1|Editor’s Choice: "The Hub’s Metropolis: A Glimpse into Greater Boston’s Development"|James C. O’Connell|
Winter 2014, Vol. 42, No. 1|Mr. Sprague Did Not Think People Would Do It: The Sprague Electric Strike in North Adams, 1970|Maynard Seider|
Winter 2014, Vol. 42, No. 1|Women Reformers and the Limitations of Labor Politics in Massacusetts, 1874-1912|Kathleen Banks Nutter|
Winter 2014, Vol. 42, No. 1|The Lost History of Slaves and Slave Owners in Billerica, Massachusetts, 1655-1790|Christopher M. Spraker|
Winter 2014, Vol. 42, No. 1|Cholera in Worcester: A Study of the Nineteenth- Century Public Health Movement|Alan Ira Gordon|
Winter 2014, Vol. 42, No. 1|Book Reviews||
Summer 2013, Vol. 41, No. 2|Photo Essay: The Robbins Family of East Lexington, Massachusetts:  Furriers and Their Clothing, 1775-1825|Jennifer Swope|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Robbins%20Family.pdf)
Summer 2013, Vol. 41, No. 2|Editor’s Choice: Confronting the Vietnam Draft: A New York Draftee and a Northampton Draft Counselor|Tom Weiner|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Vietnam%20Experiences.pdf)
Summer 2013, Vol. 41, No. 2|The Veteran’s Education Project of Amherst|Robert M. Wilson|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Veterans%20Education%20Project.pdf)
Summer 2013, Vol. 41, No. 2|John F. Kennedy: Public Perception and Campaign Strategy in 1946|Seth M. Ridinger|[PDF](http://www.westfield.ma.edu/mhj/pdfs/John%20F%20Kennedy%20article.pdf)
Summer 2013, Vol. 41, No. 2|Beyond the New England Frontier: Native American Historiography Since 1965|Ethan A. Schmidt|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Beyond%20the%20New%20England%20Frontier.pdf)
Summer 2013, Vol. 41, No. 2|BOOK REVIEWS (12)||[PDF](http://www.westfield.ma.edu/mhj/pdfs/Book%20Reviews.pdf)
Winter 2013, Vol. 41, No. 1|Photo Essay: Furnishing the Frontier: The Material World of the Connecticut River Valley, 1680-1720|Historic Deerfield Curators|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Furnishing%20the%20Frontier.pdf)
Winter 2013, Vol. 41, No. 1|Editor’s Choice: Elinor Frost: A Poet’s Wife |Sandra L. Katz|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Elinor%20Frost.pdf)
Winter 2013, Vol. 41, No. 1|Jonathan Jackson’s Thoughts: A High Federalist Critique of the Philadelphia Constitution|Michael J. Connolly|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Jonathan%20Jackson.pdf)
Winter 2013, Vol. 41, No. 1|Sex and Sin: The Historiography of Women, Gender, and Sexuality in Colonial Massachusetts|Sandra Slater|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Sex%20and%20Sin.pdf)
Winter 2013, Vol. 41, No. 1|Eyewitness to the Mushroom Clouds: A Dorchester Native, the Bomb, and Its Power to Transform an Ordinary Life|Anita C. Danker|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Eyewitness%20to%20the%20Mushroom%20Cloud.pdf)
Winter 2013, Vol. 41, No. 1|BOOK REVIEWS (13)||[PDF](http://www.westfield.ma.edu/mhj/pdfs/Book%20Reviews%20%2813%29.pdf)
2012 Special 40th Anniversary Double Issue, Vol. 40, No. 1 & 2|Photo Essay: The Historical Journal of Massachusetts and Westfield State: A Brief History|L. Mara Dodge|[PDF](http://www.westfield.ma.edu/mhj/pdfs/PHOTOESSAY%20The%20Historical%20Journal%20of%20Mass.pdf)
2012 Special 40th Anniversary Double Issue, Vol. 40, No. 1 & 2|Editor’s Choice: "Our Life’s Work": Rhetorical Preparation and Teacher Training at Westfield State Normal School, 1844–1932 |Beth Ann Rothermel|[PDF](http://www.westfield.ma.edu/mhj/pdfs/EDITORS%20CHOICE%20our%20life's%20work.pdf)
2012 Special 40th Anniversary Double Issue, Vol. 40, No. 1 & 2|Editor’s Choice: Uncovering the Stories of Black Families in Springfield and Hampden County, Massachusetts: 1650–1865|Joseph Carvalho III|[PDF](http://www.westfield.ma.edu/mhj/pdfs/EDITORS%20CHOICE%20Uncovering%20the%20Stories%20of%20black%20families.pdf)
2012 Special 40th Anniversary Double Issue, Vol. 40, No. 1 & 2|Bread, Roses, and Other Possibilities: The 1912 Lawrence Textile Strike|Mark W. Robbins|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Bread,%20roses,%20and%20other%20possibilities.pdf)
2012 Special 40th Anniversary Double Issue, Vol. 40, No. 1 & 2|Huguenot Identity and Protestant Unity in Colonial Massachusetts: The Reverend André Le Mercier and the "Sociable Spirit" (1692-1764)|Paula Wheeler Carlo|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Huguenot%20Identity.pdf)
2012 Special 40th Anniversary Double Issue, Vol. 40, No. 1 & 2|Foreign Affairs and the Ratification of the Constitution in Massachusetts (1789)|Robert W. Smith|[PDF](http://www.westfield.ma.edu/mhj/pdfs/foreign%20affairs.pdf)
2012 Special 40th Anniversary Double Issue, Vol. 40, No. 1 & 2|Bewitched and Bewildered: Salem Witches, Empty Factories, and Tourist Dollars|Robert Weir|[PDF](http://www.westfield.ma.edu/mhj/pdfs/bewitched%20and%20bewildered.pdf)
2012 Special 40th Anniversary Double Issue, Vol. 40, No. 1 & 2|Teaching Resources: Teaching the Salem Witch Trials through Time & Place|Jerra Jenrette, Mary Jo Melvin, Debbie Piper, and Rebecca Schaef|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/06/TEACHING-RESOURCES-Teaching-the-Salem-Witch-Trials.pdf)
2012 Special 40th Anniversary Double Issue, Vol. 40, No. 1 & 2|BOOK REVIEWS (14)||[PDF](http://www.westfield.ma.edu/mhj/pdfs/book%20reviews%20%2814%29.pdf)
2011 Special Double Issue, Vol. 39, No. 1 & 2|Photo Essay: "‘Cranberry Pickers’ Mural at the Wareham Post Office"|Bernice L. Thomas|[PDF](http://www.westfield.ma.edu/mhj/pdfs/PHOTOESSAY%20Cranberrry%20pickers.pdf)
2011 Special Double Issue, Vol. 39, No. 1 & 2|Photo Essay: "A Tale of Two Portraits: Motivations Behind Self-Fashioning in Seventeenth-Century Boston Portraiture"|Susan M. Llewellyn|[PDF](http://www.westfield.ma.edu/mhj/pdfs/PHTOESSAY%20A%20Tale%20of%20Two%20Portraits.pdf)
2011 Special Double Issue, Vol. 39, No. 1 & 2|Revisiting Pocumtuck History in Deerfield: George Sheldon’s Vanishing Indian Act|Margaret M. Bruchac|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Revisiting%20Pocumtuck%20History.pdf)
2011 Special Double Issue, Vol. 39, No. 1 & 2|The Puritan Origins of Black Abolitionism in Massachusetts|Christopher Cameron|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Puritan%20Origins%20of%20Black%20Abolitionism.pdf)
2011 Special Double Issue, Vol. 39, No. 1 & 2|Let the People Remember!: Rhode Island’s Dorr Rebellion and Bay State Politics, 1842-1843|Erik J. Chaput|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Let%20the%20People%20remember%20-%20Rhode%20Island's%20Dorr%20Rebellion%20and%20Bay%20State%20Politics.pdf)
2011 Special Double Issue, Vol. 39, No. 1 & 2|Luther Gulick: His Contributions to Springfield University, the YMCA, and "Muscular Christianity"|Clifford Putney|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Luther%20Gulick.pdf)
2011 Special Double Issue, Vol. 39, No. 1 & 2|The Maya of New Bedford: Genesis and Evolution of a Community, 1980-2010|Lisa Maya Knauer|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/06/The-Maya-of-New-Bedford.pdf)
2011 Special Double Issue, Vol. 39, No. 1 & 2|Senator Edward Kennedy and the "Ulster Troubles": Irish and Irish-American Politics, 1965-2009|Andrew Sanders|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Senator%20Edward%20Kennedy%20and%20the%20Ulster%20Troubles.pdf)
2011 Special Double Issue, Vol. 39, No. 1 & 2|Teaching Resource: Exhuming Hidden History: Sources for Teaching about Slavery in New England|David Lucander|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/06/Teaching-Resources.-Exhuming-Hidden-History.pdf)
2011 Special Double Issue, Vol. 39, No. 1 & 2|BOOK REVIEWS (24)||[PDF](http://www.westfield.ma.edu/mhj/pdfs/Book%20Reviews%20%2824%29.pdf)
Spring 2010, Vol. 38, No. 2|Editor’s Choice: The Mill River Flood of 1874: From Williamsburg to Northampton|Elizabeth M. Sharpe|[PDF](http://www.westfield.ma.edu/mhj/pdfs/EDITORS%20CHOICE%20-%20Mill%20River%20Flood.pdf)
Spring 2010, Vol. 38, No. 2|Photo Essay: "A Million Things to Get Done:" The Skinner Family Servants (Holyoke)|Kate N. Thibodeau|[PDF](http://www.westfield.ma.edu/mhj/pdfs/PHOTOESSAY%20A%20Million%20Things%20To%20get%20Done%20-%20Skinner%20Family%20Servants.pdf)
Spring 2010, Vol. 38, No. 2|Young Patrick A. Collins and Boston Politics after the Civil War|Lawrence W. Kennedy|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/06/Young-Patrick-A.-Collins.pdf)
Spring 2010, Vol. 38, No. 2|Red Riots and the Origins of the Civil Liberties Union of Massachusetts, 1915-1930|Shawn M. Lynch|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Red%20Riots.pdf)
Spring 2010, Vol. 38, No. 2|Esther Forbes’s Rainbow on the Road: Portrait of the Nineteenth Century Provincial Artistby Kent P. Ljungquist||[PDF](http://www.westfield.ma.edu/mhj/pdfs/Esther%20Forbes.pdf)
Spring 2010, Vol. 38, No. 2|Defending the "New England Way": Cotton Mather’s "Exact Map of New England and New York"|Jefferson Dillman|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Defending%20the%20New%20England%20way%20-%20Cotton%20Mather's%20Exact%20Mapp.pdf)
Spring 2010, Vol. 38, No. 2|Teaching Resources: Teaching Massachusetts History: Online Primary Sources and Curriculum|Kate N. Thibodeau|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/06/Teaching-Massachusetts-History.pdf)
Spring 2010, Vol. 38, No. 2|BOOK REVIEWS (19)||[PDF](http://www.westfield.ma.edu/mhj/pdfs/Book%20Reviews%20%2811%29.pdf)
Fall 2010, Vol. 38, No. 1|Photo Essay: In the Details: Style in New England Architecture  by Aaron M. Helfand||[PDF](http://www.westfield.ma.edu/mhj/pdfs/PHOTOESSAY%20-%20In%20the%20Details%20Style%20in%20New%20England%20architecture.pdf)
Fall 2010, Vol. 38, No. 1|Editor’s Choice: The Lizzie Borden Murder Trial: Womanhood as Asset and Liability (Fall River) by A. Cheree Carlson||[PDF](http://www.westfield.ma.edu/mhj/pdfs/lizzie%20borden.pdf)
Fall 2010, Vol. 38, No. 1|Militant Mothers: Boston, Busing, and the Bicentennial of 1976  by Kathleen Banks Nutter||[PDF](http://www.westfield.ma.edu/mhj/pdfs/militant%20mothers.pdf)
Fall 2010, Vol. 38, No. 1|John Denison Hartshorn: A Colonial Apprentice in "Physick" and Surgery|Catherine L. Thompson|[PDF](http://www.westfield.ma.edu/mhj/pdfs/John%20Denison%20Hartshorn%20-%20olonial%20apprentice.pdf)
Fall 2010, Vol. 38, No. 1|Murder by Counseling:  The 1816 Case of George Bowen (Northampton)|Jack Tager|[PDF](http://www.westfield.ma.edu/mhj/pdfs/murder%20by%20counseling.pdf)
Fall 2010, Vol. 38, No. 1|Certainly a Man May Quibble For His Life: Public Execution and Capital Punishment in Massachusetts|Brian J. Rizzo|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Certainly%20a%20Man%20May%20Quibble%20For%20His%20Life%20-%20public%20execution.pdf)
Fall 2010, Vol. 38, No. 1|Teaching Resources: Using "America’s Historic Newspapers" Database in the Classroom: From Primary Sources to Research Assignments|Brad Austin|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/06/TEACHING-RESOURCE-Using-historic-newspaper-databases.pdf)
Fall 2010, Vol. 38, No. 1|BOOK REVIEWS (11)||[PDF](http://www.westfield.ma.edu/mhj/pdfs/Book%20reviews%20%2819%29.pdf)
Fall 2009, Vol. 37, No. 2|Photo Essay: "The Allen Sisters: ‘Foremost Women Photographers in America’" (1880-1915)|Suzanne L. Flynt|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Allen%20Sisters.pdf)
Fall 2009, Vol. 37, No. 2|Editor’s Choice: "Mr. and Mrs. Prince: An African American Courtship and Marriage in Colonial Deerfield"|Gretchen Holbrook Gerzina|[PDF](http://www.westfield.ma.edu/mhj/pdfs/PDF%20Mr%20&%20Mts.%20Prince%20F'09.pdf)
Fall 2009, Vol. 37, No. 2|African American Heritage Trails: From Boston to the Berkshires|Anita C. Danker|[PDF](http://www.westfield.ma.edu/mhj/pdfs/African%20American%20Heritage%20Trails.pdf)
Fall 2009, Vol. 37, No. 2|’Something Will Drop’: Socialists, Unions, and Trusts in 19th Century Holyoke|Joshua L. Root|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Something%20Will%20Drop%20Holyoke%20Socialists.pdf)
Fall 2009, Vol. 37, No. 2|’Until Death Do Us Part’: Wills, Widows Women, and Dower in Oxford County, 1805-20|Jean F. Hankins|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/06/Until-Death-Do-Us-Part.pdf)
Fall 2009, Vol. 37, No. 2|Politics, Honor, and Self-Defense in Post-Revolutionary Boston: The 1806 Manslaughter Trial of Thomas Selfridge|Jack Tager|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/06/Thomas-Selfridge.pdf)
Fall 2009, Vol. 37, No. 2|’Weltering in Their Own Blood’: Puritan Casualties in King Philip’s War|Robert E. Cray, Jr.|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/06/Weltering-in-their-Own-Blood-Puritan-Casualties.pdf)
Fall 2009, Vol. 37, No. 2|Teaching the History of Education in Collaboration with a University Archive by  Kelly Kolodny, Julia Zoino-Jeannetti and Colleen Previte||[PDF](http://www.westfield.ma.edu/mhj/pdfs/Teaching%20Resources.pdf)
Spring 2009, Vol. 37, No. 1|Photo Essay: "Don’t Smile for the Camera: Another Angle on Early Photography"|Suzanne L. Flynt|[PDF](http://www.westfield.ma.edu/mhj/pdfs/PDF%20Don't%20Smile%20%20S09.pdf)
Spring 2009, Vol. 37, No. 1|Editor’s Choice: "Massachusetts Folk Art in the 21st Century: New Immigrants Redefine Tradition"|Maggie Holtzberg|[PDF](http://www.westfield.ma.edu/mhj/pdfs/PDF%20Mass%20Folk%20Art%20S'09.pdf)
Spring 2009, Vol. 37, No. 1|’Take Me to the Brawl Game’: Sports and Workers in Gilded Age Massachusetts|Robert Weir|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Robert%20Weir%20combined%20spring%202009.pdf)
Spring 2009, Vol. 37, No. 1|Mrs. Elizabeth Towne: Pioneering Woman in Publishing and Politics, 1865-1960|Tzivia Gover|[PDF](http://www.westfield.ma.edu/mhj/pdfs/PDF%20Elizabeth%20Towne%20S'09.pdf)
Spring 2009, Vol. 37, No. 1|Black and Irish Relations in 19th Century Boston: The Interesting Case Lawyer Robert Morris|William Leonard|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/06/William-Leonard-combined-spring-2009.pdf)
Spring 2009, Vol. 37, No. 1|’Won’t Be Home Again’: A Lynn Grocer’s Letters from the California Gold Rush|Michael Gutierrez|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/06/Wont-Be-Home-Again.pdf)
Spring 2009, Vol. 37, No. 1|Gentlemen and Scholars: Harvard’s History Department and the path to Professionalization, 1920-1950|William Palmer|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/06/William-Palmer-combined-spring-2009.pdf)
Spring 2009, Vol. 37, No. 1|Building to a Revolution: The Powder Alarm and Popular Mobilization of New England Countryside, 1774-1775|Patrick Johnston |[PDF](http://www.westfield.ma.edu/mhj/pdfs/Patrick%20Johnson%20combined%20spring%202009.pdf)
Summer 2008, Vol. 36, No. 2|The 1953 Worcester Tornado in its Time: Panic and Recovery|Lianne Lajoie|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Lianne%20Lajoie%20combined%20summer%202008.pdf)
Summer 2008, Vol. 36, No. 2|They Came Here to Fish: Early Massachusetts Fishermen in a Puritan Society|Serena Newman|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Serena%20Newman%20combined%20summer%202008.pdf)
Summer 2008, Vol. 36, No. 2|Addie Card: The Search for Lewis Hine’s  ‘Anemic Little Spinner’|Joe Manning|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Joe%20Manning%20combined%20summer%202008.pdf)
Summer 2008, Vol. 36, No. 2|Anna B. Sullivan, 1903-1983: The Formative Years of a Textile Union Organizer (Holyoke, Massachusetts)|L. Mara Dodge|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Mara%20Dodge%20combined%20summer%202008.pdf)
Summer 2008, Vol. 36, No. 2|Bela Pratt’s Angel of the Battlefield: Good out of Evil|Leslie Jane Sullivan|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Jane%20Sullivan%20combined%20summer%202008.pdf)
Winter 2008, Vol. 36, No. 1|Cultivation of the Higher Self: William Smith Clark (1886-1926) and Agricultural Education|Patrick Browne|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Patrick%20Browne%20combined%20winter%202008.pdf)
Winter 2008, Vol. 36, No. 1|Showing More Profile Than Courage: McCarthyism in Massachusetts and its Challenge to the Young John Fitzgerald Kennedy|Michael Connolly|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Michael%20Connolly%20combined%20winter%202008.pdf)
Winter 2008, Vol. 36, No. 1|A Bridge for Crispus Attucks?|Anita Danker|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Anita%20Danker%20combined%20winter%202008.pdf)
Winter 2008, Vol. 36, No. 1|Massachusetts Agriculture: Charles S. Walker and the National Agrarian Crusade of the 1880s and 90s|Gerald Vaughn|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Gerald%20Vaughn%20combined%20winter%202008.pdf)
Summer 2007, Vol. 35, No. 2|The Literary and Military Career of Benjamin Church (1639-1718): Change or Continuity in Early American Warfare|Guy Chet|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Chet%20summer%202007%20combined.pdf)
Summer 2007, Vol. 35, No. 2|The Complexities Found, as Well as Insights Gained, From the Identification of a Birthplace of Free Public Education: The Case of Rehoboth, Massachusetts|Kelly Ann Kolodny|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Kolodny%20Summer%202007%20combined.pdf)
Summer 2007, Vol. 35, No. 2|Locating Wissatinnewag: A Second Opinion|Lion Miles|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Miles%20Summer%202007%20combined.pdf)
Summer 2007, Vol. 35, No. 2|‘The Most Agreeable Country’: New Light on Democratic-Republican Opinion of Massachusetts in the 1790s|Arthur Scherr|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Scherr%20Summer%202007%20combined.pdf)
Summer 2007, Vol. 35, No. 2|A Massachusetts Yankee in the Court of Charleston: Jasper Adams (1793-1841), University President in Antebellum South Carolina|Gerald Vaughn|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/06/Vaughn-Summer-2007-combined.pdf)
Winter 2007, Vol. 35, No. 1|Beyond the Scarlet ‘A’: Hawthorne and the Matter of Racism|Richard Klayman|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Klayman%20winter%202007%20combined.pdf)
Winter 2007, Vol. 35, No. 1|Reform Politics in Hard Times: Battles Over Labor Legislation during the Decline of Traditional Manufacturing in Massachusetts, 1922-1928|David Koistinen|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Koistinen%20winter%202007%20combined.pdf)
Winter 2007, Vol. 35, No. 1|Political Rivalry in Rhode Island: William H. Vanderbilt vs. J. Howard McGrath: The Wiretapping Case|Debra Mulligan|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Mulligan%20winter%202007%20combined.pdf)
Winter 2007, Vol. 35, No. 1|Ralph Waldo Emerson’s Mentor at Harvard|Gerald Vaughn|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/06/Vaughn-winter-2007-combined.pdf)
Summer 2006, Vol. 34, No. 2|From Norton to Saint Gaubin: Grinding Labor Down in Worcester, 1885-2006|Bruce Cohen|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Cohen%20summer%202006%20combined.pdf)
Summer 2006, Vol. 34, No. 2|‘Charmed with the French’: Reassessing the Early Career of Charles Bulfinch (1763-1844), Architect|Thomas Conroy|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Conroy%20summer%202006%20combined.pdf)
Summer 2006, Vol. 34, No. 2|Greenfield Tap & Die: Economic and Historical Analysis, 1912-1992|Rebecca Ducharme|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Ducharme%20summer%202006%20combined.pdf)
Summer 2006, Vol. 34, No. 2|Interpreting the Place Space of an Extinct Cultural Landscape: The Swift River Valley of Central Massachusetts, 1820-1940|Delphis Levia and Mark Bashour|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Levia%20Bashour%20summer%202006%20combined.pdf)
Winter 2006, Vol. 34, No. 1|‘Tyrant and Oppressor!’: Colonial Press Reaction to the Quebec Act|Paul Langston|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Langston%20winter%202006%20combined.pdf)
Winter 2006, Vol. 34, No. 1|Piracy, Riches, and Social Equality: The Wreck of the Whydah off Cape Cod, 1700-1717|Sara Schubert|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Schubert%20winter%202006%20combined.pdf)
Winter 2006, Vol. 34, No. 1|Amherst Professor Joseph Haven and His Influence on America’s Greatest Social Critic, Thorstein Veblen|Gerald Vaughn|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/06/Vaughn-winter-2006-combined.pdf)
Winter 2006, Vol. 34, No. 1|Locating Wissatinnewag in John Pynchon’s Letter of 1663|Marge Bruchac and Peter Thomas|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Bruchac%20and%20Thomas%20winter%202006%20combined.pdf)
Summer 2005, Vol. 33, No. 2|The Civil War Draft in Palmer: Reaction of a Small Town|Amber Vaill|
Summer 2005, Vol. 33, No. 2|No Early Pardon for Traitors: Rebellion in Massachusetts in 1787|John Dryden Kazar|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Kazar%20combined%20summer%202005.pdf)
Summer 2005, Vol. 33, No. 2|The Political Evolution of Northampton, Massachusetts, 1970-1995|Robert Driscoll|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Driscoll%20combined%20summer%202005.pdf)
Summer 2005, Vol. 33, No. 2|Founding Aswalos House: Separate But Equal – The YWCA in Roxbury, Massachusetts, 1968-1988|Sharlene Voogd Cochrane|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Cochrane%20combined%20summer%202005.pdf)
Summer 2005, Vol. 33, No. 2|The Aftermath of the Salem Witch Trials in Colonial America|Marc Callis|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Callis%20combined%20summer%202005.pdf)
Winter 2005, Vol. 33, No. 1|‘I Spake the Truth in the Feare of God’: The Puritan Management of Dissent during the Henry Dunster Controversy|Timothy Wood|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/06/Wood-winter-2005-combined.pdf)
Winter 2005, Vol. 33, No. 1|The Civil War Draft in Palmer: Reaction of a Small Town|Amber Vail|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/06/Vaill-winter-2005-combined.pdf)
Winter 2005, Vol. 33, No. 1|A Mirror of Boston: Faneuil Hall at the Turn of the 19th Century|Christopher Harris|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Harris%20winter%202005%20combined.pdf)
Winter 2005, Vol. 33, No. 1|Northampton Local Monuments: Testament to an Enduring Historical Legacy|Jill Walton|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/06/Walton-winter-2005-combined.pdf)
Summer 2004, Vol. 32, No. 2|The Beginning of the Past: Boston and the Early Historic Preservation Movement, 1863-1918|Marc Callis|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Callis%20summer%202004%20combined.pdf)
Summer 2004, Vol. 32, No. 2|Ethnic Catholicism and Craft Unionism In Worcester, Massachusetts, 1887-1920:  A Mixed Story|Bruce Cohen|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Cohen%20summer%202004%20combined.pdf)
Summer 2004, Vol. 32, No. 2|Alternative Communities in Seventeenth Century Massachusetts|Marsha Hamilton|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Hamilton%20summer%202004%20combined.pdf)
Summer 2004, Vol. 32, No. 2|What Did They Call It When They Died?  A Study of the Listed Causes of Death in the Town of Hyde Park, Massachusetts, 1869-1908|Nancy Hannan|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Hannan%20combined.pdf)
Summer 2004, Vol. 32, No. 2|Sheffield’s Richard P. Wakefield (1921-2000): Advocate for Human Values, World Futures, and the Environment|Gerald Vaughn|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/06/Vaughn-summer-2004-combined.pdf)
Winter 2004, Vol. 32, No. 1|Class and the Ideology of Womenhood: The Early Years of the Boston Young Women’s Christian Association, 1866-1905|Jennifer Cote|[PDF](http://www.westfield.ma.edu/mhj/pdfs/cote%20winter%202004%20combined.pdf)
Winter 2004, Vol. 32, No. 1|Charter Changes in Boston from 1885-1949|Charlie Tebbetts|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/06/tebbetts-winter-2004-combined.pdf)
Winter 2004, Vol. 32, No. 1|Fitz Hugh Lane and the Legacy of the Codfish Aristocracy, 1830-1964|Sharon Worley|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/06/worley-winter-2004-combined.pdf)
Winter 2004, Vol. 32, No. 1|Oxenbridge Thacher (1719-1765): Boston Lawyer, Early Patriot|Clifford Putney|[PDF](http://www.westfield.ma.edu/mhj/pdfs/putney%20winter%202004%20combined.pdf)
Summer 2003, Vol. 31, No. 2|‘The Artificial Advantage Money Gives’: A Brahmin Reformer’s Use of Class Privilege|Jana Brubaker|[PDF](http://www.westfield.ma.edu/mhj/pdfs/brubaker%20summer%202003%20combined.pdf)
Summer 2003, Vol. 31, No. 2|The Altar of Liberty: Enlightened Dissent and the Dudleian Lectures, 1755-1765|Leslee K. Gilbert|[PDF](http://www.westfield.ma.edu/mhj/pdfs/gilbert%20summer%202003%20combined.pdf)
Summer 2003, Vol. 31, No. 2|‘We Are to Be Reduced to the Level of Slaves’: Planters, Taxes, Aristocrats, and Massachusetts Antifederalists, 1787-1788|John Craig Hammond|[PDF](http://www.westfield.ma.edu/mhj/pdfs/hammond%20summer%202003%20combined.pdf)
Summer 2003, Vol. 31, No. 2|Almshouse, Workhouse, Outdoor Relief: Responses to the Poor in Southeastern Massachusetts, 1740-1800|Jennifer Turner|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/06/turner-summer-2003-combined.pdf)
Winter 2003, Vol. 31, No. 1|Mysteries of the Tyringham Shakers Unmasked: A New Examination of People, Facts, and Figures|Stephen Paterwic|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Paterwic%20winter%202003%20combined.pdf)
Winter 2003, Vol. 31, No. 1|David Rozman and Land-Planning in Massachusetts, 1927-1961|Gerald Vaughn|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/06/Vaughn-winter-2003-combined.pdf)
Winter 2003, Vol. 31, No. 1|How ‘Poor Country Boys’ Became Boston Brahmins: The Rise of the Appletons and the Lawrences in Ante-bellum Massachusetts|Anthony Mann|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Mann%20winter%202003%20combined.pdf)
Winter 2003, Vol. 31, No. 1|Grass-Roots Garrisonians in Central Massachusetts: The Case of Hubbardston’s Jonas and Susan Clark|William Koelsch|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Koelsch%20winter%202003%20combined.pdf)
Winter 2003, Vol. 31, No. 1|The Roots of Connecticut River Deindustrialization: The Springfield American Bosch Plant 1940-1975|Robert Forrant|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Farrant%20winter%202003%20combined.pdf)
Summer 2002, Vol. 30, No. 2|The Publisher of the Foreign-Language as an Ethnic Leader? The Case of James V. Donnamura and Boston’s Italian-American Community in the Interwar Years|Benedicte Deschamps and Stefano Luconi|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Luconi%20and%20Deschamps%20Summer%202002%20complete.pdf)
Summer 2002, Vol. 30, No. 2|The Groton Indian Raid of 1694 and Lydia Longley|William Wolkovich-Valkavicius|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/06/Wolkovich-Valkavicius-Summer-2002-complete.pdf)
Summer 2002, Vol. 30, No. 2|The Devil and Father Rallee|Thomas Kidd|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Kidd%20Summer%202002%20complete.pdf)
Summer 2002, Vol. 30, No. 2|The Role of the 54th Massachusetts Regiment in Potter’s Raid|Leonne Hudson|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Hudson%20Summer%202002%20complete.pdf)
Summer 2002, Vol. 30, No. 2|’So I Must Be Contented to Live a Widow’: The Revolutionary War Service of Sarah Hodgkins of Ipswich, 1775-1779|Richard Tracey|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/06/Tracey-Summer-2002-complete.pdf)
Winter 2002, Vol. 30, No. 1|A Pox on Amherst: Smallpox, Jeffrey Amherst, and a Town Named Amherst|Francis Flavin|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/07/Flavin-combined.pdf)
Winter 2002, Vol. 30, No. 1|‘I Wish for Nothing More Ardent upon Earth, Than to See My Friends and Country Again’: The Return of Massachusetts Loyalists|Stephanie Kermes|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Kermes%20Winter%202002%20complete.pdf)
Winter 2002, Vol. 30, No. 1|The First Hurrah: James Michael Curley Versus the ‘Goo-Goo’s in the Boston Mayoralty Election of 1914|Michael Connolly|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Connolly%20Winter%202002%20complete.pdf)
Winter 2002, Vol. 30, No. 1|Brahmins Under Fire: Peer Courage and the Civil War Harvard Regiment|Richard Miller|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Miller%20Winter%202002%20complete.pdf)
Summer 2001, Vol. 29, No. 2|Indian Land in Seventeenth Century Massachusetts|Christopher Hannan|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Hannan%20Summer%202001%20complete.pdf)
Summer 2001, Vol. 29, No. 2|Mapping the Metaphysical Landscape of Cape Ann: The Reception of Ralph Waldo Emerson’s Transcendentalism among the Gloucester Audience of Reverend Amory Dwight Mayo and Fitz Hugh Lane|Sharon Worley|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/06/Worley-Summer-2001-complete.pdf)
Summer 2001, Vol. 29, No. 2|’A Brave Man’s Child’: Theodore Parker and the Memory of the American Revolution|Paul Teed|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/06/Teed-Winter-2001.pdf)
Summer 2001, Vol. 29, No. 2|New England and Early Conservationism: The North American Review 1830-1860|Edward Spann|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Spann%20Summer%202001%20complete.pdf)
Winter 2001, Vol. 29, No. 1|Monster of Monsters and the Emergence of Political Satire in New England|Alison Olson|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Olson%20Winter%202001.pdf)
Winter 2001, Vol. 29, No. 1|Bunker Hill Refought: Memory Wars and Partisan Conflicts, 1775-1825|Robert Cray|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Cray%20winter%202001%20combined.pdf)
Winter 2001, Vol. 29, No. 1|Conflict in the Church and City: The Problem of Catholic Parish Government in Boston, 1790-1865|Ronald Patkus|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Patkus%20Winter%202001.pdf)
Winter 2001, Vol. 29, No. 1|Lords of Capital and Knights of Labor: Worcester’s Labor History During the Gilded Age|Bruce Cohen|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Cohen%20Winter%202001.pdf)
Summer 2000, Vol. 28, No. 2|A ‘Great National Calamity’: Sir William Pepperell and Isaac Royall, Reluctant Loyalists|Colin Nicolson|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Nicolson%20and%20Scott%20summer%202000%20combined.pdf)
Summer 2000, Vol. 28, No. 2|‘Sober Dissent’ and ‘Spirited Conduct’: The Sandemanians and the American Revolution, 1765-1781|John Smith|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Smith%20summer%202000%20combined.pdf)
Summer 2000, Vol. 28, No. 2|Progressive Nativism: The Know-Nothing Party in Massachusetts|Steven Taylor|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/06/Taylor-summer-2000-combined.pdf)
Summer 2000, Vol. 28, No. 2|The Enigma of Mount Holyoke’s Nellie Neilson (1873-1947)|Gerald Vaughn|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/06/Vaughn-summer-2000-combined.pdf)
Winter 2000, Vol. 28, No. 1|’The Most Bewitching Piece of Parliamentary Oratory: Fisher Ames’ Jay Treaty Speech Reconsidered|Todd Estes|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Estes%20winter%202000%20combined.pdf)
Winter 2000, Vol. 28, No. 1|Who Were the Pelham Shaysites? (1738-1883)|Robert Lord Keyes|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Keyes%20winter%202000%20combined.pdf)
Winter 2000, Vol. 28, No. 1|Pulpits and Politics: Anti-Catholicism in Boston in the 1880s and 1890s|Lawrence Kennedy|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Kennedy%20winter%202000%20combined.pdf)
Winter 2000, Vol. 28, No. 1|The Blackstone Canal, 1796-1828|Richard Wilson|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/06/Wilson-winter-2000-combined.pdf)
Summer 1999, Vol. 27, No. 2|‘Every Composer to be his own Carver’: The Manliness of William Billings|Eben Simmons Miller|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/07/Miller-combined.pdf)
Summer 1999, Vol. 27, No. 2|Founding Mothers of Social Justice: The Women’s Educational and Industrial Union of Boston, 1877-1892|Erica Harth|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/07/Harth-combined.pdf)
Summer 1999, Vol. 27, No. 2|‘Boycott!’: Louis Imogen Guiney and the American Protective Association in the 1890s|Patricia Fanning|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/07/Fanning-combined.pdf)
Summer 1999, Vol. 27, No. 2|Politics of the Boston Irish, 1861-1920|Patrick Kennedy|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/07/Kennedy-combined.pdf)
Winter 1999, Vol. 27, No. 1|‘And Let the People Say Amen’:  Priests, Presbyters, and the Arminian [Jacobus Arminius] Uprising in Massachusetts, 1717-1724|Lawrence Jannuzzi|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/07/Jannuzzi-combined.pdf)
Winter 1999, Vol. 27, No. 1|The Wrongheaded and the Transparent Eye-ball: Garrison, Emerson, and Antebellum Reform|Denis Brennan|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/07/Brennan-combined.pdf)
Winter 1999, Vol. 27, No. 1|Do Unto Others: The Golden Rule and Organized Labor in Worcester, Massachusetts, 1955-1965|James Hanlan and Bruce Cohen|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Hanlan%20and%20Cohen%20Winter%201999.pdf)
Winter 1999, Vol. 27, No. 1|Petersham’s Ayers Brinser (1909-1967):  Distinguished American Conservationist|Gerald Vaughn|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/07/Vaughn-combined.pdf)
Summer 1998, Vol. 26, No. 2|Where Did Captain Martin Pring Anchor in New England?|Richard Whalen|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/07/Whalen-combined.pdf)
Summer 1998, Vol. 26, No. 2|Massachusetts Gave Leadership to America’s Country Life Movement: The Collaboration of Kenyon L. Butterfield and Wilbert L. Anderson, 1894-1919|Gerald Vaughn|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/07/Vaughn-combined-1.pdf)
Summer 1998, Vol. 26, No. 2|Two Lithuanian Immigrants’ Blasphemy Trials during the Red Scare|Rev. William Wolkovich-Valkavicius|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/07/Wolkovich-Valkavicious-combined.pdf)
Summer 1998, Vol. 26, No. 2|The Boston Longshoremen’s Strike of 1931|Francis McLaughlin|[PDF](http://www.westfield.ma.edu/mhj/pdfs/McLaughlin%20winter%201998%20combined.pdf)
Summer 1998, Vol. 26, No. 2|Springfield’s Washingtonians:  The Triumph of Legal Sanctions to Save the Soul of the Drunkard|Thomas Moriarty|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/07/Moriarty-combined.pdf)
Winter 1998, Vol. 26, No. 1|‘Melancholy Catastrophe!’ The Story of Jason Fairbanks and Elizabeth Fales (1801)|Dale Freeman|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/07/Freeman-combined.pdf)
Winter 1998, Vol. 26, No. 1|The Replacement of the Knights of Labor|the International Longshoremen’s Association in the Port of Boston|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/07/McLaughlin-winter-1998-combined.pdf)
Winter 1998, Vol. 26, No. 1|Springfield’s Puritans and Indians: 1636-1655|Marty O’Shea|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/07/OShea-combined.pdf)
Winter 1998, Vol. 26, No. 1|Public Days in Massachusetts Bay, 1630-1685: Reasons Behind the Ritual and the Ironic Results|Melissa Weinbrenner|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/07/Weinbrenner-combined.pdf)
Summer 1997, Vol. 26, No. 2|The Ecstasy of Sara Prentice:  Death, Re-Birth, and the Great Awakening in Grafton, Massachusetts|Ross Beales|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Beales%20Summer%2097%20combined.pdf)
Summer 1997, Vol. 26, No. 2|The Crispus Attucks Monument Dedication|Dale Freeman|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Freeman%20Summer%201997%20combined.pdf)
Summer 1997, Vol. 26, No. 2|The New Deal Origins of the Cape Cod National Seashore|Douglas Doe|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Doe%20Summer%201997%20combined.pdf)
Summer 1997, Vol. 26, No. 2|Black Citizenship and Military Self-Presentation in Antebellum Massachusetts|Hal Goldman|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Hal%20Goldman%20winter%2097%20combined.pdf)
Winter 1997, Vol. 26, No. 1|School Suffrage and the Campaign for Women’s Suffrage in Massachusetts, 1879-1920|Edmund Thomas|[PDF](http://www.westfield.ma.edu/mhj/pdfs/E%20Thomas%20winter%2097%20combined.pdf)
Winter 1997, Vol. 26, No. 1|Black Citizenship and Military Self-Presentation in Antebellum Massachusetts|Hal Goldman|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Hal%20Goldman%20winter%2097%20combined.pdf)
Winter 1997, Vol. 26, No. 1|Disharmony and Harmony Along the Merrimack: The Civic Traditions of Lowell and Manchester (1815-1936)|Brian O’Donnell|[PDF](http://www.westfield.ma.edu/mhj/pdfs/O'Donnell%20Winter%2097%20combined.pdf)
Winter 1997, Vol. 26, No. 1|The Massachusetts Veterinary Profession, 1882-1904: A Case Study|Philip Teigen and Sheryl Blair|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/06/Teigen-and-Blair-winter-97-combined.pdf)
Summer 1996, Vol. 24, No. 2|The Early Years of the Monomoy National Wildlife Refuge|Douglas Doe|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/07/Doe-combined.pdf)
Summer 1996, Vol. 24, No. 2|Two Years in Blue: The Civil War Letters of Joseph K. Taylor|Kevin Murphy|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/07/Murphy-combined.pdf)
Summer 1996, Vol. 24, No. 2|Ambiguous Loyalties: The Boston Irish, Slavery, and the Civil War|Brian Kelly|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/07/Kelly-combined.pdf)
Summer 1996, Vol. 24, No. 2|John Adams v. William Brattle: A Non-Debate on Judicial Tenure, 1763-1772|Thomas Martin|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/07/Martin-combined.pdf)
Winter 1996, Vol. 24, No. 1|Reading Between the Lines: Early English Accounts of the New England Indians|Michael Puglisi|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Puglisi%20combined.pdf)
Winter 1996, Vol. 24, No. 1|The Tatnuck Ladies’ Sewing Circle: A Forum for Women’s Political Views in Worcester Massachusetts, 1847-1867|Laura Wasowics|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/06/Wasowicz-combined.pdf)
Winter 1996, Vol. 24, No. 1|Skilled Workers’ Union Organizations in Springfield: The American Bosch Story|Robert Forrant (early 20th century)|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Forrant%20combined.pdf)
Winter 1996, Vol. 24, No. 1|Confronting Jim Crow: Boston’s Anti-Slavery Tradition, 1890-1920|Mark Schneider|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Schneider%20combined.pdf)
Summer 1995, Vol. 23, No. 2|Nursing in Massachusetts During the Roaring Twenties|Mary Ellen Donna, Joellen Hawkins, Ursula Van Ryzin, Alive Friedman, Loretta Higgins|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Doona%20combined.pdf)
Summer 1995, Vol. 23, No. 2|Development of the Boston Area Highway System|Michael Passanisi|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Passanisi%20combined.pdf)
Summer 1995, Vol. 23, No. 2|‘As if a Great Darkness’: Native American Refugees of the Middle Connecticut River Valley in the Aftermath of King Philip’s War|James Spady|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Spady%20combined.pdf)
Summer 1995, Vol. 23, No. 2|Factionalism in Post-Revolution Boston|Myron Wehtje|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/06/Wehtje-combined.pdf)
Winter 1995, Vol. 23, No. 1|The Hoosac Tunnel: Massachusetts’ Western Gateway|Terrence Coyne|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Coyne%20combined.pdf)
Winter 1995, Vol. 23, No. 1|William Lloyd Garrison and the Crisis of Nonresistance|Lawrence Jannuzzi|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Jannuzzi%20combined.pdf)
Winter 1995, Vol. 23, No. 1|Two Approaches to Indian Conversion in Puritan New England: The Missions of Thomas Mayhew Jr. and John Elliot|Richard Cogley|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Cogley%20combined.pdf)
Winter 1995, Vol. 23, No. 1|Building Boston’s Back Bay: Marriage of Money and Hygiene|Allan Galper|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Galper%20combined.pdf)
Summer 1994, Vol. 22, No. 2|The Founders of the Boston Bar Association: A Collective Analysis|Alan Rogers|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Rogers%20combined.pdf)
Summer 1994, Vol. 22, No. 2|This Greenback Lunacy: Third Party Politics in Franklin County, 1878|Kathleen Banks Nutter|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Nutter%20combined.pdf)
Summer 1994, Vol. 22, No. 2|The Negotiation of Power Relations in a Puritan Settlement|Aristide Sechandice|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Sechandice%20combined.pdf)
Summer 1994, Vol. 22, No. 2|Nathaniel Ames, Sr. (-1764), and the Political Culture of Provincial New England|William Pencak|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Pencak%20combined.pdf)
Winter 1994, Vol. 22, No. 1|Intellectual Authority and Gender Ideology in Nineteenth Century Boston: The Life Letters of Oliver Wendell Holmes, Sr.|Tim Duffy|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Duffy%20combined.pdf)
Winter 1994, Vol. 22, No. 1|Paradox of Opportunities: Lucy Stone, Alice Stone Blackwell and the Tragedy of Reform|Lori Bogle|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Bogle%20combined.pdf)
Winter 1994, Vol. 22, No. 1|The U.S. Naval Reserve Midshipmen School, Northampton, 1942-45: A Personal Account|Margaret Clifford Dwyer|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Dwyer%20combined.pdf)
Winter 1994, Vol. 22, No. 1|The CIO in Rural Massachusetts: Sprague Electric and North Adams, 1937-1944|Maynard Seider|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Seider%20combined.pdf)
Summer 1993, Vol. 21, No. 2|Frolics for Fun: Dances, Weddings, and Dinner Parties in Colonial New England|Bruce Daniels|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Daniels%20cobmined.pdf)
Summer 1993, Vol. 21, No. 2|Bidwell’s Saltbox House|Shirley Clute|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Clute%20combined.pdf)
Summer 1993, Vol. 21, No. 2|Maria W. Stewart (1803-1879): The First Female African-American Journalist|Rodger Streitmatter|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Streitmatter%20combined.pdf)
Summer 1993, Vol. 21, No. 2|Gender Barriers to Forming a Teachers’ Union in Boston (1919-1965)|Kathleen Murphy|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Murphey%20combined.pdf)
Winter 1993, Vol. 21, No. 1|Urban Renewal in Boston, 1950-1976|Jack Tager|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Tager%20combined.pdf)
Winter 1993, Vol. 21, No. 1|Party and Politics: Ashburnham in the 1850s|Joseph von Deck|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Deck%20combined.pdf)
Winter 1993, Vol. 21, No. 1|The Home for Aged Colored Women, 1861-1944|Esther MacCarthy|[PDF](http://www.westfield.ma.edu/mhj/pdfs/MacCarthy%20combined.pdf)
Winter 1993, Vol. 21, No. 1|New England Academy Education in the Early Republic|Brian Cooke|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Cooke%20combined.pdf)
Summer 1992, Vol. 20, No. 2|Railroad Rivalry in the 19th Century Connecticut River Valley|Larry Lowenthal|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/07/Lowenthal-combined.pdf)
Summer 1992, Vol. 20, No. 2|Controversy over the Legal Profession in Post-Revolutionary Boston|Myron Wehtje|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/07/Wehtje-combined.pdf)
Summer 1992, Vol. 20, No. 2|Pliny Earle’s (1809-1892) State Hospital at Northampton: Moral Treatment or Peonage?|David Dill, Jr.|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/07/Dill-combined.pdf)
Summer 1992, Vol. 20, No. 2|Labor and the State in Worcester: Organization of the Metal Trades, 1937-1971|Bruce Cohen|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/07/Cohen-combined.pdf)
Winter 1992, Vol. 20, No. 1|Forest Conservation Policy in Early New England|Yasuhide Kawashima|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Kawashima%20combined.pdf)
Winter 1992, Vol. 20, No. 1|Free Blacks and Kidnapping in Antebellum Boston|Peter P. Hinks|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Hinks%20combined.pdf)
Winter 1992, Vol. 20, No. 1|Worcester County Soldiers in the Civil War|Pamela J. Cummings|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Cummings%20combined.pdf)
Winter 1992, Vol. 20, No. 1|Religious Opposition to the Massachusetts Lottery|Susan Ponte|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Ponte%20combined.pdf)
Winter 1992, Vol. 20, No. 1|Development of the Assabet Mills in19th Century Maynard|John R. Mullin (early 19th century)|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Mullin%20combined.pdf)
Summer 1991, Vol. 19, No. 2|The Massachusetts Miracle, 1947-1988|Jack Tager|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Tager%20combined.pdf)
Summer 1991, Vol. 19, No. 2|Fairground Days: When Worcester Was a National League City, 1880-1882|Charles Goslow|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Goslow%20combined.pdf)
Summer 1991, Vol. 19, No. 2|Agricultural Science and Tobacco Agriculture in the Connecticut River Valley|Gregory Field|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Field%20combined.pdf)
Summer 1991, Vol. 19, No. 2|The Politics of Abolition in Northhampton|Michael D. Blanchard|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Blanchard%20combined.pdf)
Winter 1991, Vol. 19, No. 1|Dr. Gamaliel Bradford (1795-1839), Early Abolitionist|James W. Mathews|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Mathews%20combined.pdf)
Winter 1991, Vol. 19, No. 1|Lillian Clayton Jewett and the Rescue of the Baker Family, 1899-1900|Roger K. Hux|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Hux%20combined.pdf)
Winter 1991, Vol. 19, No. 1|The Evolution of the Marshall Street Complex in North Adams, 1861-1985|Robert Paul Gabrielsky|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Gabrielsky%20combined.pdf)
Winter 1991, Vol. 19, No. 1|The Spanish Influenza of 1918 and Berkshire County|Dennis J. Carr|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/07/Carr-combined.pdf)
Winter 1991, Vol. 19, No. 1|The European Journey of the Stebbins Family of Springfield (1882)|Pamela J. Getchell|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Getchell%20combined.pdf)
Summer 1990, Vol. 18, No. 2|Julia Harrington Duff: An Irish Woman Confronts the Boston Power Structure, 1900-1905|Polly Welts Kaufman|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Kaufman%20combined%20summer%2090.pdf)
Summer 1990, Vol. 18, No. 2|Joseph Knight Taylor (1840-1864): ‘Plain Path of Duty’|Donna Gnatek|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Gnatek%20Combined.pdf)
Summer 1990, Vol. 18, No. 2|Fear of British Influence in Boston, 1783-1787|Myron Wehtje|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/06/Wehtje-combined-summer-90.pdf)
Summer 1990, Vol. 18, No. 2|The Homecoming of the Flags: The Return of a Civil War Flag to Springfield Massachusetts|Larry Lowenthal|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Lowenthal%20combined%20summer%2090.pdf)
Summer 1990, Vol. 18, No. 2|A Massachusetts Perspective on the Income Tax Amendment|David A. Rawson|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Rawson%20combined%20summer%2090.pdf)
Winter 1990, Vol. 18, No. 1|The Yarmouth Register and the Emerging Crisis over Slavery|Paul R. Mangelinkx|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Mangelinkx%20combind.pdf)
Winter 1990, Vol. 18, No. 1|The Builder Calvin Stearns: Methods of Survival in the Rural Economy (1807-1840)|Kathleen Banks Nutter|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Nutter%20combined.pdf)
Winter 1990, Vol. 18, No. 1|Medical Practice in the Connecticut River Valley, 1650-1750|Paul Berman|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Berman%20combind.pdf)
Winter 1990, Vol. 18, No. 1|The Workingman’s Party of Hampshire County, 1811-1835|Marc Ferris|[PDF](http://www.westfield.ma.edu/mhj/pdfs/Ferris%20combined.pdf)
Winter 1990, Vol. 18, No. 1|The Ku Klux Klan in the Nashoba Valley, 1840-1933|William Wolkovich|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/06/Wolkovich-combind.pdf)
Summer 1989, Vol. 17, No. 2|Elkanah Watson (1758-1842) and the Early Agricultural Fair|Mark A. Mastromarino|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/07/Mastromarino-combined.pdf)
Summer 1989, Vol. 17, No. 2|The Monarch of Hampshire: Israel Williams (1723-1788)|Deborah Day Emery|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/07/Emery-combined.pdf)
Summer 1989, Vol. 17, No. 2|The Working Poor of Pre-Revolutionary Boston|Eric G. Nellis|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/07/Nellis-combined.pdf)
Summer 1989, Vol. 17, No. 2|The Humorous Side of Shays’ Rebellion|William Pencak|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/07/Pencak-combined.pdf)
Summer 1989, Vol. 17, No. 2|An Ambiguous to the Market: The Early New England-Barbados Trade|Larry D. Gragg|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/07/Gragg-combined.pdf)
Winter 1989, Vol. 17, No. 1|Local Merchants and the Regional Economy of the Connecticut River Valley, 1795-1823|Gerald F. Reid|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/07/Reid-combined.pdf)
Winter 1989, Vol. 17, No. 1|Water-Cure in the Bay State|Susan E. Cayleff|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/07/Cayleff-combined.pdf)
Winter 1989, Vol. 17, No. 1|Loud Sermons in the Press: The Reporting of Death in Early Massachusetts Newspapers|Stephan C. Messer|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/07/Messer-combined.pdf)
Winter 1989, Vol. 17, No. 1|Albanians of Hudson and the Origin of the Independent Albanian Orthodox Church|William Wolkovich|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/07/Wolkovich-combined.pdf)
Winter 1989, Vol. 17, No. 1|The Ideal of Virtue in Post-Revolutionary Boston|Myron F. Wehtje|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/07/Wehtje-combined-1.pdf)
Summer 1988, Vol. 16, No. 2|Governor Francis Bernard and His Land Acquisitions, 1762-1779|Ruth Owen Jones|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/10/Jones-combined.pdf)
Summer 1988, Vol. 16, No. 2|Jedidiah Morse and the Illuminati Affair: A Re-Reading, 1789-1799|Richard J. Moss|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/10/Moss-combined.pdf)
Summer 1988, Vol. 16, No. 2|The Worcester Machinists’ Strike of 1915|Bruce Cohen|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/10/Cohen-combined.pdf)
Summer 1988, Vol. 16, No. 2|The Spirit of Reform in Hopkinton, 1829-1849|John J. Navin|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/10/Navin-combined.pdf)
Summer 1988, Vol. 16, No. 2|‘An Insupportable Burden’: Paying for King Philip’s War on the Massachusetts Frontier|Michael J. Puglisi|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/10/Puglisi-combined.pdf)
Summer 1988, Vol. 16, No. 2|The Boston Juvenile Court and the Progressive Challenge of Child Saving, 1906-1986|Richard Klayman|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/10/Klayman-combined.pdf)
January 1988, Vol. 16, No. 1|The Know-Nothings in Quincy|James Tracey|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/10/Tracy-combined.pdf)
January 1988, Vol. 16, No. 1|Emerson and the Campaign of 1851|Leonard G. Gougeon|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/10/Gougeon-combined.pdf)
January 1988, Vol. 16, No. 1|Samuel Hopkins and the Coming of the Church of England to Great Barrington|Michael Winship|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/10/Winship-combined.pdf)
January 1988, Vol. 16, No. 1|Fall River and the Decline of the New England Textile Industry, 1949-1954|Bruce Saxon|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/10/Saxon-combined.pdf)
January 1988, Vol. 16, No. 1|Springfield’s Union Relief Association, 1877-1886|David W. Anthony|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/10/Anthony-combined.pdf)
January 1988, Vol. 16, No. 1|The Limits of Partisanship in Gilded Age Worcester: The Citizens Coalition|Robert J. Kolesar|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/10/Kolesar-combined.pdf)
June 1987, Vol. 15, No. 2|Boston and the Calling of the Federal Convention of 1787|Myron F. Wehtje|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/10/Wehtje-combined.pdf)
June 1987, Vol. 15, No. 2|Industrialization and the Transformation of Public Education in New Bedford, 1865-1900|Thomas A. McMullin|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/10/McMullin-combined.pdf)
June 1987, Vol. 15, No. 2|An Experiment in Labor Peace: Haverhill, 1890-1930|Paul H. Tedesco|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/10/Tedesco-combined.pdf)
June 1987, Vol. 15, No. 2|Ethnicity and Urban Politics: French Canadians in Worcester, 1895-1915|Ronald A. Petrin|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/10/PEtrin-combined.pdf)
June 1987, Vol. 15, No. 2|The Development of the Massachusetts District Courts, 1821-1922|Kathleen McDermott|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/10/McDermott-combined.pdf)
June 1987, Vol. 15, No. 2|Civil War Military Service and Loyalty to the Republican Party: 1884|Gerald W. McFarland and Kazuto Oshio|[PDF](http://www.westfield.ma.edu/historical-journal/wp-content/uploads/2018/10/McFarland-and-Oshio-combined.pdf)
January 1987, Vol. 15, No. 1|Philip English and the Witchcraft Hysteria of Salem Massachusetts|Bryan F. Le Beau|
January 1987, Vol. 15, No. 1|The New England Textile Strike of 1922: Focus on Fitchburg|Edmund B. Thomas, Jr.|
January 1987, Vol. 15, No. 1|The Transformation of Agriculture in Brookline, 1770-1885|Ronald Dale Karr|
January 1987, Vol. 15, No. 1|Jonathon Bliss: Massachusetts Loyalist|William L. Welch|
January 1987, Vol. 15, No. 1|‘American Herbarium’: Key to Deerfield’s Historic Landscape|William W. Jenney|
January 1987, Vol. 15, No. 1|Boston and the New Nation, 1783-1786|Myron F. Wehtje|
June 1986, Vol. 14, No. 2|Women in the Boston Gazette, 1755-1775|Susan Dion|
June 1986, Vol. 14, No. 2|The Massachusetts Land Lottery of 1786-1787|William L. Welch|
June 1986, Vol. 14, No. 2|The Election of Father Robert F. Drinan to the House of Representatives,1962-1970|Philip A. Grant Jr.|
June 1986, Vol. 14, No. 2|Selling Massachusetts Medicines 1708-1889|J. Worth Estes|
June 1986, Vol. 14, No. 2|The Steam Power on the Connecticut, 1787-1826|Guy A. MacLain Jr.|
June 1986, Vol. 14, No. 2|The Daily Transactions of a Westfield Pastor, 1726-1740|Walter L. Powell|
January 1986, Vol. 14, No. 1|Harvard and Yale in the Great Awakening|Ross W. Beales, Jr.|
January 1986, Vol. 14, No. 1|The Colonial Background of New England’s Secondary Urban Centers (Connecticut, Massachusetts, New Hampshire and Rhode Island)|Bruce C. Daniels|
January 1986, Vol. 14, No. 1|The Massachusetts Whigs and Industrialism|Thomas Brown|
January 1986, Vol. 14, No. 1|Profiles of Nineteenth Century Working Women|Marjorie Ruzich Abel|
January 1986, Vol. 14, No. 1|Domestic Violence in Colonial Massachusetts|Brend D. McDonald|
June 1985, Vol. 13, No. 2|Boston Two Hundred Years Ago|Myron F. Wehtje|
June 1985, Vol. 13, No. 2|The Beginnings of Theological Education at Andover|James W. Fraser|
June 1985, Vol. 13, No. 2|Two Centuries of Oligarchy in Brookline|Ronald Dale Karr|
June 1985, Vol. 13, No. 2|Cardinal and Cleric: O’Connell and Mullen in Conflict|William Wolkovich (early20th century)|
June 1985, Vol. 13, No. 2|The Spirit of the Springfield Armory|Stanislaus Skarzynski|
June 1985, Vol. 13, No. 2|Labor in a City of Immigrants: Holyoke, 1882-1888|Marianne Pedulla|
June 1985, Vol. 13, No. 2|The South Hadley Canal, 1795-1847|David Bell|
January 1985, Vol. 13, No. 1|Captain Myles Standish’s Military Role at Plymouth|John S. Erwin|
January 1985, Vol. 13, No. 1|General Edward A. Wild and Civil War Discrimination|Richard Reid|
January 1985, Vol. 13, No. 1|Women at Work: Views and Visions from the Pioneer Valley, 1870-1945|Donna S. Kenny|
January 1985, Vol. 13, No. 1|The Mount Tom Electric Railway at Holyoke, 1900-1915|Robert A. Young Jr.|
January 1985, Vol. 13, No. 1|Israel Williams and the Hampshire University Project (1761-1764)|William L. Welch|
January 1985, Vol. 13, No. 1|Novanglus and Massachusettensis: Different Conceptions of a Crisis|Jonathan M. Atkins|
June 1984, Vol. 12, No. 2|Boston’s Celebration of Peace in 1783 and 1784|Myron F. Wehtje|
June 1984, Vol. 12, No. 2|Reverend Lyman Whiting’s Test of Faith (Massachusetts 1854)|Robert M. Taylor Jr.|
June 1984, Vol. 12, No. 2|The Boston Vigilance Committee: A Reconsideration|Gary L. Collison|
June 1984, Vol. 12, No. 2|Mayor John F. Fitzgerald and Boston’s Schools, 1905-1913|James W. Fraser|
June 1984, Vol. 12, No. 2|The Emergency Relief Committee of Fitchburg, 1931-1934|Edmund B. Thomas, Jr.|
January 1984, Vol. 12, No. 1|American Officer Development in the Massachusetts Campaign, 1775-1776|Victor Daniel Brooks|
January 1984, Vol. 12, No. 1|Boston’s Response to Disorder in the Commonwealth, 1783-1787|Myron F. Wehtje|
January 1984, Vol. 12, No. 1|The Dedham Temporary Asylum for Discharged Female Prisoners, 1864-1909|Mary J. Bularzik|
January 1984, Vol. 12, No. 1|Edward Dickinson and the Amherst and Belchertown Railroad: A Lost Letter, 1851-1860|Daniel J. Lombardo|
January 1984, Vol. 12, No. 1|The Delayed Development of Parochial Education among Irish Catholics in Worcester|Timothy J. Meagher|
January 1984, Vol. 12, No. 1|The Professional Preparation of Parochial School Teachers, 1870-1940|Mary J. Oates|
June 1983, Vol. 11, No. 2|Edward Everett and the Constitutional Union Party of 1860|Thomas Brown|
June 1983, Vol. 11, No. 2|Francis William Bird: A Radical’s Progress through the Republican Party, 1836-1872|Donald B. Marti|
June 1983, Vol. 11, No. 2|Educating Irish Immigrants in Antebellum Lowell|Brian C. Mitchell|
June 1983, Vol. 11, No. 2|Irish Regiments in the Union Army: The Massachusetts Experience|William L. Burton|
January 1983, Vol. 11, No. 1|Constructing the Western Railroad: The Irish Dimension (Middlefield)|Edward O’Day|
January 1983, Vol. 11, No. 1|Scandal Behind the Convent Walls: The Know-Nothing Nunnery Committee of 1855|John R. Mulkern|
January 1983, Vol. 11, No. 1|The Shaping of Values in Nineteenth Century Massachusetts: The Case of Henry L. Dawes|Fred Nicklason|
January 1983, Vol. 11, No. 1|The Philosophy of Loyalism among the Ministers of Western Massachusetts|Thomas S. Martin|
June 1982, Vol. 10, No. 2|Integrating Men’s Universitys at the Turn of the Century|Mary Roth Walsh and Francis R. Walsh|
June 1982, Vol. 10, No. 2|Educational Patterns of French-Canadians in Holyoke, 1868-1910|Pater Haebler|
June 1982, Vol. 10, No. 2|The Administration of Welfare: A Comparative Analysis of Salem, Danvers, Deerfield, and Greenfield, 1821-1855|Louis J. Piccarello|
June 1982, Vol. 10, No. 2|Sylvester Judd: Historian of the Connecticut River Valley, 1789-1869|Altina Waller|
January 1982, Vol. 10, No. 1|Brahmins and Bullyboys: William Henry Cardinal O’Connell and Massachusetts Politics, 1907-1944|Robert O’Leary|
January 1982, Vol. 10, No. 1|Charity for a City in Crisis: Boston 1740-1775|Peter R. Virgadamo|
January 1982, Vol. 10, No. 1|‘The Hinterland of Belief’: The Revolutionary Correspondence of Edmund Quincy|Robert V. Sparks|
January 1982, Vol. 10, No. 1|Pulling Together and Drawing Apart: A Comment|Robert A. Gross|
June 1981, Vol. 9, No. 2|The Boston Pilot Reports the Civil War|Francis R. Walsh|
June 1981, Vol. 9, No. 2|Early Black Leadership in Collegiate Football: Massachusetts as a Pioneer, 1877-1917|Jack W. Berryman|
June 1981, Vol. 9, No. 2|Baker’s Chocolate: The Making of a Name|Elinor F. Oakes|
June 1981, Vol. 9, No. 2|The Celebration of the Fourth of July in Westfield, 1826-1853|Anne Marie Hickey|
June 1981, Vol. 9, No. 2|Boston Eight Hour Men, New York Marxists, and the Emergence of the International Labor Union: Prelude to the AFL|Kenneth Fones-Wolf|
January 1981, Vol. 9, No. 1|Bread and Roses: The Proletarianization of Women Workers in New England Textile Mills, 1837-1848|Laurie Nisonoff|
January 1981, Vol. 9, No. 1|Blackinton: A Case Study of Industrialization|Elizabeth Allegret Baker|
January 1981, Vol. 9, No. 1|Industry and Society in 19th Century Massachusetts: A Commentary|Carl Siracusa|
January 1981, Vol. 9, No. 1|Defending Orthodoxy in Massachusetts: 1650-1652|Timothy J. Sehr|
January 1981, Vol. 9, No. 1|‘This Poor People’: Seventeenth Century Massachusetts and the Poor|Charles R. Lee|
January 1981, Vol. 9, No. 1|The Puritan Self-Image and Enemies Within: Commentary|Francis Bremer|
June 1980, Vol. 8, No. 2|Exposure of Prostitution in Western Massachusetts|Karen A. Terrell|
June 1980, Vol. 8, No. 2|The Massachusetts Clergy and the New Deal|Monroe Billington and Cal Clark|
June 1980, Vol. 8, No. 2|Springfield’s Citizen-Soldiers in the Spanish-American War|Leo J. Leamy|
June 1980, Vol. 8, No. 2|Amos A. Lawrence and the Formation of the Constitutional Union Party: The Conservative Failure in 1860|Barry A. Crouch|
January 1980, Vol. 8, No. 1|The Presidential Election of 1932 in Western Massachusetts|Philip A. Grant, Jr.|
January 1980, Vol. 8, No. 1|Western Massachusetts in the Know-Nothing Years: An Analysis of Voting Patterns|John Mulkern|
January 1980, Vol. 8, No. 1|Sanitation and Cholera: Springfield and the 1866 Epidemic|Margaret M. Phaneuf|
January 1980, Vol. 8, No. 1|The Rise of the New Divinity in Western New England, 1740-1800|Joseph Conforti|
January 1980, Vol. 8, No. 1|In the Wake of the Awakening: The Politics of Purity in Granville, 1754-1776|Gregory Nobles|
June 1979, Vol. 7, No. 2|Early Medical Care in Deerfield|Mark C. Kestigan|
June 1979, Vol. 7, No. 2|Public Education in Holyoke, 1850-1873|Gary L. Coutchesne|
June 1979, Vol. 7, No. 2|Blacks in Springfield, 1868-1880: A Mobility Study|Joseph P. Lynch|
June 1979, Vol. 7, No. 2|Jonathon Ashley: Tory Minister|Robert C. Coughlin|
January 1979, Vol. 7, No. 1|Holyoke’s French-Canadian Community in Turmoil: The Role of the Church in Assimilation, 1869-1887|Peter Haebler|
January 1979, Vol. 7, No. 1|The Franchise and the Election of Representatives from Western Massachusetts: A Case Study|Robert S. Sliwoski (18th century)|
January 1979, Vol. 7, No. 1|The Massachusetts Suffrage Referendum of 1915|Robert Granfield|
Fall 1978|No issue published.||
Spring 1978, Vol. 6, No. 2|Anti-Catholic Prejudice in Early New England: The 1806 Daley-Halligan Murder Trial of Northampton|James M. Camposeo|
Spring 1978, Vol. 6, No. 2|Recreation in Chicopee 1853-1857|John A. Koziol|
Spring 1978, Vol. 6, No. 2|Congressman Ezekial Bacon of Massachusetts and the Coming of the War of 1812|William Barlow and David O. Powell|
Spring 1978, Vol. 6, No. 2|Research Aids: Genealogy in Western Massachusetts|Ellen M. Coty|
Fall 1977, Vol. 6, No. 1|Demographic Study of Easthampton, 1850-1870|Paul Hynek|
Fall 1977, Vol. 6, No. 1|William Douglas and the Beginnings of Medical Professionalism: Reinterpretation of the 1721 Boston Inoculation Controversy|James Schmotter|
Fall 1977, Vol. 6, No. 1|The History of the Canal System between New Haven and Northampton (1822-1847)|James Mark Camposeo|
Fall 1977, Vol. 6, No. 1|Thomas Sheldon: Forgotten Resident of Westfield (1787-1838)|Charles Bockelman|
Fall 1977, Vol. 6, No. 1|Witchcraft in Early Springfield: The Parsons Case|Christine Wrona (mid 17th century)|
Spring 1977, Vol. 5, No. 2|The Last Shall Be First: The Amherst University Days of Calvin Coolidge|Thomas W. Kilmartin|
Spring 1977, Vol. 5, No. 2|An Overview of the Criminal Justice System of Hampshire County, 1677-1728|Evlyn Belz Russell|
Spring 1977, Vol. 5, No. 2|The Silk Industry in Northampton|Ronald Savoie|
Spring 1977, Vol. 5, No. 2|To Secure the Party: Henry L. Dawes and the Politics of Reconstruction|Steven J. Arcanti|
Spring 1977, Vol. 5, No. 2|Greek Immigrants in Springfield, 1884-1944|George T. Eliopoulos|
Fall 1976|No issue published. ||
Spring 1976, Vol. 5, No. 1|The Evolution, Tone, and Milieu of New England’s Greatest Newspaper: The Springfield Republican, 1824-1920|William P. Koscher|
Spring 1976, Vol. 5, No. 1|Westfield’s Black Community, 1755-1905|Michael Camerota|
Spring 1976, Vol. 5, No. 1|1876: Local Observance of the Centennial|Joanne E. Moller|
Spring 1976, Vol. 5, No. 1|George Thompson and the Springfield 1851 ‘Anti-Abolition’ Riot|Theresa A. Harrison|
Fall 1975, Vol. 4, No. 2|’A Lamentable and Woeful Sight’: The Indian Attack on Springfield|Richard J. Pinkos|
Fall 1975, Vol. 4, No. 2|The Real William Pynchon: Merchant and Politician|Stephen J. Cote|
Fall 1975, Vol. 4, No. 2|Death in Colonial New England|James J. Naglack|
Fall 1975, Vol. 4, No. 2|The Stockbridge Indian in the American Revolution|Deirdre Almeida|
Spring 1975, Vol. 4, No. 1|Rural Medical Practice in Early 19th Century New England|Joseph Carvalho III|
Spring 1975, Vol. 4, No. 1|The Hopedale Community|David M. Coffey|
Spring 1975, Vol. 4, No. 1|Prohibition and its Effect on Western Massachusetts, 1919-1920|Debra P. Sansoucy|
Spring 1975, Vol. 4, No. 1|Housing in Holyoke and its Effect on Family Life, 1860-1910|Paul N. Dubovik|
Spring 1975, Vol. 4, No. 1|Quabbin Reservoir: The Elimination of Four Small New England Towns|James Naglack|
Fall 1974, Vol. 3, No. 2|The Epidemic Cholera in Springfield 1832 and 1849|John E. Doyle|
Fall 1974, Vol. 3, No. 2|The Railroad Comes to Springfield|Mark Mackler|
Fall 1974, Vol. 3, No. 2|Springfield during the Civil War Years 1861-1865|Edward M. Morin|
Fall 1974, Vol. 3, No. 2|Private John E. Bisbee, the 52nd Massachusetts Volunteers, and the Banks Expedition by David Boilard and Joseph Carvalho III||
Spring 1974, Vol. 3, No. 1|The French in Holyoke, 1850-1900|Therese Bilodeau|
Spring 1974, Vol. 3, No. 1|Chicopee’s Irish, 1830-1875|John E. Doyle|
Spring 1974, Vol. 3, No. 1|The Westfield Home Front During the Civil War|Madeline Warner|
Spring 1974, Vol. 3, No. 1|Noah Atwater (1752-1799) and His Contribution to Westfield Life|Kathleen Girardi|
Spring 1974, Vol. 3, No. 1|The Social Effects of the Construction of the Wachusett Reservoir on Boylston and West Boylston|Joseph Carvalho III and Robert Everett|
Fall 1973, Vol. 2, No. 2|Marriage Customs in Colonial New England|Irene Ktorides|
Fall 1973, Vol. 2, No. 2|The Charleston State Prison (1804-1878)|Anne Bauer|
Fall 1973, Vol. 2, No. 2|Benjamin Franklin and the Inoculation Controversy|Kathe Palermo Gwozdz|
Spring 1973, Vol. 2, No. 1|The Effect of the Civil War on Shaker Societies|Stephen Paterwic|
Spring 1973, Vol. 2, No. 1|Life and Death at Andersonville Prison|Ann Clearly|
Spring 1973, Vol. 2, No. 1|The Horse Distemper of 1872 and its Effect on Urban Transportation|Sr. Denise Granger|
Spring 1973, Vol. 2, No. 1|Curtin’s Atlantic Slave Trade: An Analysis From Two Perspectives|Robert T. Brown and David L. Chandler|
Spring 1973, Vol. 2, No. 1|An Antiwar Poem of the Last Century edited|William K. Dunlap, with introduction|
Fall 1972, Vol. 1, No. 2|The Trolley Car as a Social Factor: Springfield, Massachusetts|Scott R. Johnson|
Fall 1972, Vol. 1, No. 2|The Effect of United States Land Allotment and Religious Policies on American Indian Culture|Janet Dimock|
Fall 1972, Vol. 1, No. 2|The Social Impact of Radio By Douglas Stanley||
Spring 1972, Vol. 1, No. 1|William L. Bulkley and the New York Negro, 1890-1910|George Psychas|
Spring 1972, Vol. 1, No. 1|William Lloyd Garrison and the Election of 1864|Paul H. Julian|
Spring 1972, Vol. 1, No. 1|The Indians of Eastern Massachusetts, 1620-1645|Frederick F. Harling|
Spring 1972, Vol. 1, No. 1|Model Cities, 1966|Scott R. Johnson|


## Questia
[Questia][questia] 提供 [Historical Journal of Massachusetts](https://www.questia.com/library/p62471/historical-journal-of-massachusetts)自1998年起的文章目錄。

>Historical Journal of Massachusetts is an academic journal focusing on Massachusetts.

Vol|No.|Title
---|---|---
Vol. 47| No. 1|[American Tempest: How the Boston Tea Party Sparked a Revolution](https://www.questia.com/read/1P4-2166309165/american-tempest-how-the-boston-tea-party-sparked)
Vol. 47| No. 1|[An Exceptional Benefactor: Larry Gwozdz](https://www.questia.com/read/1P4-2166309038/an-exceptional-benefactor-larry-gwozdz)
Vol. 47| No. 1|[At Sword's Point: The United Electrical Workers Union and the Greenfield Tap & Die Company](https://www.questia.com/read/1P4-2166309414/at-sword-s-point-the-united-electrical-workers-union)
Vol. 47| No. 1|[Creating a Class around the Lives of the Civil War Dead: The Worcester Soldiers' Monument Biography Project](https://www.questia.com/read/1P4-2166309495/creating-a-class-around-the-lives-of-the-civil-war)
Vol. 47| No. 1|[From Slate to Marble: Gravestone Carving Traditions in Eastern Massachusetts, 1750-1850](https://www.questia.com/read/1P4-2166308830/from-slate-to-marble-gravestone-carving-traditions)
Vol. 47| No. 1|[From Slate to Marble: Gravestone Carving Traditions in Eastern Massachusetts, 1770-1870](https://www.questia.com/read/1P4-2166309473/from-slate-to-marble-gravestone-carving-traditions)
Vol. 47| No. 1|[Harry Clark Bentley: A Pioneering Accountant and the Founder of Bentley University (1877-1967)](https://www.questia.com/read/1P4-2166309136/harry-clark-bentley-a-pioneering-accountant-and-the)
Vol. 47| No. 1|[JFK in the Senate: Pathway to the Presidency](https://www.questia.com/read/1P4-2166309320/jfk-in-the-senate-pathway-to-the-presidency)
Vol. 47| No. 1|[King Philip's War: Civil War in New England, 1675-1676](https://www.questia.com/read/1P4-2166309361/king-philip-s-war-civil-war-in-new-england-1675-1676)
Vol. 47| No. 1|[Lizzie Borden on Trial: Murder, Ethnicity, and Gender](https://www.questia.com/read/1P4-2166307556/lizzie-borden-on-trial-murder-ethnicity-and-gender)
Vol. 47| No. 1|[Practicing Medicine in a Black Regiment: The Civil War Diary of Bert G. Wilder, 55th Massachusetts](https://www.questia.com/read/1P4-2166308471/practicing-medicine-in-a-black-regiment-the-civil)
Vol. 47| No. 1|[Sightseeking: Clues to the Landscape History of New England](https://www.questia.com/read/1P4-2166307323/sightseeking-clues-to-the-landscape-history-of-new)
Vol. 47| No. 1|[Survival of the Pilgrims: A Reevaluation of the Lethal Epidemic among the Wampanoag](https://www.questia.com/read/1P4-2166308906/survival-of-the-pilgrims-a-reevaluation-of-the-lethal)
Vol. 47| No. 1|[The Ku Klux Klan in 1920s Massachusetts](https://www.questia.com/read/1P4-2166309395/the-ku-klux-klan-in-1920s-massachusetts)
Vol. 47| No. 1|[The Remarkable Education of John Quincy Adams](https://www.questia.com/read/1P4-2166309057/the-remarkable-education-of-john-quincy-adams)
Vol. 46| No. 2|[A Kiss from Thermopylae: Emily Dickinson and Law](https://www.questia.com/read/1P4-2058256422/a-kiss-from-thermopylae-emily-dickinson-and-law)
Vol. 46| No. 2|[Boston's New Immigrants and New Economy, 1965-2015](https://www.questia.com/read/1P4-2058256463/boston-s-new-immigrants-and-new-economy-1965-2015)
Vol. 46| No. 2|[Confronting Urban Legacy: Rediscovering Hartford and New England's Forgotten Cities](https://www.questia.com/read/1P4-2058256616/confronting-urban-legacy-rediscovering-hartford-and)
Vol. 46| No. 2|[Hatfield's Forgotten Industrial Past: The Porter-McLeod Machine Works and the Connecticut River Valley Industrial Economy, 1870-1970](https://www.questia.com/read/1P4-2058262035/hatfield-s-forgotten-industrial-past-the-porter-mcleod)
Vol. 46| No. 2|[In Search of Sacco and Vanzetti: Double Lives, Troubled Times, and the Massachusetts Murder Case That Shook the World](https://www.questia.com/read/1P4-2058257258/in-search-of-sacco-and-vanzetti-double-lives-troubled)
Vol. 46| No. 2|[Latino City: Immigration and Urban Crisis in Lawrence, Massachusetts, 1945-2000](https://www.questia.com/read/1P4-2058258028/latino-city-immigration-and-urban-crisis-in-lawrence)
Vol. 46| No. 2|[Romancing the Stone: Invented Irish and Native American Memories in Northampton](https://www.questia.com/read/1P4-2058262110/romancing-the-stone-invented-irish-and-native-american)
Vol. 46| No. 2|[Scoundrels Who Made America Great](https://www.questia.com/read/1P4-2058257323/scoundrels-who-made-america-great)
Vol. 46| No. 2|[Shays's Rebellion: Authority and Distress in Post-Revolutionary America](https://www.questia.com/read/1P4-2058256584/shays-s-rebellion-authority-and-distress-in-post-revolutionary)
Vol. 46| No. 2|[The Glass Universe: How the Ladies of the Harvard Observatory Took the Measure of the Stars](https://www.questia.com/read/1P4-2058256442/the-glass-universe-how-the-ladies-of-the-harvard)
Vol. 46| No. 2|[When the Chinese Came to Massachusetts: Representations of Race, Labor, Religion, and Citizenship in the 1870 Press](https://www.questia.com/read/1P4-2058257291/when-the-chinese-came-to-massachusetts-representations)
Vol. 46| No. 1|[A Short History of Boston](https://www.questia.com/read/1P4-1980490110/a-short-history-of-boston)
Vol. 46| No. 1|[David Ruggles: A Radical Black Abolitionist and the Underground Railroad in New York City](https://www.questia.com/read/1P4-1980493276/david-ruggles-a-radical-black-abolitionist-and-the)
Vol. 46| No. 1|[Dickens and Massachusetts: The Lasting Legacy of the Commonwealth Visits](https://www.questia.com/read/1P4-1980493268/dickens-and-massachusetts-the-lasting-legacy-of-the)
Vol. 46| No. 1|[Dissenting Puritans: Anne Hutchinson and Mary Dyer](https://www.questia.com/read/1P4-1980490108/dissenting-puritans-anne-hutchinson-and-mary-dyer)
Vol. 46| No. 1|[For a Short Time Only: Itinerants and the Resurgence of Popular Culture in Early America](https://www.questia.com/read/1P4-1980494740/for-a-short-time-only-itinerants-and-the-resurgence)
Vol. 46| No. 1|[Horace Holley: Transylvania University and the Making of Liberal Education in the Early American Republic](https://www.questia.com/read/1P4-1980490096/horace-holley-transylvania-university-and-the-making)
Vol. 46| No. 1|[John Adams, Political Moderation, and the 1820 Massachusetts Constitutional Convention: A Reappraisal](https://www.questia.com/read/1P4-1980490107/john-adams-political-moderation-and-the-1820-massachusetts)
Vol. 46| No. 1|[John Quincy Adams: Militant Spirit](https://www.questia.com/read/1P4-1980492981/john-quincy-adams-militant-spirit)
Vol. 46| No. 1|[Lovewell's Fight: War, Death, and Memory in Borderland New England](https://www.questia.com/read/1P4-1980492149/lovewell-s-fight-war-death-and-memory-in-borderland)
Vol. 46| No. 1|[Nathaniel Bowditch and the Power of Numbers: How a Nineteenth-Century Man of Business, Science, and the Sea Changed American Life](https://www.questia.com/read/1P4-1980490105/nathaniel-bowditch-and-the-power-of-numbers-how-a)
Vol. 46| No. 1|[New Bedford's Infamous 1983 Rape Case: Defending the Portuguese-American Community](https://www.questia.com/read/1P4-1980490106/new-bedford-s-infamous-1983-rape-case-defending-the)
Vol. 46| No. 1|[New England Beginnings: Commemorating the Cultures That Shaped New England](https://www.questia.com/read/1P4-1980490113/new-england-beginnings-commemorating-the-cultures)
Vol. 46| No. 1|[The Rise and Demise of the Connecticut River Valley's Industrial Economy](https://www.questia.com/read/1P4-1980492173/the-rise-and-demise-of-the-connecticut-river-valley-s)
Vol. 46| No. 1|["This Would Be a Ghost Town": Urban Crisis and Latino Migration in Lawrence, 1945-2000](https://www.questia.com/read/1P4-1980493447/this-would-be-a-ghost-town-urban-crisis-and-latino)
Vol. 46| No. 1|[To Plead Our Own Cause: African Americans in Massachusetts and the Making of the Anti-Slavery Movement](https://www.questia.com/read/1P4-1980492698/to-plead-our-own-cause-african-americans-in-massachusetts)
Vol. 46| No. 1|[Tyrannicide: Forging an American Law of Slavery in Revolutionary South Carolina and Massachusetts](https://www.questia.com/read/1P4-1980493818/tyrannicide-forging-an-american-law-of-slavery-in)
Vol. 45| No. 2|[Confronting Decline: The Political Economy of Deindustrialization in Twentieth-Century New England](https://www.questia.com/read/1P4-1919409163/confronting-decline-the-political-economy-of-deindustrialization)
Vol. 45| No. 2|[Founding Mothers: The Women Who Raised Our Nation](https://www.questia.com/read/1P4-1919409175/founding-mothers-the-women-who-raised-our-nation)
Vol. 45| No. 2|[Mabel Loomis Todd: The Civic Impulses and Civic Engagement of an Accidental Activist](https://www.questia.com/read/1P4-1919409510/mabel-loomis-todd-the-civic-impulses-and-civic-engagement)
Vol. 45| No. 2|[Native Apostles: Black and Indian Missionaries in the British Atlantic World](https://www.questia.com/read/1P4-1919409394/native-apostles-black-and-indian-missionaries-in)
Vol. 45| No. 2|[Robert Love’s Warnings: Searching for Strangers in Colonial Boston](https://www.questia.com/read/1P4-1919409581/robert-love-s-warnings-searching-for-strangers-in)
Vol. 45| No. 2|[The Fever of 1721: The Epidemic That Revolutionized Medicine and American Politics](https://www.questia.com/read/1P4-1919409308/the-fever-of-1721-the-epidemic-that-revolutionized)
Vol. 45| No. 2|[The Great Depression in the North Berkshires: The New Deal, Textile Union Organizing, and a Pro-Labor Mayor](https://www.questia.com/read/1P4-1919409049/the-great-depression-in-the-north-berkshires-the)
Vol. 45| No. 2|["The Unity of the Republic and the Freedom of an Oppressed Race": Fitchburg's Civil War Soldiers' Monument, 1874](https://www.questia.com/read/1P4-1919409319/the-unity-of-the-republic-and-the-freedom-of-an-oppressed)
Vol. 45| No. 1|[A Generation of Hope, Pain, and Heartbreak: The Worcester Molder.S Union, 1904–1921](https://www.questia.com/read/1P4-1935363083/a-generation-of-hope-pain-and-heartbreak-the-worcester)
Vol. 45| No. 1|[A Massachusetts Entrepreneur in Gold Rush California: Jonas Clark and the Economic Foundations of Clark University](https://www.questia.com/read/1P4-1935227091/a-massachusetts-entrepreneur-in-gold-rush-california)
Vol. 45| No. 1|[American Passage: The Communications Frontier in Early New England](https://www.questia.com/read/1P4-1935359495/american-passage-the-communications-frontier-in-early)
Vol. 45| No. 1|[Conquering Winter: Snow Removal from Boston's Streets from the Colonial Period to the Present](https://www.questia.com/read/1P4-1935227345/conquering-winter-snow-removal-from-boston-s-streets)
Vol. 45| No. 1|[Emily Greene Balch: The Long Road to Internationalism](https://www.questia.com/read/1P4-1935225976/emily-greene-balch-the-long-road-to-internationalism)
Vol. 45| No. 1|["I Want to Go to Jail": The Woman's Party Reception for President Wilson in Boston, 1919](https://www.questia.com/read/1P4-1935226988/i-want-to-go-to-jail-the-woman-s-party-reception)
Vol. 45| No. 1|[Louisa May Alcott: A Literary Biography](https://www.questia.com/read/1P4-1935227086/louisa-may-alcott-a-literary-biography)
Vol. 45| No. 1|[Phillis Wheatley: Biography of a Genius in Bondage](https://www.questia.com/read/1P4-1935233545/phillis-wheatley-biography-of-a-genius-in-bondage)
Vol. 45| No. 1|[Rebels in Paradise: Sketches of Northampton Abolitionists](https://www.questia.com/read/1P4-1935227022/rebels-in-paradise-sketches-of-northampton-abolitionists)
Vol. 45| No. 1|[Revolutionary Summer: The Birth of American Independence](https://www.questia.com/read/1P4-1935225383/revolutionary-summer-the-birth-of-american-independence)
Vol. 45| No. 1|[Smallpox at the Siege of Boston: "Vigilance against This Most Dangerous Enemy"](https://www.questia.com/read/1P4-1935227354/smallpox-at-the-siege-of-boston-vigilance-against)
Vol. 45| No. 1|[The Amistad Rebellion: An Atlantic Odyssey of Slavery and Freedom](https://www.questia.com/read/1P4-1935227459/the-amistad-rebellion-an-atlantic-odyssey-of-slavery)
Vol. 45| No. 1|[The Great Lawrence Textile Strike of 1912: New Scholarship on the Bread & Roses Strike](https://www.questia.com/read/1P4-1935241804/the-great-lawrence-textile-strike-of-1912-new-scholarship)
Vol. 45| No. 1|[The Life of William Apess, Pequot](https://www.questia.com/read/1P4-1935226014/the-life-of-william-apess-pequot)
Vol. 44| No. 2|[Afterword](https://www.questia.com/read/1P3-4311913171/afterword)
Vol. 44| No. 2|[Captives on the Move: Tracing the Transatlantic Movements of Africans from the Caribbean to Colonial New England](https://www.questia.com/read/1P3-4311913211/captives-on-the-move-tracing-the-transatlantic-movements)
Vol. 44| No. 2|[Hannah Packard James, Newton Librarian: Economic Motives of Nineteenth-Century Professional Women](https://www.questia.com/read/1P3-4311913191/hannah-packard-james-newton-librarian-economic-motives)
Vol. 44| No. 2|[Homegrown Terror: Benedict Arnold and the Burning of New London](https://www.questia.com/read/1P3-4311913241/homegrown-terror-benedict-arnold-and-the-burning)
Vol. 44| No. 2|[Prelude to Revolution: The Salem Gunpowder Raid of 1775](https://www.questia.com/read/1P3-4311913221/prelude-to-revolution-the-salem-gunpowder-raid-of)
Vol. 44| No. 2|[Sisters in the Faith: Shaker Women and Equality of the Sexes](https://www.questia.com/read/1P3-4311913251/sisters-in-the-faith-shaker-women-and-equality-of)
Vol. 44| No. 2|[The "Infamas Govener": Francis Bernard and the Origins of the American Revolution](https://www.questia.com/read/1P3-4311913231/the-infamas-govener-francis-bernard-and-the-origins)
Vol. 44| No. 2|[The Making of an Irish and a Jewish Boston, 1820-1900](https://www.questia.com/read/1P3-4311913181/the-making-of-an-irish-and-a-jewish-boston-1820-1900)
Vol. 44| No. 2|[The Migration of Former Slaves to Worcester: Hopes and Dreams Deferred, 1862-1900](https://www.questia.com/read/1P3-4311913161/the-migration-of-former-slaves-to-worcester-hopes)
Vol. 44| No. 2|[The Pathos of Distance Memory and Revision in S. N. Behrman's the Worcester Account](https://www.questia.com/read/1P3-4311913201/the-pathos-of-distance-memory-and-revision-in-s-n)
Vol. 44| No. 2|[Yankee Brutalism: Concrete Architecture in New England, 1957-1977](https://www.questia.com/read/1P3-4311913151/yankee-brutalism-concrete-architecture-in-new-england)
Vol. 44| No. 1|[A Corporal's Story: Civil War Recollections of the Twelfth Massachusetts](https://www.questia.com/read/1P3-4311913401/a-corporal-s-story-civil-war-recollections-of-the)
Vol. 44| No. 1|[Damnable Heresy: William Pynchon, the Indians, and the First Book Banned (and Burned) in Boston](https://www.questia.com/read/1P3-4311913361/damnable-heresy-william-pynchon-the-indians-and)
Vol. 44| No. 1|[Frederic Crowninshield: A Renaissance Man in the Gilded Age](https://www.questia.com/read/1P3-4311913421/frederic-crowninshield-a-renaissance-man-in-the-gilded)
Vol. 44| No. 1|["Hospitality Is the Best Form of Propaganda": German Prisoners of War in Western Massachusetts, 1944-1946](https://www.questia.com/read/1P3-4311913301/hospitality-is-the-best-form-of-propaganda-german)
Vol. 44| No. 1|[Massachusetts Cranberry Culture: A History from Bog to Table](https://www.questia.com/read/1P3-4311913371/massachusetts-cranberry-culture-a-history-from-bog)
Vol. 44| No. 1|[Nation Builder: John Quincy Adams and the Grand Strategy of the Republic](https://www.questia.com/read/1P3-4311913391/nation-builder-john-quincy-adams-and-the-grand-strategy)
Vol. 44| No. 1|[Sculptor Theodora Alice Ruggles Kitson: "A Woman Genius"](https://www.questia.com/read/1P3-4311913281/sculptor-theodora-alice-ruggles-kitson-a-woman-genius)
Vol. 44| No. 1|[Searching for German POWs: A Researcher's Personal Odyssey](https://www.questia.com/read/1P3-4311913311/searching-for-german-pows-a-researcher-s-personal)
Vol. 44| No. 1|[Stricken: The Impact of Disease on Two Massachusetts Families, 1911-50](https://www.questia.com/read/1P3-4311913321/stricken-the-impact-of-disease-on-two-massachusetts)
Vol. 44| No. 1|[The Court-Martial of Paul Revere: A Son of Liberty and America's Forgotten Military Disaster](https://www.questia.com/read/1P3-4311913381/the-court-martial-of-paul-revere-a-son-of-liberty)
Vol. 44| No. 1|[The Dilemma of Interracial Marriage: The Boston NAACP and the National Equal Rights League, 1912-1927](https://www.questia.com/read/1P3-4311913351/the-dilemma-of-interracial-marriage-the-boston-naacp)
Vol. 44| No. 1|[The Hub's Metropolis: Greater Boston's Development from Railroad Suburbs to Smart Growth](https://www.questia.com/read/1P3-4311913411/the-hub-s-metropolis-greater-boston-s-development)
Vol. 44| No. 1|[The New Boston: A People's History](https://www.questia.com/read/1P3-4311913291/the-new-boston-a-people-s-history)
Vol. 44| No. 1|[The Ravages of Encephalitis Lethargica](https://www.questia.com/read/1P3-4311913331/the-ravages-of-encephalitis-lethargica)
Vol. 44| No. 1|[The Women of the Hopedale Sewing Circle, 1848-63](https://www.questia.com/read/1P3-4311913341/the-women-of-the-hopedale-sewing-circle-1848-63)
Vol. 43| No. 2|[Abigail Adams](https://www.questia.com/read/1P3-3810400581/abigail-adams)
Vol. 43| No. 2|[Away Offshore: Nantucket Island and Its People, 1602-1890](https://www.questia.com/read/1P3-3810400571/away-offshore-nantucket-island-and-its-people-1602-1890)
Vol. 43| No. 2|[JFK's Last Hundred Days: The Transformation of a Man and the Emergence of a Great President](https://www.questia.com/read/1P3-3810400611/jfk-s-last-hundred-days-the-transformation-of-a-man)
Vol. 43| No. 2|[My Dearest Friend: Letters of Abigail and John Adams. with a Foreword by Joseph J. Ellis](https://www.questia.com/read/1P3-3810400591/my-dearest-friend-letters-of-abigail-and-john-adams)
Vol. 43| No. 2|[Naked Quakers Who Were Not So Naked: Seventeenth-Century Quaker Women in the Massachusetts Bay Colony](https://www.questia.com/read/1P3-3810400551/naked-quakers-who-were-not-so-naked-seventeenth-century)
Vol. 43| No. 2|[Phillis Wheatley: Researching a Life](https://www.questia.com/read/1P3-3810400541/phillis-wheatley-researching-a-life)
Vol. 43| No. 2|[Protest Politics: Liberal Activism in Massachusetts, 1974-1990](https://www.questia.com/read/1P3-3810400521/protest-politics-liberal-activism-in-massachusetts)
Vol. 43| No. 2|[Remembering the Forgotten War: The Enduring Legacies of the U.S.-Mexican War](https://www.questia.com/read/1P3-3810400601/remembering-the-forgotten-war-the-enduring-legacies)
Vol. 43| No. 2|[Splitting the Vote in Massachusetts: Father Charles E. Coughlin, the Union Party, and Political Divisions in the 1936 Presidential and Senate Elections](https://www.questia.com/read/1P3-3810400561/splitting-the-vote-in-massachusetts-father-charles)
Vol. 43| No. 2|[The People's Schools for Teachers of the People: The Development of Massachusetts' State Teachers Colleges](https://www.questia.com/read/1P3-3810400511/the-people-s-schools-for-teachers-of-the-people-the)
Vol. 43| No. 2|[The Puerto Rican Community of Western Massachusetts, 1898-1960](https://www.questia.com/read/1P3-3810400531/the-puerto-rican-community-of-western-massachusetts)
Vol. 43| No. 1|[Bunker Hill: A City, A Siege, A Revolution](https://www.questia.com/read/1P3-3810426381/bunker-hill-a-city-a-siege-a-revolution)
Vol. 43| No. 1|[First Founders: American Puritans and Puritanism in an Atlantic World](https://www.questia.com/read/1P3-3810426371/first-founders-american-puritans-and-puritanism-in)
Vol. 43| No. 1|[New Bedford's Civil War](https://www.questia.com/read/1P3-3810426411/new-bedford-s-civil-war)
Vol. 43| No. 1|[No Closure: Catholic Practice and Boston's Parish Shutdowns](https://www.questia.com/read/1P3-3810426421/no-closure-catholic-practice-and-boston-s-parish)
Vol. 43| No. 1|[Remembering Massachusetts State Normal Schools: Pioneers in Teacher Education](https://www.questia.com/read/1P3-3810426331/remembering-massachusetts-state-normal-schools-pioneers)
Vol. 43| No. 1|[Shays' Rebellion: Reclaiming the Revolution](https://www.questia.com/read/1P3-3810426351/shays-rebellion-reclaiming-the-revolution)
Vol. 43| No. 1|[So Ends This Day: The Portuguese in American Whaling 1765-1927](https://www.questia.com/read/1P3-3810426391/so-ends-this-day-the-portuguese-in-american-whaling)
Vol. 43| No. 1|["Something of the Character Within": Children of the Swift River Valley](https://www.questia.com/read/1P3-3810426341/something-of-the-character-within-children-of-the)
Vol. 43| No. 1|[The Congregational Way Assailed: The Reverend Thomas Goss in Revolutionary Massachusetts](https://www.questia.com/read/1P3-3810426401/the-congregational-way-assailed-the-reverend-thomas)
Vol. 43| No. 1|[Three Olmsted "Parks" That Weren't: The Unrealized Emerald Necklace and Its Consequences](https://www.questia.com/read/1P3-3810426321/three-olmsted-parks-that-weren-t-the-unrealized)
Vol. 43| No. 1|["With Good Will Doing Service": The Charitable Irish Society of Boston (1737-1857) 1](https://www.questia.com/read/1P3-3810426361/with-good-will-doing-service-the-charitable-irish)
Vol. 42| No. 1|[A History of Jewish Plymouth](https://www.questia.com/read/1P3-3232184101/a-history-of-jewish-plymouth)
Vol. 42| No. 1|[Architecture and Academe: College Buildings in New England before 1860](https://www.questia.com/read/1P3-3232183961/architecture-and-academe-college-buildings-in-new)
Vol. 42| No. 1|[Becoming MIT: Moments of Decision](https://www.questia.com/read/1P3-3232183921/becoming-mit-moments-of-decision)
Vol. 42| No. 1|[Cholera in Worcester: A Study of the Nineteenth-Century Public Health Movement](https://www.questia.com/read/1P3-3232183741/cholera-in-worcester-a-study-of-the-nineteenth-century)
Vol. 42| No. 1|[Defiance of the Patriots: The Boston Tea Party & the Making of America/Rebels Rising: Cities and the American Revolution](https://www.questia.com/read/1P3-3232183841/defiance-of-the-patriots-the-boston-tea-party-the)
Vol. 42| No. 1|[Imagining New England: Explorations of Regional Identity from the Pilgrims to the Mid-Twentieth Century](https://www.questia.com/read/1P3-3232183811/imagining-new-england-explorations-of-regional-identity)
Vol. 42| No. 1|[Making Slavery History: Abolitionism and the Politics of Memory in Massachusetts](https://www.questia.com/read/1P3-3232183991/making-slavery-history-abolitionism-and-the-politics)
Vol. 42| No. 1|["Mr. Sprague Did Not Believe the People Would Do It": The Sprague Electric Strike in North Adams, 1970](https://www.questia.com/read/1P3-3232183511/mr-sprague-did-not-believe-the-people-would-do-it)
Vol. 42| No. 1|[Northampton Silk Threads: The Asia Connection](https://www.questia.com/read/1P3-3232183331/northampton-silk-threads-the-asia-connection)
Vol. 42| No. 1|[Shadows in the Valley: A Cultural History of Illness, Death, and Loss in New England, 1840-1916](https://www.questia.com/read/1P3-3232184051/shadows-in-the-valley-a-cultural-history-of-illness)
Vol. 42| No. 1|[The Hub's Metropolis: A Glimpse into Greater Boston's Development](https://www.questia.com/read/1P3-3232183481/the-hub-s-metropolis-a-glimpse-into-greater-boston-s)
Vol. 42| No. 1|[The Lost History of Slaves and Slave Owners in Billerica, Massachusetts, 1655-1790](https://www.questia.com/read/1P3-3232183671/the-lost-history-of-slaves-and-slave-owners-in-billerica)
Vol. 42| No. 1|[Those about Him Remained Silent: The Battle over W.E.B. Du Bois](https://www.questia.com/read/1P3-3232184071/those-about-him-remained-silent-the-battle-over-w-e-b)
Vol. 42| No. 1|[Waterpower in Lowell: Engineering and Industry in Nineteenth-Century America](https://www.questia.com/read/1P3-3232183901/waterpower-in-lowell-engineering-and-industry-in)
Vol. 42| No. 1|[When America First Met China: An Exotic History of Tea, Drugs, and Money in the Age of Sail](https://www.questia.com/read/1P3-3232183871/when-america-first-met-china-an-exotic-history-of)
Vol. 42| No. 1|[Women Reformers and the Limitations of Labor Politics in Massachusetts, 1874-1912](https://www.questia.com/read/1P3-3232183611/women-reformers-and-the-limitations-of-labor-politics)
Vol. 41| No. 2|[Beyond the New England Frontier: Native American Historiography since 1965](https://www.questia.com/read/1P3-3227591001/beyond-the-new-england-frontier-native-american-historiography)
Vol. 41| No. 2|[Called to Serve: Stories of Men and Women Confronted by the Vietnam War Draft](https://www.questia.com/read/1P3-3227590941/called-to-serve-stories-of-men-and-women-confronted)
Vol. 41| No. 2|[Dark Tide: The Great Boston Molasses Flood of 1919](https://www.questia.com/read/1P3-3227591081/dark-tide-the-great-boston-molasses-flood-of-1919)
Vol. 41| No. 2|[Evangelicals at a Crossroads: Revivalism and Social Reform in Boston, 1860-1910](https://www.questia.com/read/1P3-3227591061/evangelicals-at-a-crossroads-revivalism-and-social)
Vol. 41| No. 2|[Extravaganza King: Robert Barnet and Boston Musical Theater](https://www.questia.com/read/1P3-3227591071/extravaganza-king-robert-barnet-and-boston-musical)
Vol. 41| No. 2|[Farewell to Factory Towns?](https://www.questia.com/read/1P3-3227590961/farewell-to-factory-towns)
Vol. 41| No. 2|[Harriet Hosmer: A Cultural Biography](https://www.questia.com/read/1P3-3227591031/harriet-hosmer-a-cultural-biography)
Vol. 41| No. 2|[John F. Kennedy](https://www.questia.com/read/1P3-3227590901/john-f-kennedy)
Vol. 41| No. 2|[John F. Kennedy: Public Perception and Campaign Strategy in 1946](https://www.questia.com/read/1P3-3227591011/john-f-kennedy-public-perception-and-campaign-strategy)
Vol. 41| No. 2|[President of the Other America: Robert Kennedy & the Politics of Poverty](https://www.questia.com/read/1P3-3227590921/president-of-the-other-america-robert-kennedy-the)
Vol. 41| No. 2|[Surviving Iraq: Soldiers' Stories/American Veterans on War: Personal Stories from WW II to Afghanistan](https://www.questia.com/read/1P3-3227590951/surviving-iraq-soldiers-stories-american-veterans)
Vol. 41| No. 2|[The Forgotten Founding Father: Noah Webster's Obsession and the Creation of an American Culture](https://www.questia.com/read/1P3-3227591021/the-forgotten-founding-father-noah-webster-s-obsession)
Vol. 41| No. 2|[The Given Day: A Novel](https://www.questia.com/read/1P3-3227590891/the-given-day-a-novel)
Vol. 41| No. 2|[The Lives of Margaret Fuller: A Biography](https://www.questia.com/read/1P3-3227591041/the-lives-of-margaret-fuller-a-biography)
Vol. 41| No. 2|[The Robbins Family of East Lexington, Massachusetts: Furriers and Their Clothing, 1775-1825](https://www.questia.com/read/1P3-3227590971/the-robbins-family-of-east-lexington-massachusetts)
Vol. 41| No. 2|[The Veterans Education Project of Amherst, Massachusetts](https://www.questia.com/read/1P3-3227590991/the-veterans-education-project-of-amherst-massachusetts)
Vol. 41| No. 2|[Vietnam Experiences: A New York Draftee and a Northampton Draft Counselor](https://www.questia.com/read/1P3-3227590981/vietnam-experiences-a-new-york-draftee-and-a-northampton)
Vol. 41| No. 2|["We Will Be Satisfied with Nothing Less": The African American Struggle for Equal Rights in the North during Reconstruction](https://www.questia.com/read/1P3-3227591051/we-will-be-satisfied-with-nothing-less-the-african)
Vol. 40| No. 1|[American Heroes: Profiles of Men and Women Who Shaped Early America](https://www.questia.com/read/1P3-2721442851/american-heroes-profiles-of-men-and-women-who-shaped)
Vol. 40| No. 1|[A Widening Sphere: Evolving Cultures at MIT](https://www.questia.com/read/1P3-2721442911/a-widening-sphere-evolving-cultures-at-mit)
Vol. 40| No. 1|[Bewitched and Bewildered: Salem Witches, Empty Factories, and Tourist Dollars](https://www.questia.com/read/1P3-2721442781/bewitched-and-bewildered-salem-witches-empty-factories)
Vol. 40| No. 1|[Bird Strike: The Crash of the Boston Electra](https://www.questia.com/read/1P3-2721442941/bird-strike-the-crash-of-the-boston-electra)
Vol. 40| No. 1|[Bread, Roses, and Other Possibilities: The 1912 Lawrence Textile Strike in Historical Memory](https://www.questia.com/read/1P3-2721442751/bread-roses-and-other-possibilities-the-1912-lawrence)
Vol. 40| No. 1|[Changes in the Land: Indians, Colonists, and the Ecology of New England](https://www.questia.com/read/1P3-2721442821/changes-in-the-land-indians-colonists-and-the-ecology)
Vol. 40| No. 1|[Crimson Confederates: Harvard Men Who Fought for the South](https://www.questia.com/read/1P3-2721442871/crimson-confederates-harvard-men-who-fought-for-the)
Vol. 40| No. 1|[Dynamite, Whiskey and Wood: Connecticut River Log Drives 1870 - 1915](https://www.questia.com/read/1P3-2721442931/dynamite-whiskey-and-wood-connecticut-river-log)
Vol. 40| No. 1|[First Family: Abigail & John Adams](https://www.questia.com/read/1P3-2721442841/first-family-abigail-john-adams)
Vol. 40| No. 1|[Firsting and Lasting: Writing Indians out of Existence in New England](https://www.questia.com/read/1P3-2721442811/firsting-and-lasting-writing-indians-out-of-existence)
Vol. 40| No. 1|[Foreign Affairs and the Ratification of the U.S. Constitution in Massachusetts](https://www.questia.com/read/1P3-2721442771/foreign-affairs-and-the-ratification-of-the-u-s-constitution)
Vol. 40| No. 1|[Huguenot Identity and Protestant Unity in Colonial Massachusetts: The Reverend Andr&#233; le Mercier and the "Sociable Spirit"](https://www.questia.com/read/1P3-2721442761/huguenot-identity-and-protestant-unity-in-colonial)
Vol. 40| No. 1|[Mass Moments: Teaching Resources from Mass Humanities](https://www.questia.com/read/1P3-2721442801/mass-moments-teaching-resources-from-mass-humanities)
Vol. 40| No. 1|[Multicultural Social Studies: Using Local History in the Classroom](https://www.questia.com/read/1P3-2721442901/multicultural-social-studies-using-local-history)
Vol. 40| No. 1|["Our Life's Work": Rhetorical Preparation and Teacher Training at the Westfield State Normal School, 1844-1932](https://www.questia.com/read/1P3-2721442731/our-life-s-work-rhetorical-preparation-and-teacher)
Vol. 40| No. 1|[Teaching the Salem Witch Trials through Place and Time](https://www.questia.com/read/1P3-2721442791/teaching-the-salem-witch-trials-through-place-and)
Vol. 40| No. 1|[The Assist: Hoops, Hope, and the Game of Their Lives](https://www.questia.com/read/1P3-2721442921/the-assist-hoops-hope-and-the-game-of-their-lives)
Vol. 40| No. 1|[The Captain's Widow of Sandwich: Self-Invention and the Life of Hannah Rebecca Burgess, 1834-1917](https://www.questia.com/read/1P3-2721442861/the-captain-s-widow-of-sandwich-self-invention-and)
Vol. 40| No. 1|[The Connecticut River: A Photographic Journey through the Heart of New England](https://www.questia.com/read/1P3-2721442891/the-connecticut-river-a-photographic-journey-through)
Vol. 40| No. 1|[The Historical Journal of Massachusetts and Westfield State University: A Brief History](https://www.questia.com/read/1P3-2721442721/the-historical-journal-of-massachusetts-and-westfield)
Vol. 40| No. 1|[The Specter of Salem: Remembering the Witch Trials in Nineteenth-Century America](https://www.questia.com/read/1P3-2721442831/the-specter-of-salem-remembering-the-witch-trials)
Vol. 40| No. 1|["Those about Him Remained Silent": The Battle over W.E.B. Du Bois](https://www.questia.com/read/1P3-2721442881/those-about-him-remained-silent-the-battle-over)
Vol. 40| No. 1|[Uncovering the Stories of Black Families in Springfield and Hampden County, Massachusetts: 1650-1865](https://www.questia.com/read/1P3-2721442741/uncovering-the-stories-of-black-families-in-springfield)
Vol. 39| No. 1|[Adams Family Correspondence (Volume 9)](https://www.questia.com/read/1P3-2384275091/adams-family-correspondence-volume-9)
Vol. 39| No. 1|[Amherst A to Z](https://www.questia.com/read/1P3-2384275261/amherst-a-to-z)
Vol. 39| No. 1|[A Shoemaker's Story: Being Chiefly about French Canadian Immigrants, Enterprising Photographers, Rascal Yankees, and Chinese Cobblers in a Nineteenth-Century Factory Town](https://www.questia.com/read/1P3-2384275181/a-shoemaker-s-story-being-chiefly-about-french-canadian)
Vol. 39| No. 1|[A Tale of Two Portraits: Motivations Behind Self-Fashioning in Seventeenth-Century Boston Portraiture](https://www.questia.com/read/1P3-2384274951/a-tale-of-two-portraits-motivations-behind-self-fashioning)
Vol. 39| No. 1|[Barney Frank: The Story of America's Only Left-Handed, Gay, Jewish Congressman](https://www.questia.com/read/1P3-2384275221/barney-frank-the-story-of-america-s-only-left-handed)
Vol. 39| No. 1|[Cranberry Pickers Mural at the Wareham Post Office](https://www.questia.com/read/1P3-2384274931/cranberry-pickers-mural-at-the-wareham-post-office)
Vol. 39| No. 1|[David Ruggles: A Radical Black Abolitionist and the Underground Railroad in New York City](https://www.questia.com/read/1P3-2384275121/david-ruggles-a-radical-black-abolitionist-and-the)
Vol. 39| No. 1|[Exhuming Hidden History: Sources for Teaching about Slavery in New England](https://www.questia.com/read/1P3-2384275021/exhuming-hidden-history-sources-for-teaching-about)
Vol. 39| No. 1|[First Fruits of Freedom: The Migration of Former Slaves and Their Search for Equality in Worcester, Massachusetts, 1862-1900](https://www.questia.com/read/1P3-2384275131/first-fruits-of-freedom-the-migration-of-former-slaves)
Vol. 39| No. 1|[Founders: The People Who Brought You a Nation](https://www.questia.com/read/1P3-2384275081/founders-the-people-who-brought-you-a-nation)
Vol. 39| No. 1|[From Slate to Marble: Gravestone Carving Traditions in Eastern Massachusetts 1770-1870](https://www.questia.com/read/1P3-2384275151/from-slate-to-marble-gravestone-carving-traditions)
Vol. 39| No. 1|[Fur, Fortune, and Empire: The Epic History of the Fur Trade in America](https://www.questia.com/read/1P3-2384275071/fur-fortune-and-empire-the-epic-history-of-the)
Vol. 39| No. 1|[Inventing the Charles River](https://www.questia.com/read/1P3-2384275241/inventing-the-charles-river)
Vol. 39| No. 1|[Jonathan Edwards's "Sinners in the Hands of an Angry God": A Casebook](https://www.questia.com/read/1P3-2384275041/jonathan-edwards-s-sinners-in-the-hands-of-an-angry)
Vol. 39| No. 1|[Landscape of Industry: An Industrial History of Blackstone Valley](https://www.questia.com/read/1P3-2384275171/landscape-of-industry-an-industrial-history-of-blackstone)
Vol. 39| No. 1|["Let the People Remember!": Rhode Island's Dorr Rebellion and Bay State Politics, 1842-1843](https://www.questia.com/read/1P3-2384275011/let-the-people-remember-rhode-island-s-dorr-rebellion)
Vol. 39| No. 1|[Louis D. Brandeis: A Life](https://www.questia.com/read/1P3-2384275201/louis-d-brandeis-a-life)
Vol. 39| No. 1|[Luther Gulick: His Contributions to Springfield College, the YMCA, and "Muscular Christianity"](https://www.questia.com/read/1P3-2384274981/luther-gulick-his-contributions-to-springfield-college)
Vol. 39| No. 1|[Making Freedom: The Extraordinary Life of Venture Smith](https://www.questia.com/read/1P3-2384275101/making-freedom-the-extraordinary-life-of-venture)
Vol. 39| No. 1|[Making Haste from Babylon: The Mayflower Pilgrims and Their World, a New History](https://www.questia.com/read/1P3-2384275051/making-haste-from-babylon-the-mayflower-pilgrims)
Vol. 39| No. 1|[Native People of Southern New England, 1650-1775](https://www.questia.com/read/1P3-2384275031/native-people-of-southern-new-england-1650-1775)
Vol. 39| No. 1|[Remaking Boston: An Environmental History of the City and Its Surroundings](https://www.questia.com/read/1P3-2384275231/remaking-boston-an-environmental-history-of-the-city)
Vol. 39| No. 1|[Revisiting Pocumtuck History in Deerfield: George Sheldon's Vanishing Indian Act](https://www.questia.com/read/1P3-2384274961/revisiting-pocumtuck-history-in-deerfield-george)
Vol. 39| No. 1|[Senator Edward Kennedy and the "Ulster Troubles": Irish and Irish-American Politics, 1965-2009](https://www.questia.com/read/1P3-2384275001/senator-edward-kennedy-and-the-ulster-troubles)
Vol. 39| No. 1|[Senda Berenson: The Unlikely Founder of Women's Basketball](https://www.questia.com/read/1P3-2384275191/senda-berenson-the-unlikely-founder-of-women-s-basketball)
Vol. 39| No. 1|[Slavery in the Connecticut Valley of Massachusetts](https://www.questia.com/read/1P3-2384275111/slavery-in-the-connecticut-valley-of-massachusetts)
Vol. 39| No. 1|[Thanksgiving: The Biography of an American Holiday](https://www.questia.com/read/1P3-2384275141/thanksgiving-the-biography-of-an-american-holiday)
Vol. 39| No. 1|[The Gardiners of Massachusetts: Provincial Ambition and the British-American Career](https://www.questia.com/read/1P3-2384275061/the-gardiners-of-massachusetts-provincial-ambition)
Vol. 39| No. 1|[The Maya of New Bedford: Genesis and Evolution of a Community, 1980 - 2010](https://www.questia.com/read/1P3-2384274991/the-maya-of-new-bedford-genesis-and-evolution-of)
Vol. 39| No. 1|[The Puritan Origins of Black Abolitionism in Massachusetts](https://www.questia.com/read/1P3-2384274971/the-puritan-origins-of-black-abolitionism-in-massachusetts)
Vol. 39| No. 1|[Thoreau's Democratic Withdrawal: Alienation, Participation, and Modernity](https://www.questia.com/read/1P3-2384275161/thoreau-s-democratic-withdrawal-alienation-participation)
Vol. 39| No. 1|[Walking Tours of Boston's Made Land](https://www.questia.com/read/1P3-2384275251/walking-tours-of-boston-s-made-land)
Vol. 39| No. 1|[Writers, Plumbers, and Anarchists: The WPA Writers' Project in Massachusetts](https://www.questia.com/read/1P3-2384275211/writers-plumbers-and-anarchists-the-wpa-writers)
Vol. 38| No. 2|[A Common Thread: Labor, Politics, and Capital Mobility in the Textile Industry](https://www.questia.com/read/1P3-2190595861/a-common-thread-labor-politics-and-capital-mobility)
Vol. 38| No. 2|[Announcements](https://www.questia.com/read/1P3-2190595741/announcements)
Vol. 38| No. 2|["Certainly a Man May Quibble for His Life": Public Execution and Capital Punishment in Massachusetts](https://www.questia.com/read/1P3-2190595761/certainly-a-man-may-quibble-for-his-life-public)
Vol. 38| No. 2|[In the Details: Style in New England Architecture](https://www.questia.com/read/1P3-2191095191/in-the-details-style-in-new-england-architecture)
Vol. 38| No. 2|[John Denison Hartshorn: A Colonial Apprentice in "Physick" and Surgery (Boston)](https://www.questia.com/read/1P3-2190595751/john-denison-hartshorn-a-colonial-apprentice-in-physick)
Vol. 38| No. 2|[Leviathan: The History of Whaling in America](https://www.questia.com/read/1P3-2190595791/leviathan-the-history-of-whaling-in-america)
Vol. 38| No. 2|[Linked Labor Histories: New England, Colombia, and the Making of a Global Working Class](https://www.questia.com/read/1P3-2190595871/linked-labor-histories-new-england-colombia-and)
Vol. 38| No. 2|[Making Heretics: Militant Protestantism and Free Grace in Massachusetts, 1636-1641](https://www.questia.com/read/1P3-2190595781/making-heretics-militant-protestantism-and-free-grace)
Vol. 38| No. 2|["Militant Mothers": Boston, Busing, and the Bicentennial of 1976](https://www.questia.com/read/1P3-2191095221/militant-mothers-boston-busing-and-the-bicentennial)
Vol. 38| No. 2|[Money, Morals, and Politics: Massachusetts in the Age of the Boston Associates](https://www.questia.com/read/1P3-2190595841/money-morals-and-politics-massachusetts-in-the)
Vol. 38| No. 2|["Murder by Counseling": The 1816 Case of George Bowen (Northampton)](https://www.questia.com/read/1P3-2191095181/murder-by-counseling-the-1816-case-of-george-bowen)
Vol. 38| No. 2|[Out of the Attic: Inventing Antiques in Twentieth-Century New England](https://www.questia.com/read/1P3-2190595851/out-of-the-attic-inventing-antiques-in-twentieth-century)
Vol. 38| No. 2|[Quincy's Market: A Boston Landmark](https://www.questia.com/read/1P3-2190595821/quincy-s-market-a-boston-landmark)
Vol. 38| No. 2|[Sacco and Vanzetti (DVD)](https://www.questia.com/read/1P3-2190595931/sacco-and-vanzetti-dvd)
Vol. 38| No. 2|[Sacco and Vanzetti: Rebel Lives](https://www.questia.com/read/1P3-2190595911/sacco-and-vanzetti-rebel-lives)
Vol. 38| No. 2|[Schooling Citizens: The Struggle for African American Education in Antebellum America](https://www.questia.com/read/1P3-2190595811/schooling-citizens-the-struggle-for-african-american)
Vol. 38| No. 2|[Slavery in the Age of Reason - Archaeology at a New England Farm](https://www.questia.com/read/1P3-2190595801/slavery-in-the-age-of-reason-archaeology-at-a-new)
Vol. 38| No. 2|[Small Strangers: The Experiences of Immigrant Children in America, 1880-1925](https://www.questia.com/read/1P3-2190595881/small-strangers-the-experiences-of-immigrant-children)
Vol. 38| No. 2|[Springfield (Postcard History Series)](https://www.questia.com/read/1P3-2190595961/springfield-postcard-history-series)
Vol. 38| No. 2|[The Great Silent Army of Abolitionism: Ordinary Women in the Antislavery Movement](https://www.questia.com/read/1P3-2190595831/the-great-silent-army-of-abolitionism-ordinary-women)
Vol. 38| No. 2|[The Lizzie Borden Murder Trial: Womanhood as Asset and Liability (Fall River, 1892)](https://www.questia.com/read/1P3-2191095211/the-lizzie-borden-murder-trial-womanhood-as-asset)
Vol. 38| No. 2|[The Puerto Rican Diaspora: Historical Perspectives](https://www.questia.com/read/1P3-2190595941/the-puerto-rican-diaspora-historical-perspectives)
Vol. 38| No. 2|[The Woman Behind the New Deal: The Life of Frances Perkins, FDR'S Secretary of Labor and His Moral Conscience](https://www.questia.com/read/1P3-2190595891/the-woman-behind-the-new-deal-the-life-of-frances)
Vol. 38| No. 2|[Through an Uncommon Lens: The Life and Photography of F. Holland Day](https://www.questia.com/read/1P3-2190595901/through-an-uncommon-lens-the-life-and-photography)
Vol. 38| No. 2|[Twentieth-Century New England Land Conservation: A Heritage of Civic Engagement](https://www.questia.com/read/1P3-2190595921/twentieth-century-new-england-land-conservation-a)
Vol. 38| No. 2|[Using Historic Newspaper Databases in the Classroom: From Primary Sources to Research Assignments](https://www.questia.com/read/1P3-2190595771/using-historic-newspaper-databases-in-the-classroom)
Vol. 38| No. 2|[When a Heart Turns Rock Solid: The Lives of Three Puerto Rican Brothers on and off the Streets](https://www.questia.com/read/1P3-2190595951/when-a-heart-turns-rock-solid-the-lives-of-three)
Vol. 38| No. 1|["A Million Things to Get Done": The Skinner Family Servants of Holyoke](https://www.questia.com/read/1P3-2036414221/a-million-things-to-get-done-the-skinner-family)
Vol. 38| No. 1|[Asian Americans in New England: Culture and Community](https://www.questia.com/read/1P3-2036414341/asian-americans-in-new-england-culture-and-community)
Vol. 38| No. 1|[Beyond Vietnam: The Politics of Protest in Massachusetts, 1974-1990](https://www.questia.com/read/1P3-2036414311/beyond-vietnam-the-politics-of-protest-in-massachusetts)
Vol. 38| No. 1|[Boston Women's Heritage Trail: Seven Self-Guided Walks through Four Centuries of Boston's Women's History](https://www.questia.com/read/1P3-2036414331/boston-women-s-heritage-trail-seven-self-guided-walks)
Vol. 38| No. 1|[Defending the "New England Way": Cotton Mather's "Exact Mapp of New England and New York"](https://www.questia.com/read/1P3-2036414261/defending-the-new-england-way-cotton-mather-s-exact)
Vol. 38| No. 1|[Emerson](https://www.questia.com/read/1P3-2036414361/emerson)
Vol. 38| No. 1|[Esther Forbes' Rainbow on the Road: Portrait of the Nineteenth-Century Provincial Artist](https://www.questia.com/read/1P3-2036414251/esther-forbes-rainbow-on-the-road-portrait-of-the)
Vol. 38| No. 1|[Fresh Pond: The History of a Cambridge Landscape](https://www.questia.com/read/1P3-2036414401/fresh-pond-the-history-of-a-cambridge-landscape)
Vol. 38| No. 1|[Introduction to African American Photographs: Identification, Research, Care & Collecting, 1840-1950](https://www.questia.com/read/1P3-2036414391/introduction-to-african-american-photographs-identification)
Vol. 38| No. 1|[King Caesar of Duxbury - Exploring the World of Ezra Weston, Shipbuilder and Merchant](https://www.questia.com/read/1P3-2036414351/king-caesar-of-duxbury-exploring-the-world-of-ezra)
Vol. 38| No. 1|[Massachusetts Quilts: Our Common Wealth](https://www.questia.com/read/1P3-2036414321/massachusetts-quilts-our-common-wealth)
Vol. 38| No. 1|[Mass Moments: Teaching Resources from Mass Humanities](https://www.questia.com/read/1P3-2036414281/mass-moments-teaching-resources-from-mass-humanities)
Vol. 38| No. 1|[New Museums and Historical Sites](https://www.questia.com/read/1P3-2036414291/new-museums-and-historical-sites)
Vol. 38| No. 1|[Prophets of Protest: Reconsidering the History of American Abolitionism](https://www.questia.com/read/1P3-2036414371/prophets-of-protest-reconsidering-the-history-of)
Vol. 38| No. 1|["Red Riots" and the Origins of the Civil Liberties Union of Massachusetts, 1915-1930](https://www.questia.com/read/1P3-2036414241/red-riots-and-the-origins-of-the-civil-liberties)
Vol. 38| No. 1|[Teaching Massachusetts History: Online Primary Sources and Curriculum](https://www.questia.com/read/1P3-2036414271/teaching-massachusetts-history-online-primary-sources)
Vol. 38| No. 1|[The Fragile Fabric of Union: Cotton, Federal Politics, and the Global Origins of the Civil War](https://www.questia.com/read/1P3-2036414381/the-fragile-fabric-of-union-cotton-federal-politics)
Vol. 38| No. 1|[The Great Gypsy Moth War: The History of the First Campaign in Massachusetts to Eradicate the Gypsy Moth, 1890-1901](https://www.questia.com/read/1P3-2036414301/the-great-gypsy-moth-war-the-history-of-the-first)
Vol. 38| No. 1|[The Mill River Flood of 1874: From Williamsburg to Northampton](https://www.questia.com/read/1P3-2036414211/the-mill-river-flood-of-1874-from-williamsburg-to)
Vol. 38| No. 1|[Young Patrick A. Collins and Boston Politics after the Civil War](https://www.questia.com/read/1P3-2036414231/young-patrick-a-collins-and-boston-politics-after)
Vol. 37| No. 2|[African American Heritage Trails: From Boston to the Berkshires](https://www.questia.com/read/1P3-1893226171/african-american-heritage-trails-from-boston-to-the)
Vol. 37| No. 2|[Beyond the Farm: National Ambitions in Rural New England](https://www.questia.com/read/1P3-1893226301/beyond-the-farm-national-ambitions-in-rural-new-england)
Vol. 37| No. 2|[Boston's Back Bay: The Story of America's Greatest Nineteenth-Century Landfill Project](https://www.questia.com/read/1P3-1893226321/boston-s-back-bay-the-story-of-america-s-greatest)
Vol. 37| No. 2|[Captive Histories: English, French, and Native Narratives of the 1704 Deerfield Raid](https://www.questia.com/read/1P3-1893226271/captive-histories-english-french-and-native-narratives)
Vol. 37| No. 2|[Changing Rapture: Emily Dickinson's Poetic Development](https://www.questia.com/read/1P3-1893226291/changing-rapture-emily-dickinson-s-poetic-development)
Vol. 37| No. 2|[Courting Equality: A Documentary History of America's First Legal Same-Sex Marriages](https://www.questia.com/read/1P3-1893226341/courting-equality-a-documentary-history-of-america-s)
Vol. 37| No. 2|[From the Puritans to the Projects: Public Housing and Public Neighbors](https://www.questia.com/read/1P3-1893226331/from-the-puritans-to-the-projects-public-housing)
Vol. 37| No. 2|[Mission Statement](https://www.questia.com/read/1P3-1893226151/mission-statement)
Vol. 37| No. 2|[Mr. and Mrs. Prince: An African American Courtship and Marriage in Colonial Deerfield](https://www.questia.com/read/1P3-1893226161/mr-and-mrs-prince-an-african-american-courtship)
Vol. 37| No. 2|[New England Silver and Silversmithing, 1620-1815](https://www.questia.com/read/1P3-1893226311/new-england-silver-and-silversmithing-1620-1815)
Vol. 37| No. 2|[Politics, Honor, and Self-Defense in Post-Revolutionary Boston: The 1806 Manslaughter Trial of Thomas Selfridge](https://www.questia.com/read/1P3-1893226211/politics-honor-and-self-defense-in-post-revolutionary)
Vol. 37| No. 2|[Preserving African American History](https://www.questia.com/read/1P3-1893226181/preserving-african-american-history)
Vol. 37| No. 2|["Something Will Drop": Socialists, Unions, and Trusts in Nineteenth-Century Holyoke](https://www.questia.com/read/1P3-1893226191/something-will-drop-socialists-unions-and-trusts)
Vol. 37| No. 2|[Student Handout #1: Bertha Johnston (1864-1953)](https://www.questia.com/read/1P3-1893226241/student-handout-1-bertha-johnston-1864-1953)
Vol. 37| No. 2|[Student Handout #2: Jennie E. Howard (1841-1931)](https://www.questia.com/read/1P3-1893226251/student-handout-2-jennie-e-howard-1841-1931)
Vol. 37| No. 2|[Teaching Resources: Teaching the History of Education Using College and Local Archives](https://www.questia.com/read/1P3-1893226231/teaching-resources-teaching-the-history-of-education)
Vol. 37| No. 2|[The Allen Sisters: "Foremost Women Photographers in America"](https://www.questia.com/read/1P3-1893226131/the-allen-sisters-foremost-women-photographers-in)
Vol. 37| No. 2|[Tribe, Race, History: Native Americans in Southern New England, 1780-1880](https://www.questia.com/read/1P3-1893226261/tribe-race-history-native-americans-in-southern)
Vol. 37| No. 2|["Until Death Do Us Part": Wills, Widows, Women, and Dower in Oxford County, Massachusetts, 1805-1818](https://www.questia.com/read/1P3-1893226201/until-death-do-us-part-wills-widows-women-and)
Vol. 37| No. 2|["Weltering in Their Own Blood": Puritan Casualties in King Philip's War](https://www.questia.com/read/1P3-1893226221/weltering-in-their-own-blood-puritan-casualties)
Vol. 37| No. 2|[Woman's Voice, Woman's Place: Lucy Stone and the Birth of the Woman's Rights Movement](https://www.questia.com/read/1P3-1893226281/woman-s-voice-woman-s-place-lucy-stone-and-the-birth)
Vol. 37| No. 1|[Black and Irish Relations in Nineteenth Century Boston: The Interesting Case of Lawyer Robert Morris](https://www.questia.com/read/1P3-1678503581/black-and-irish-relations-in-nineteenth-century-boston)
Vol. 37| No. 1|[Brook Farm: The Dark Side of Utopia](https://www.questia.com/read/1P3-1678503731/brook-farm-the-dark-side-of-utopia)
Vol. 37| No. 1|[Building to a Revolution: The Powder Alarm and Popular Mobilization of the New England Countryside, 1774-1775](https://www.questia.com/read/1P3-1678503621/building-to-a-revolution-the-powder-alarm-and-popular)
Vol. 37| No. 1|[Defining Women's Enterprise: Mount Holyoke Faculty and the Rise of American Science](https://www.questia.com/read/1P3-1678503641/defining-women-s-enterprise-mount-holyoke-faculty)
Vol. 37| No. 1|[Don't Smile for the Camera: Expression in Early Photography](https://www.questia.com/read/1P3-1678503571/don-t-smile-for-the-camera-expression-in-early-photography)
Vol. 37| No. 1|[From Dependency to Independence: Economic Revolution in Colonial New England](https://www.questia.com/read/1P3-1678503701/from-dependency-to-independence-economic-revolution)
Vol. 37| No. 1|[Gentlemen and Scholars: Harvard's History Department and the Path to Professionalism, 1920-1950](https://www.questia.com/read/1P3-1678503591/gentlemen-and-scholars-harvard-s-history-department)
Vol. 37| No. 1|[Keepers of Tradition: Art and Folk Heritage in Massachusetts](https://www.questia.com/read/1P3-1678503631/keepers-of-tradition-art-and-folk-heritage-in-massachusetts)
Vol. 37| No. 1|[Massachusetts Folk Art: New Immigrants Redefine Tradition](https://www.questia.com/read/1P3-1678503521/massachusetts-folk-art-new-immigrants-redefine-tradition)
Vol. 37| No. 1|[Mrs. Elizabeth Towne: Pioneering Woman in Publishing and Politics (1865 - 1960)](https://www.questia.com/read/1P3-1678503551/mrs-elizabeth-towne-pioneering-woman-in-publishing)
Vol. 37| No. 1|[Romance, Remedies, and Revolution: The Journal of Dr. Elihu Ashley of Deerfield, Massachusetts, 1773-1775](https://www.questia.com/read/1P3-1678503721/romance-remedies-and-revolution-the-journal-of)
Vol. 37| No. 1|[Southeast Asian Refugees and Immigrants in the Mill City: Changing Families, Communities, Institutions - Thirty Years Afterward](https://www.questia.com/read/1P3-1678503681/southeast-asian-refugees-and-immigrants-in-the-mill)
Vol. 37| No. 1|["Take Me out to the Brawl Game": Sports and Workers in Gilded Age Massachusetts](https://www.questia.com/read/1P3-1678503531/take-me-out-to-the-brawl-game-sports-and-workers)
Vol. 37| No. 1|[Teaching Resources: Exploration & Colonial Genres](https://www.questia.com/read/1P3-1678503611/teaching-resources-exploration-colonial-genres)
Vol. 37| No. 1|[The Athens of America: Boston 1825-1845](https://www.questia.com/read/1P3-1678503651/the-athens-of-america-boston-1825-1845)
Vol. 37| No. 1|[The Exchange Artist: A Tale of High-Flying Speculation and America's First Banking Collapse](https://www.questia.com/read/1P3-1678503711/the-exchange-artist-a-tale-of-high-flying-speculation)
Vol. 37| No. 1|[The Lowell Experiment: Public History in a Postindustrial City](https://www.questia.com/read/1P3-1678503691/the-lowell-experiment-public-history-in-a-postindustrial)
Vol. 37| No. 1|[The Other Black Bostonians: West Indians in Boston, 1900-1950](https://www.questia.com/read/1P3-1678503661/the-other-black-bostonians-west-indians-in-boston)
Vol. 37| No. 1|[The Paddy Camps: The Irish of Lowell, 1821-1861](https://www.questia.com/read/1P3-1678503671/the-paddy-camps-the-irish-of-lowell-1821-1861)
Vol. 37| No. 1|["Won't Be Home Again": A Lynn Grocer's Letters Home from the California Gold Rush](https://www.questia.com/read/1P3-1678503601/won-t-be-home-again-a-lynn-grocer-s-letters-home)
Vol. 35| No. 2|[A Massachusetts Yankee in the Court of Charleston: Jasper Adams, College President in Antebellum South Carolina](https://www.questia.com/read/1P3-1346636321/a-massachusetts-yankee-in-the-court-of-charleston)
Vol. 35| No. 2|[Book Notes](https://www.questia.com/read/1P3-1346636331/book-notes)
Vol. 35| No. 2|[Destination: Holyoke](https://www.questia.com/read/1P3-1348497151/destination-holyoke)
Vol. 35| No. 2|[Locating "Wissatinnewag": A Second Opinion](https://www.questia.com/read/1P3-1348497131/locating-wissatinnewag-a-second-opinion)
Vol. 35| No. 2|[Political Places of Boston: From the Backrooms to the Golden Dome](https://www.questia.com/read/1P3-1348497191/political-places-of-boston-from-the-backrooms-to)
Vol. 35| No. 2|[The Complexities Found, as Well as Insights Gained, from the Identification of a Birthplace of Free Public Education: The Case of Rehoboth, Massachusetts](https://www.questia.com/read/1P3-1348497171/the-complexities-found-as-well-as-insights-gained)
Vol. 35| No. 2|[The Literary and Military Career of Benjamin Church: Change or Continuity in Early American Warfare](https://www.questia.com/read/1P3-1346636301/the-literary-and-military-career-of-benjamin-church)
Vol. 35| No. 2|["The Most Agreeable Country": New Light on Democratic-Republican Opinion of Massachusetts in the 1790s](https://www.questia.com/read/1P3-1346636311/the-most-agreeable-country-new-light-on-democratic-republican)
Vol. 35| No. 1|[All on Fire: William Lloyd Garrison and the Abolition of Slavery](https://www.questia.com/read/1P3-1386989731/all-on-fire-william-lloyd-garrison-and-the-abolition)
Vol. 35| No. 1|[Beyond the Scarlet "A": Hawthorne and the Matter of Racism](https://www.questia.com/read/1P3-1386989681/beyond-the-scarlet-a-hawthorne-and-the-matter-of)
Vol. 35| No. 1|[Book Notes](https://www.questia.com/read/1P3-1386989741/book-notes)
Vol. 35| No. 1|[Mayflower: A Story of Courage, Community, and War](https://www.questia.com/read/1P3-1386989721/mayflower-a-story-of-courage-community-and-war)
Vol. 35| No. 1|[Political Rivalry in Rhode Island: William H. Vanderbilt vs. J. Howard McGrath: The Wiretapping Case](https://www.questia.com/read/1P3-1386989701/political-rivalry-in-rhode-island-william-h-vanderbilt)
Vol. 35| No. 1|[Ralph Waldo Emerson's Mentor at Harvard: Professor Levi Frisbie, Jr](https://www.questia.com/read/1P3-1386989711/ralph-waldo-emerson-s-mentor-at-harvard-professor)
Vol. 35| No. 1|[Reform Politics in Hard Times: Battles over Labor Legislation during the Decline of Traditional Manufacturing in Massachusetts, 1922-1928](https://www.questia.com/read/1P3-1386989691/reform-politics-in-hard-times-battles-over-labor)
Vol. 34| No. 2|[An Unfinished Life: John F. Kennedy, 1917-1963](https://www.questia.com/read/1P3-1207777941/an-unfinished-life-john-f-kennedy-1917-1963)
Vol. 34| No. 2|["Charmed with the French": Reassessing the Early Career of Charles Bulfinch, Architect](https://www.questia.com/read/1P3-1207777891/charmed-with-the-french-reassessing-the-early-career)
Vol. 34| No. 2|[Fire & Roses: The Burning of the Charlestown Convent, 1834](https://www.questia.com/read/1P3-1207777961/fire-roses-the-burning-of-the-charlestown-convent)
Vol. 34| No. 2|[From Norton to Saint-Gobain, 1885-2006: Grinding Labor Down](https://www.questia.com/read/1P3-1207777881/from-norton-to-saint-gobain-1885-2006-grinding-labor)
Vol. 34| No. 2|[Greenfield Tap & Die: Economic and Historical Analysis](https://www.questia.com/read/1P3-1207777911/greenfield-tap-die-economic-and-historical-analysis)
Vol. 34| No. 2|[Interpreting the Place Space of an Extinct Cultural Landscape: The Swift River Valley of Central Massachusetts](https://www.questia.com/read/1P3-1207777931/interpreting-the-place-space-of-an-extinct-cultural)
Vol. 34| No. 1|[Amherst Professor Joseph Haven and His Influence on America's Great Social Critic, Thorstein Veblen](https://www.questia.com/read/1P3-1207609001/amherst-professor-joseph-haven-and-his-influence-on)
Vol. 34| No. 1|[Locating "Wissatinnewag" in John Pynchon's Letter of 1663](https://www.questia.com/read/1P3-1207609011/locating-wissatinnewag-in-john-pynchon-s-letter)
Vol. 34| No. 1|[Piracy, Riches, and Social Equality: The Wreck of the Whydah off Cape Cod](https://www.questia.com/read/1P3-1207608991/piracy-riches-and-social-equality-the-wreck-of)
Vol. 34| No. 1|[Puritan Family Life: The Diary of Samuel Sewall](https://www.questia.com/read/1P3-1207609021/puritan-family-life-the-diary-of-samuel-sewall)
Vol. 34| No. 1|["Tyrant and Oppressor!"1: Colonial Press Reaction to the Quebec Act](https://www.questia.com/read/1P3-1207608981/tyrant-and-oppressor-1-colonial-press-reaction)
Vol. 34| No. 1|[Voices of a People's History of the United States](https://www.questia.com/read/1P3-1207609031/voices-of-a-people-s-history-of-the-united-states)
Vol. 33| No. 2|[Founding Aswalos House: Separate and Powerful; the YWCA in Roxbury, Massachusetts, 1968-1988](https://www.questia.com/read/1P3-1219929081/founding-aswalos-house-separate-and-powerful-the)
Vol. 33| No. 2|[No Early Pardon for Traitors: Rebellion in Massachusetts in 1787](https://www.questia.com/read/1P3-1219929021/no-early-pardon-for-traitors-rebellion-in-massachusetts)
Vol. 33| No. 2|[The Aftermath of the Salem Witch Trials in Colonial America](https://www.questia.com/read/1P3-1219929101/the-aftermath-of-the-salem-witch-trials-in-colonial)
Vol. 33| No. 2|[The Civil War Draft in Palmer: Reaction in a Small Town](https://www.questia.com/read/1P3-1219928991/the-civil-war-draft-in-palmer-reaction-in-a-small)
Vol. 33| No. 2|[The Political Evolution of Northampton, Massachusetts](https://www.questia.com/read/1P3-1219929051/the-political-evolution-of-northampton-massachusetts)
Vol. 33| No. 1|[A Mirror of Boston: Faneuil Hall at the Turn of the Century](https://www.questia.com/read/1P3-839418701/a-mirror-of-boston-faneuil-hall-at-the-turn-of-the)
Vol. 33| No. 1|[Book Notes](https://www.questia.com/read/1P3-839418711/book-notes)
Vol. 33| No. 1|[Diminished Democracy: From Membership to Management in American Life](https://www.questia.com/read/1P3-839418691/diminished-democracy-from-membership-to-management)
Vol. 33| No. 1|["I Spake the Truth in the Feare of God"; the Puritan Management of Dissent during the Henry Dunster Controversy](https://www.questia.com/read/1P3-839418671/i-spake-the-truth-in-the-feare-of-god-the-puritan)
Vol. 33| No. 1|[Northampton Local Monuments: Testament to an Enduring Historical Legacy](https://www.questia.com/read/1P3-839418751/northampton-local-monuments-testament-to-an-enduring)
Vol. 33| No. 1|[The Civil War Draft in Palmer: Reaction in a Small Town](https://www.questia.com/read/1P3-839418731/the-civil-war-draft-in-palmer-reaction-in-a-small)
Vol. 33| No. 1|[The First World Series and the Baseball Fanatics of 1903](https://www.questia.com/read/1P3-839418741/the-first-world-series-and-the-baseball-fanatics-of)
Vol. 32| No. 2|[Alternative Communities in Seventeenth Century Massachusetts](https://www.questia.com/read/1P3-839431941/alternative-communities-in-seventeenth-century-massachusetts)
Vol. 32| No. 2|[Book Notes](https://www.questia.com/read/1P3-839431981/book-notes)
Vol. 32| No. 2|[Ethnic Catholicism and Craft Unionism in Worcester, Massachusetts, 1887-1920: A Mixed Story](https://www.questia.com/read/1P3-839431961/ethnic-catholicism-and-craft-unionism-in-worcester)
Vol. 32| No. 2|[Sheffield's Richard P. Wakefield: Advocate for Human Values, World Futures, and the Environment](https://www.questia.com/read/1P3-839431931/sheffield-s-richard-p-wakefield-advocate-for-human)
Vol. 32| No. 2|[The Beginning of the Past: Boston and the Early Historic Preservation Movement, 1863-1918](https://www.questia.com/read/1P3-839431921/the-beginning-of-the-past-boston-and-the-early-historic)
Vol. 32| No. 2|[What Did They Call It When They Died? A Study of the Listed Causes of Death in the Town of Hyde Park, Massachusetts, 1869-1900](https://www.questia.com/read/1P3-839431971/what-did-they-call-it-when-they-died-a-study-of-the)
Vol. 32| No. 1|[Charter Changes in Boston from 1885 - 1949](https://www.questia.com/read/1P3-569754641/charter-changes-in-boston-from-1885-1949)
Vol. 32| No. 1|[Class and the Ideology of Womanhood: The Early Years of the Boston Young Women's Christian Association](https://www.questia.com/read/1P3-569754821/class-and-the-ideology-of-womanhood-the-early-years)
Vol. 32| No. 1|[Fitz Hugh Lane and the Legacy of the Codfish Aristocracy](https://www.questia.com/read/1P3-569754711/fitz-hugh-lane-and-the-legacy-of-the-codfish-aristocracy)
Vol. 32| No. 1|[Oxenbridge Thacher: Boston Lawyer, Early Patriot](https://www.questia.com/read/1P3-569754661/oxenbridge-thacher-boston-lawyer-early-patriot)
Vol. 32| No. 1|[Purchasing Identity in the Atlantic World: Massachusetts Merchants](https://www.questia.com/read/1P3-569754681/purchasing-identity-in-the-atlantic-world-massachusetts)
Vol. 32| No. 1|[Running against the Wind: The Struggle of Women in Massachusetts Politics](https://www.questia.com/read/1P3-569754611/running-against-the-wind-the-struggle-of-women-in)
Vol. 32| No. 1|[The Mohawk Trail: Route 2 from Boston, Massachusetts to Troy, New York](https://www.questia.com/read/1P3-569754391/the-mohawk-trail-route-2-from-boston-massachusetts)
Vol. 31| No. 2|[Almshouse, Workhouse, Outdoor Relief: Responses to the Poor in Southeastern Massachusetts, 1740-1800](https://www.questia.com/read/1P3-385846011/almshouse-workhouse-outdoor-relief-responses-to)
Vol. 31| No. 2|[My Dear Mrs. Ames: A Study of Suffragist Cartoonist Blanche Ames Ames](https://www.questia.com/read/1P3-385846041/my-dear-mrs-ames-a-study-of-suffragist-cartoonist)
Vol. 31| No. 2|[The Altar of Liberty: Enlightened Dissent and the Dudleian Lectures, 1755-1765](https://www.questia.com/read/1P3-385846051/the-altar-of-liberty-enlightened-dissent-and-the)
Vol. 31| No. 2|["The Artificial Advantage Money Gives" A Brahmin Reformer's Use of Class Privilege](https://www.questia.com/read/1P3-385846031/the-artificial-advantage-money-gives-a-brahmin-reformer-s)
Vol. 31| No. 2|[The Hub: Boston Past and Present](https://www.questia.com/read/1P3-385845991/the-hub-boston-past-and-present)
Vol. 31| No. 2|[Thomas Hutchinson & the Origins of the American Revolution](https://www.questia.com/read/1P3-385845981/thomas-hutchinson-the-origins-of-the-american-revolution)
Vol. 31| No. 2|["We Are to Be Reduced to the Level of Slaves": Planters, Taxes, Aristocrats, and Massachusetts Antifederalists, 1787-1788](https://www.questia.com/read/1P3-385846001/we-are-to-be-reduced-to-the-level-of-slaves-planters)
Vol. 31| No. 1|[David Rozman and Land-Use Planning in Massachusetts](https://www.questia.com/read/1P3-305033141/david-rozman-and-land-use-planning-in-massachusetts)
Vol. 31| No. 1|[Grass-Roots Garrisonians in Central Massachusetts: The Case of Hubbardston's Jonas and Susan Clark](https://www.questia.com/read/1P3-305036531/grass-roots-garrisonians-in-central-massachusetts)
Vol. 31| No. 1|[How 'Poor Country Boys' Became Boston Brahmins: The Rise of the Appletons and the Lawrences in Ante-Bellum Massachusetts](https://www.questia.com/read/1P3-305036461/how-poor-country-boys-became-boston-brahmins-the)
Vol. 31| No. 1|[Indians in the United States and Canada: A Comparative History](https://www.questia.com/read/1P3-305036631/indians-in-the-united-states-and-canada-a-comparative)
Vol. 31| No. 1|[Mysteries of the Tyringham Shakers Unmasked: A New Examination of People, Facts, and Figures](https://www.questia.com/read/1P3-305033131/mysteries-of-the-tyringham-shakers-unmasked-a-new)
Vol. 31| No. 1|[The Allen Sisters: Pictorial Photographers 1885-1920](https://www.questia.com/read/1P3-305036601/the-allen-sisters-pictorial-photographers-1885-1920)
Vol. 31| No. 1|[The Roots of Connecticut River Valley Deindustrialization: The Springfield American Bosch Plant 1940-1975](https://www.questia.com/read/1P3-305036561/the-roots-of-connecticut-river-valley-deindustrialization)
Vol. 30| No. 2|[Jefferson and the Indians: The Tragic Fate of the First Americans](https://www.questia.com/read/1P3-154470041/jefferson-and-the-indians-the-tragic-fate-of-the)
Vol. 30| No. 2|[John Adams](https://www.questia.com/read/1P3-154470111/john-adams)
Vol. 30| No. 2|[Legislators of the Masschusetts General Court, 1691-1780: A Biographical Dictionary](https://www.questia.com/read/1P3-154470101/legislators-of-the-masschusetts-general-court-1691-1780)
Vol. 30| No. 2|["So I Must Be Contented to Live a Widow." the Revolutionary War Service of Sarah Hodgkins of Ipswich (1775-1779)](https://www.questia.com/read/1P3-154469941/so-i-must-be-contented-to-live-a-widow-the-revolutionary)
Vol. 30| No. 2|["The Devil and Father Ralle": The Narration of Father Rale's War in Provinicial Massachusetts](https://www.questia.com/read/1P3-154469831/the-devil-and-father-ralle-the-narration-of-father)
Vol. 30| No. 2|[The Groton Indian Raid of 1964 and Lydia Longley](https://www.questia.com/read/1P3-154469781/the-groton-indian-raid-of-1964-and-lydia-longley)
Vol. 30| No. 2|[The Publisher of the Foreign-Language Press as an Ethnic Leader? the Case of James V. Donnaruma and Bostons's Italian-American Community in the Interwar Years](https://www.questia.com/read/1P3-154469741/the-publisher-of-the-foreign-language-press-as-an)
Vol. 30| No. 2|[The Role of the 54th Massachusetts Regimen on Potter's Raid](https://www.questia.com/read/1P3-154469871/the-role-of-the-54th-massachusetts-regimen-on-potter-s)
Vol. 30| No. 2|[Writing New England: An Anthology from the Puritans to the Present](https://www.questia.com/read/1P3-154469991/writing-new-england-an-anthology-from-the-puritans)
Vol. 30| No. 1|[Addendum](https://www.questia.com/read/1P3-109959512/addendum)
Vol. 30| No. 1|[A Pox on Amherst: Smallpox, Sir Jeffery, and a Town Named Amherst](https://www.questia.com/read/1P3-109959500/a-pox-on-amherst-smallpox-sir-jeffery-and-a-town)
Vol. 30| No. 1|[Boston Riots: Three Centuries of Boston Riots](https://www.questia.com/read/1P3-109959515/boston-riots-three-centuries-of-boston-riots)
Vol. 30| No. 1|[Brahmins under Fire: Peer Courage and the Harvard Regiment](https://www.questia.com/read/1P3-109959508/brahmins-under-fire-peer-courage-and-the-harvard)
Vol. 30| No. 1|[In the Heart of the Sea: The Tragedy of the Whaleship](https://www.questia.com/read/1P3-109959657/in-the-heart-of-the-sea-the-tragedy-of-the-whaleship)
Vol. 30| No. 1|["I Wish for Nothing More Ardently upon Earth, Than to See My Friends and Country Again": The Return of Massachusetts Loyalists](https://www.questia.com/read/1P3-109959506/i-wish-for-nothing-more-ardently-upon-earth-than)
Vol. 30| No. 1|[Knights Unhorsed: Internal Conflicts in Gilded Age Social Movement](https://www.questia.com/read/1P3-109959649/knights-unhorsed-internal-conflicts-in-gilded-age)
Vol. 30| No. 1|[The First Hurrah: James Michael Curley versus the "Goo-Goos" in the Boston Mayoralty Election of 1914](https://www.questia.com/read/1P3-109959507/the-first-hurrah-james-michael-curley-versus-the)
Vol. 30| No. 1|[Women and the City: Gender, Space, and Power in Boston, 1870-1940](https://www.questia.com/read/1P3-109959558/women-and-the-city-gender-space-and-power-in-boston)
Vol. 29| No. 2|[" A Brave Man's Child": Theodore Parker and the Memory of the American Revolution](https://www.questia.com/read/1P3-78009468/a-brave-man-s-child-theodore-parker-and-the-memory)
Vol. 29| No. 2|[From African to Yankee: Narratives of Slavery and Freedom in Antebellum New England](https://www.questia.com/read/1P3-78011201/from-african-to-yankee-narratives-of-slavery-and)
Vol. 29| No. 2|[Indian Land in Seventeenth Century Massachusetts](https://www.questia.com/read/1P3-78009460/indian-land-in-seventeenth-century-massachusetts)
Vol. 29| No. 2|[Mapping Boston](https://www.questia.com/read/1P3-78011241/mapping-boston)
Vol. 29| No. 2|[Mapping the Metaphysical Landscape off Cape Ann: The Receptions of Ralph Waldo Emerson's Transcendentalism among the Gloucester Audience of Reverend Amory Dwight Mayo and Fitz Hugh Lane](https://www.questia.com/read/1P3-78009465/mapping-the-metaphysical-landscape-off-cape-ann-the)
Vol. 29| No. 2|[Migration and the Origins of the English Atlantic World](https://www.questia.com/read/1P3-78010491/migration-and-the-origins-of-the-english-atlantic)
Vol. 29| No. 2|[New England and Early Conservationism: The North American Review 1830-1860](https://www.questia.com/read/1P3-78009996/new-england-and-early-conservationism-the-north-american)
Vol. 29| No. 2|[Where Death and Glory Meet: Colonel Robert Gould Shaw and the 54th Massachusetts Infantry](https://www.questia.com/read/1P3-78011001/where-death-and-glory-meet-colonel-robert-gould-shaw)
Vol. 29| No. 1|[A People's Army: Massachusetts Soldiers and Society in the Seven-Years' War](https://www.questia.com/read/1P3-224290061/a-people-s-army-massachusetts-soldiers-and-society)
Vol. 29| No. 1|[Bunker Hill Refought: Memory Wars and Partisan Conflicts, 1775-1825](https://www.questia.com/read/1P3-224288011/bunker-hill-refought-memory-wars-and-partisan-conflicts)
Vol. 29| No. 1|[Conflict in the Church and the City: The Problem of Catholic Parish Government in Boston, 1790-1865](https://www.questia.com/read/1P3-224288061/conflict-in-the-church-and-the-city-the-problem-of)
Vol. 29| No. 1|[John Laurens and the American Revolution](https://www.questia.com/read/1P3-224290071/john-laurens-and-the-american-revolution)
Vol. 29| No. 1|[Lords of Capital and Knights of Labor: Worcester's Labor Hisotry during the Gilded Age](https://www.questia.com/read/1P3-224288071/lords-of-capital-and-knights-of-labor-worcester-s)
Vol. 29| No. 1|[Monster of Monsters and the Emergence of Political Satire in New England](https://www.questia.com/read/1P3-224287991/monster-of-monsters-and-the-emergence-of-political)
Vol. 29| No. 1|[The Coming of Industrial Order: Town and Factory Life in Rural Massachusetts, 1810-1860](https://www.questia.com/read/1P3-224290091/the-coming-of-industrial-order-town-and-factory-life)
Vol. 29| No. 1|[The Crisis of the Standing Order: Clerical Intellectuals and Cultural Authority in Massachusetts, 1780-1833](https://www.questia.com/read/1P3-224290081/the-crisis-of-the-standing-order-clerical-intellectuals)
Vol. 29| No. 1|[The Magic of the Many: Josiah Quincy and the Rise of Mass Politics in Boston, 1800-1830](https://www.questia.com/read/1P3-224290051/the-magic-of-the-many-josiah-quincy-and-the-rise)
Vol. 28| No. 2|["A Good Poor Man's Wife": Being a Chronicle of Harriet Hanson Robinson & Her Family in 19th-Century New England](https://www.questia.com/read/1P3-56875410/a-good-poor-man-s-wife-being-a-chronicle-of-harriet)
Vol. 28| No. 2|[A 'Great National Calamity': Sir William Pepperell and Isaac Royall, Reluctant Loyalists](https://www.questia.com/read/1P3-56874750/a-great-national-calamity-sir-william-pepperell)
Vol. 28| No. 2|[Benjamin Lincoln and the American Revolution](https://www.questia.com/read/1P3-56875186/benjamin-lincoln-and-the-american-revolution)
Vol. 28| No. 2|[Emily Dickinson and the Art of Belief](https://www.questia.com/read/1P3-56875242/emily-dickinson-and-the-art-of-belief)
Vol. 28| No. 2|[New Englanders on the Ohio River: The Migration and Settlement of Worthington, Ohio](https://www.questia.com/read/1P3-56875374/new-englanders-on-the-ohio-river-the-migration-and)
Vol. 28| No. 2|[Progressive Nativism: The Know-Nothing Party in Massachusetts](https://www.questia.com/read/1P3-56874880/progressive-nativism-the-know-nothing-party-in-massachusetts)
Vol. 28| No. 2|["Sober Dissent" and "Spirited Conduct": The Sandemanians and the American Revolution, 1765-1781](https://www.questia.com/read/1P3-56874849/sober-dissent-and-spirited-conduct-the-sandemanians)
Vol. 28| No. 2|[The Enigma of Mount Holyoke's Nellie Neilson](https://www.questia.com/read/1P3-56875020/the-enigma-of-mount-holyoke-s-nellie-neilson)
Vol. 28| No. 2|[The Triumph of Ethnic Progressivism: Urban Political Culture in Boston, 1900-1925](https://www.questia.com/read/1P3-56875594/the-triumph-of-ethnic-progressivism-urban-political)
Vol. 28| No. 1|[Ebb Tide in New England: Women, Seaports, and Social Change, 1630-1800](https://www.questia.com/read/1P3-50038743/ebb-tide-in-new-england-women-seaports-and-social)
Vol. 28| No. 1|[Memory's Nation: The Place of Plymouth Rock](https://www.questia.com/read/1P3-50038739/memory-s-nation-the-place-of-plymouth-rock)
Vol. 28| No. 1|[Portuguese Spinner: An American Story](https://www.questia.com/read/1P3-50038744/portuguese-spinner-an-american-story)
Vol. 28| No. 1|[Preserving Historic New England: Preservation, Progressivism, and the Remaking of Memory](https://www.questia.com/read/1P3-50038748/preserving-historic-new-england-preservation-progressivism)
Vol. 28| No. 1|[South Boston: My Home Town, the History of an Ethnic Neighborhood](https://www.questia.com/read/1P3-50038737/south-boston-my-home-town-the-history-of-an-ethnic)
Vol. 28| No. 1|[The Blackstone Canal](https://www.questia.com/read/1P3-50038732/the-blackstone-canal)
Vol. 27| No. 2|["Boycott!": Louise Imogen Guiney and the American Protective Association](https://www.questia.com/read/1P3-43276435/boycott-louise-imogen-guiney-and-the-american)
Vol. 27| No. 2|[Civil War Boston: Home Front and Battlefield](https://www.questia.com/read/1P3-43276446/civil-war-boston-home-front-and-battlefield)
Vol. 27| No. 2|["Every Composer to Be His Own Carver": The Manliness If William Billings](https://www.questia.com/read/1P3-43276400/every-composer-to-be-his-own-carver-the-manliness)
Vol. 27| No. 2|[Founding Mothers of Social Justice: The Women's Educational and Industrial Union of Boston, 1877-1892](https://www.questia.com/read/1P3-43276417/founding-mothers-of-social-justice-the-women-s-educational)
Vol. 27| No. 2|[How Massachusetts Representative Ray Flynn Left the Shadow of Busing: Boston Politics in the Fall of 1974](https://www.questia.com/read/1P3-43276442/how-massachusetts-representative-ray-flynn-left-the)
Vol. 27| No. 2|[John Quincy Adams](https://www.questia.com/read/1P3-43276447/john-quincy-adams)
Vol. 27| No. 2|[The Trials of Anthony Burns: Freedom and Slavery in Emerson's Boston](https://www.questia.com/read/1P3-43276454/the-trials-of-anthony-burns-freedom-and-slavery-in)
Vol. 27| No. 1|["And Let All the People Say Amen": Priests, Presbyters, and the Arminian Uprising in Massachusetts, 1717-1724](https://www.questia.com/read/1P3-38894140/and-let-all-the-people-say-amen-priests-presbyters)
Vol. 27| No. 1|[Boston Marathon: The History of the World's Premier Running Event](https://www.questia.com/read/1P3-38894677/boston-marathon-the-history-of-the-world-s-premier)
Vol. 27| No. 1|[Dispossession by Degrees: Indian Land and Identity in Natick Massachusetts, 1650-1790](https://www.questia.com/read/1P3-38894679/dispossession-by-degrees-indian-land-and-identity)
Vol. 27| No. 1|[Do Unto Others: The Golden Rule Fund and Organized Labor in Worcester, Massachusetts, 1955-1965](https://www.questia.com/read/1P3-38894674/do-unto-others-the-golden-rule-fund-and-organized)
Vol. 27| No. 1|[No King, No Propery: Anti-Catholicism in Revolutionary New England](https://www.questia.com/read/1P3-38894680/no-king-no-propery-anti-catholicism-in-revolutionary)
Vol. 27| No. 1|[Petersham's Ayers Brinser: Distinguished American Conservationist](https://www.questia.com/read/1P3-38894676/petersham-s-ayers-brinser-distinguished-american)
Vol. 27| No. 1|[The New England Village](https://www.questia.com/read/1P3-38894678/the-new-england-village)
Vol. 27| No. 1|[The Trials of Anthony Burns: Freedom and Slavery in Emerson's Boston](https://www.questia.com/read/1P3-38894891/the-trials-of-anthony-burns-freedom-and-slavery-in)
Vol. 27| No. 1|[The Wrongheaded and the Transparent Eye-Ball: Garrison, Emerson, and Antebellum Reform](https://www.questia.com/read/1P3-38894390/the-wrongheaded-and-the-transparent-eye-ball-garrison)
Vol. 26| No. 2|[Jonathan Belcher: Colonial Governor](https://www.questia.com/read/1P3-33440555/jonathan-belcher-colonial-governor)
Vol. 26| No. 2|[Massachusetts Gave Leadership to America's Country Life Movement: The Collaboration of Kenyon L Butterfield and Wilbert L Anderson](https://www.questia.com/read/1P3-33440548/massachusetts-gave-leadership-to-america-s-country)
Vol. 26| No. 2|[Native People of Southern New England, 1500-1650](https://www.questia.com/read/1P3-33440623/native-people-of-southern-new-england-1500-1650)
Vol. 26| No. 2|[Seers of God: Puritian Providentialism in the Restoration and Early Enlightenment](https://www.questia.com/read/1P3-33440558/seers-of-god-puritian-providentialism-in-the-restoration)
Vol. 26| No. 2|[Springfield's Washingtonians: The Triumph of Legal Sanctions to Save the Soul of the Drunkard](https://www.questia.com/read/1P3-33440553/springfield-s-washingtonians-the-triumph-of-legal)
Vol. 26| No. 2|[The Boston Longshoremen's Strike of 1931](https://www.questia.com/read/1P3-33440550/the-boston-longshoremen-s-strike-of-1931)
Vol. 26| No. 2|[Two Lithuanian Immigrants' Blasphemy Trials during the Red Scare](https://www.questia.com/read/1P3-33440549/two-lithuanian-immigrants-blasphemy-trials-during)
Vol. 26| No. 2|[Where Did Captain Martin Pring Anchor in New England?](https://www.questia.com/read/1P3-33440547/where-did-captain-martin-pring-anchor-in-new-england)
Vol. 26| No. 1|[A Hedge Away: The Other Side of Emily Dickinson's Amherst](https://www.questia.com/read/1P3-28700281/a-hedge-away-the-other-side-of-emily-dickinson-s)
Vol. 26| No. 1|[Massachusetts Congregationalist Political Thought, 1760-1790: The Design of Heaven](https://www.questia.com/read/1P3-28700306/massachusetts-congregationalist-political-thought)
Vol. 26| No. 1|[Maternal Justice: Miriam Van Waters and the Female Reform Tradition](https://www.questia.com/read/1P3-28700293/maternal-justice-miriam-van-waters-and-the-female)
Vol. 26| No. 1|["Melancholy Catastrophe!" the Story of Jason Fairbanks and Elizabeth Fales](https://www.questia.com/read/1P3-28700191/melancholy-catastrophe-the-story-of-jason-fairbanks)
Vol. 26| No. 1|[Public Days in Massachusetts Bay, 1630-1685: Reasons Behind the Ritual and the Ironic Results](https://www.questia.com/read/1P3-28700262/public-days-in-massachusetts-bay-1630-1685-reasons)
Vol. 26| No. 1|[Springfield's Puritans and Indians: 1636-1655](https://www.questia.com/read/1P3-28700243/springfield-s-puritans-and-indians-1636-1655)
Vol. 26| No. 1|[The Pequot War](https://www.questia.com/read/1P3-28700314/the-pequot-war)
Vol. 26| No. 1|[The Replacement of the Knights of Labor by the International Longshoremen's Association in the Port of Boston](https://www.questia.com/read/1P3-28700225/the-replacement-of-the-knights-of-labor-by-the-international)



## Change Log
* Mar 09, 2019 14:47 Sat -0500 EST
    * 初稿

[historical-journal]:https://www.westfield.ma.edu/historical-journal/
[questia]:https://www.questia.com "Online Research Library: Questia"

<!-- End -->
