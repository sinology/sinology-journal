# Contemporary Historical Review


**當代史學** (Contemporary Historical Review) 是由 [香港浸會大學][hkbu] [歷史系](http://histweb.hkbu.edu.hk) 主辦的學術期刊 (1998.09~2012.09)，共11卷43期。

官方網站主頁 [Contemporary Historical Review 《當代史學》](http://histweb.hkbu.edu.hk/contemporary/contem.html)。


## Journal List
Vol *1~7* 爲HTML頁面，Vol *8~12* 爲PDF文件。數據提取腳本見 [Sehll Script](/scripts/ContemporaryHistoricalReview.md)。

**注意**：`Vol. 4. No. 1 March 2001`的url地址使用的是`Vol. 3. No. 1 January 2000`的地址，需要更正。


Vol|No. (Date)|HTML|PDF
---|---|---|---
Vol.1|No.1 (September 1998)|[HTML](http://histweb.hkbu.edu.hk/contemporary/jourvol1_1/Jour1.html)|
Vol.1|No.2 (October 1998)|[HTML](http://histweb.hkbu.edu.hk/contemporary/jourvol1_2/Jour2.html)|
Vol.1|No.3 (November 1998)|[HTML](http://histweb.hkbu.edu.hk/contemporary/jourvol1_3/Jour3.html)|
Vol.1|No.4 (December 1998)|[HTML](http://histweb.hkbu.edu.hk/contemporary/jourvol1_4/Jour4.html)|
Vol.2|No.1 (January 1999)|[HTML](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_1/Jour5.htm)|
Vol.2|No.2 (April 1999)|[HTML](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_2/jour2.html)|
Vol.2|No.3 (July 1999)|[HTML](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_3/jour3.html)|
Vol.2|No.4 (October 1999)|[HTML](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_4/journal8.html)|
Vol.3|No.1 (January 2000)|[HTML](http://histweb.hkbu.edu.hk/contemporary/Jourvol3_1/journal9.htm)|
Vol.4|No.1 (March 2001)|[HTML](http://histweb.hkbu.edu.hk/contemporary/Jourvol3_1/journal9.htm)|
Vol.3|No.2 (April 2000)|[HTML](http://histweb.hkbu.edu.hk/contemporary/Jourvol3_2/journal10.htm)|
Vol.3|No.3 (August 2000)|[HTML](http://histweb.hkbu.edu.hk/contemporary/Jourvol3_3/journal11.htm)|
Vol.3|No.4 (December 2000)|[HTML](http://histweb.hkbu.edu.hk/contemporary/Jourvol3_4/journal12.htm)|
Vol.4|No.1 (March 2001)|[HTML](http://histweb.hkbu.edu.hk/contemporary/Jourvol4_1/journal13.htm)|
Vol.4|No.2 (June 2001)|[HTML](http://histweb.hkbu.edu.hk/contemporary/Jourvol4_2/journal14.htm)|
Vol.4|No.3 (September 2001)|[HTML](http://histweb.hkbu.edu.hk/contemporary/jourvol4_3/Journal15.htm)|
Vol.4|No.4 (December 2001)|[HTML](http://histweb.hkbu.edu.hk/contemporary/jourvol4_4/Journal16.htm)|
Vol.5|No.1 (March 2002)|[HTML](http://histweb.hkbu.edu.hk/contemporary/jourvol5_1/Journal17.htm)|
Vol.5|No.2 (July 2002)|[HTML](http://histweb.hkbu.edu.hk/contemporary/jourvol5_2/Journal18.htm)|
Vol.5|No.3 (October 2002)|[HTML](http://histweb.hkbu.edu.hk/contemporary/jourvol5_3/Journal19.htm)|
Vol.5|No.4 (December 2002)|[HTML](http://histweb.hkbu.edu.hk/contemporary/jourvol5_4/Journal20.htm)|
Vol.6|No.1 (March 2003)|[HTML](http://histweb.hkbu.edu.hk/contemporary/jourvol6_1/Journal21.htm)|
Vol.6|No.2 (September 2003)|[HTML](http://histweb.hkbu.edu.hk/contemporary/jourvol6_2/Jourvol22.htm)|
Vol.6|No.3 (June 2004)|[HTML](http://histweb.hkbu.edu.hk/contemporary/jourvol6_3/jourvol23.htm)|
Vol.6|No.4 (September 2004)|[HTML](http://histweb.hkbu.edu.hk/contemporary/jourvol6_4/jourvol24.htm)|
Vol.7|No.1 (June 2005)|[HTML](http://histweb.hkbu.edu.hk/contemporary/jourvol7_1/jourvol25.htm)|
Vol.7|No.2 (September 2005)|[HTML](http://histweb.hkbu.edu.hk/contemporary/jourvol7_2/jourvol26.htm)|
Vol.7|No.3 (March 2006)|[HTML](http://histweb.hkbu.edu.hk/contemporary/jourvol7_3/jourvol27.htm)|
Vol.7|No.4 (September 2006)|[HTML](http://histweb.hkbu.edu.hk/contemporary/jourvol7_4/jourvol28.htm)|
Vol.8|No.1 (March 2007)||[PDF](http://histweb.hkbu.edu.hk/contemporary/jourvol8_1.pdf)
Vol.8|No.2 (June 2007)||[PDF](http://histweb.hkbu.edu.hk/contemporary/jourvol8_2.pdf)
Vol.8|No.3 (September 2007)||[PDF](http://histweb.hkbu.edu.hk/contemporary/jourvol8_3.pdf)
Vol.8|No.4 (December 2007)||[PDF](http://histweb.hkbu.edu.hk/contemporary/jourvol8_4.pdf)
Vol.9|No.1 (March 2008)||[PDF](http://histweb.hkbu.edu.hk/contemporary/no.33.pdf)
Vol.9|No.2 (June 2008)||[PDF](http://histweb.hkbu.edu.hk/contemporary/no.34.pdf)
Vol.9|No.3 (September 2008)||[PDF](http://histweb.hkbu.edu.hk/contemporary/no.35.pdf)
Vol.9|No.4 (December 2008)||[PDF](http://histweb.hkbu.edu.hk/contemporary/no.36.pdf)
Vol.10|No.1 (March 2009)||[PDF](http://histweb.hkbu.edu.hk/contemporary/no.37.pdf)
Vol.10|No.2 (June 2009)||[PDF](http://histweb.hkbu.edu.hk/contemporary/no.38.pdf)
Vol.10|No.3 (September 2010)||[PDF](http://histweb.hkbu.edu.hk/contemporary/no39.pdf)
Vol.10|No.4 (December 2010)||[PDF](http://histweb.hkbu.edu.hk/contemporary/no40.pdf)
Vol.11|No.1 (March 2011)||[PDF](http://histweb.hkbu.edu.hk/contemporary/no41.pdf)
Vol.11|No.2 (June 2011)||[PDF](http://histweb.hkbu.edu.hk/contemporary/no42.pdf)
Vol.11|No.3 (September 2012)||[PDF](http://histweb.hkbu.edu.hk/contemporary/no43.pdf)


## Volume 1
### No 1
[Vol. 1 No. 1 September 1998](http://histweb.hkbu.edu.hk/contemporary/jourvol1_1/Jour1.html)


Category|Title|Author|University
---|---|---|---
代創刊詞|[香港浸會大學歷史學系概況－－二十年來的回顧及其展望](http://histweb.hkbu.edu.hk/contemporary/jourvol1_1/jourvol1_1main.htm#1)|周佳榮|香港浸會大學歷史學系
學術演講|[華夏族性別制度的形成及其特點（提要）](http://histweb.hkbu.edu.hk/contemporary/jourvol1_1/jourvol1_1main.htm#2 '1997年3月13日在香港浸會大學歷史學系與中國女性史研究室合辦公開講座的演講提要')|杜芳琴|天津師範大學
學術演講|[中華傳統文化與海外文化的雙向關係](http://histweb.hkbu.edu.hk/contemporary/jourvol1_1/jourvol1_1main.htm#3 '1997年5月16日在香港浸會大學歷史學系的學術演講')|來新夏|南開大學
史學新聞|[消息匯報](http://histweb.hkbu.edu.hk/contemporary/jourvol1_1/jourvol1_1main.htm#4)| |
Seminar Abstract|[Chinese Diaspora Identities Reconsidered](http://histweb.hkbu.edu.hk/contemporary/jourvol1_1/jourvol1_1main.htm#5)|Wing Chung Ng|University of Texas, San Antonio
Academic Conference|[A Report on the "International Conference: Heritage and Education"](http://histweb.hkbu.edu.hk/contemporary/jourvol1_1/jourvol1_1main.htm#6)|Yik-yi Chu| Department of History Hong Kong Baptist University
Departmental Activities|[Guest Lectures, 1997](http://histweb.hkbu.edu.hk/contemporary/jourvol1_1/jourvol1_1main.htm#7)||
Departmental Activities|[Seminars, 1997](http://histweb.hkbu.edu.hk/contemporary/jourvol1_1/jourvol1_1main.htm#8)||
Departmental Activities|[Staff Publications, 1996-1997](http://histweb.hkbu.edu.hk/contemporary/jourvol1_1/jourvol1_1main.htm#9)||


### 史學新聞
* 香港浸會大學歷史學系「日本研究講座」
* 香港大學中文系七十周年紀念國際學術研討會
* 香港保良局文物館展覽廳揭幕
* 第二屆「周恩來研究國際學術討論會」
* 「香港非政府機構面對21世紀挑戰」學術座談會
* 「華南社會組織與身份認同」講座
* 「禮教與情慾」研討會
* 香港古物古蹟辦事處講座
* 第二屆「中國商業史會議」
* 《男女》籌備創刊

#### Nan Nü
《男女：上古及帝制時期中國之兩性》([Nan Nü](https://brill.com/view/journals/nanu/nanu-overview.xml): Men, Women, and Gender in Early and Imperial China)是一部跨學科學術期刊(半年刊)，刊載具有創見的中國兩性研究論著，範圍包括史學、文學、語言學、人類學、考古學、藝術、音樂、法律、哲學、醫學、科學、及宗教等。時限起自中國文明之始，迄二十世紀初期。另設書評專欄，評論近期以中文、日文及西方語文出版的中國兩性研究專著；間中亦刊載中國兩性研究近期動態之學術報告和書評論文。所有文章均以英語發表。編輯與出版費用由荷蘭萊頓 Brill Academic Publishers 及美國聖路易市華盛頓大學分擔。

>自 2004 年起，《男女》改名《男女：中國男性、女性及性別》(Nan Nü: Men, Women, and Gender in China)，將涵蓋的範圍伸展至整個 20 世紀，個別論文更牽涉現今社會的討論。 -- [走過十五載的《男女》 - 盧嘉琪](http://www.mh.sinica.edu.tw/MHDocument/PublicationDetail/PublicationDetail_1498.pdf '中央研究院近代史研究所 近代中國婦女史研究 第 20 期（2012 年 12 月）')


### No 2
[Vol. 1 No. 2 October 1998](http://histweb.hkbu.edu.hk/contemporary/jourvol1_2/Jour2.html)

Category|Title|Author|University
---|---|---|---
院校巡禮|[北京大學歷史學系簡介](http://histweb.hkbu.edu.hk/contemporary/jourvol1_2/jourvol1_2main.htm#1)||北京大學歷史學系
研究動態|[回憶與歷史－－兼論德國紀臣大學 (University of Giessen)的「回憶文化」研究計劃](http://histweb.hkbu.edu.hk/contemporary/jourvol1_2/jourvol1_2main.htm#2)|麥勁生|香港浸會大學歷史系
學術會議|[玉器．絲織品．漢簡－－「開闢歷史教育的眼界」講座紀要](http://histweb.hkbu.edu.hk/contemporary/jourvol1_2/jourvol1_2main.htm#3)|周佳榮|香港浸會大學歷史系
史學新聞|[學人動向](http://histweb.hkbu.edu.hk/contemporary/jourvol1_2/jourvol1_2main.htm#4)
Historian and History|[Jacob Burckhardt: A Man of Contradictions](http://histweb.hkbu.edu.hk/contemporary/jourvol1_2/jourvol1_2main.htm#5)|Yik-yi Chu|Department of History Hong Kong Baptist University
Academic Conference|[A Brief Note on the International Conference on "James Legge: The Heritage of China and the West"](http://histweb.hkbu.edu.hk/contemporary/jourvol1_2/jourvol1_2main.htm#6)|Wong Man-kong|Hong Kong Baptist University
Academic Conference|[A Report on the "International Conference on Hong Kong and Modern China"](http://histweb.hkbu.edu.hk/contemporary/jourvol1_2/jourvol1_2main.htm#7)|Chung Po-yin|Department of History Hong Kong Baptist University

### No 3
[Vol. 1 No. 3 November 1998](http://histweb.hkbu.edu.hk/contemporary/jourvol1_3/Jour3.html)

Category|Title|Author|University
---|---|---|---
院校巡禮|[南開大學歷史系簡介](http://histweb.hkbu.edu.hk/contemporary/jourvol1_3/jourvol1_3main.htm#1)|王富春|南開大學歷史系
研究札記|[越南漢文史籍解題](http://histweb.hkbu.edu.hk/contemporary/jourvol1_3/jourvol1_3main.htm#2)|周佳榮|香港浸會大學歷史系
學術會議|[近代中國基督教史研討會報導](http://histweb.hkbu.edu.hk/contemporary/jourvol1_3/jourvol1_3main.htm#3)||香港浸會大學歷史系資料室
Historian and History|[Why Did Wang Tao go to Hong Kong? Some Preliminary Observation from the Unpublished Documents in the Public Records Office, London](http://histweb.hkbu.edu.hk/contemporary/jourvol1_3/jourvol1_3main.htm#4)|Natascha Vittinghoff|Heidelberg University
Seminar Abstract|[The Curious Case of He Shuangqing 賀雙卿: The Grest Peasant Woman Poet](http://histweb.hkbu.edu.hk/contemporary/jourvol1_3/jourvol1_3main.htm#5)|Paul S.Ropp|Clark University
Academic Journal|[Journal of the History of Christianity in Modern China](http://histweb.hkbu.edu.hk/contemporary/jourvol1_3/jourvol1_3main.htm#6)|Cathy J.Potter|The Chinese University of Hong Kong

#### Academic Journal
Journal of the History of Christianity in Modern China (近代中國基督教史研究集刊)

>Co-published by the Department of History and the Centre for Educational Development, the Journal of the History of Christianity in Modern China近代中國基督教史研究集刊 is to be published annually in April. Dr. Lee Kam-keung serves as the chief editor and the Editorial Board members include Prof. J. Barton Starr, Dr. Chow Kai-wing, Dr. Lam Kai-yin and Dr. Wong Man-kong. 15 scholars from China Mainland, Taiwan, Hong Kong, Japan, and U.S.A. agreed to serve in the Editorial Advisory Board.

[香港浸會大學][hkbu] [圖書館](https://library.hkbu.edu.hk)提供頁面
[Archives on the History of Christianity in China](https://library.hkbu.edu.hk/sca/ahc.html)，其中的 [References](https://library.hkbu.edu.hk/sca/ahc_ref.html)列有該期刊的Vol信息。

<!-- * [Collection in the Archives on the History of Christianity in China (Microform Materials)](https://library.hkbu.edu.hk/sca/file/ahc_mf.pdf) 2533種
* [Printed Materials in the Archives on the History of Christianity in China](https://library.hkbu.edu.hk/sca/file/ahc.pdf) 5488種 -->

* [Vol.1](https://library.hkbu.edu.hk/sca/file/cmc01.html)
* [Vol.2](https://library.hkbu.edu.hk/sca/file/cmc02.html)
* [Vol.3](https://library.hkbu.edu.hk/sca/file/cmc03.html)
* [Vol.4](https://library.hkbu.edu.hk/sca/file/vol_4/cmc04.html)
* [Vol.5](https://library.hkbu.edu.hk/sca/file/vol_5/cmc05.html)
* [Vol.6](https://library.hkbu.edu.hk/sca/file/vol_6/cmc06.html)
* [Vol.7](https://library.hkbu.edu.hk/sca/file/vol_7/cmc07.html)
* [Vol.8](https://library.hkbu.edu.hk/sca/file/vol_8/cmc08.html)
* [Vol.9](https://library.hkbu.edu.hk/sca/file/vol_9/cmc09.html)
* [Vol.10](https://library.hkbu.edu.hk/sca/file/vol_10/cmc10.html)


[聖神修院神哲學院](http://www.hsscol.org.hk "Holy Spirit Seminary College of Theology & Philosophy")提供資料集 [中國新方誌中的基督宗教資料](http://www.hsscol.org.hk/FangZhi/main.htm)，摘錄中國各地方誌中關於基督教、天主教的記錄。

>傳統以來中國官方都以編撰誌書的方式，記錄當地的風土人情事物。每在承平的時候，由中央至地方各級政府將修訂其轄區的「誌」列入其重要的工作。在中央則編修「一統誌」，地方則按其行政劃分有不同的「省誌」、「府誌」、「縣誌」甚至「鎮誌」、「村誌」等。上世紀二零年代民國時期，各地政府就曾陸續編修過部份地區的方誌，我們一般稱為「舊方誌」資料。最新的修訂則要到上世紀八零年代才由現時的中央人民政府推動重新修訂各地的方誌。目前各省、市、州、縣等所修訂方誌已有數千冊，我們一般稱之為「新方誌」。


香港中文大學 [中國研究服務中心][usc_cuhk]

>1988年中心加入香港中文大學後，1993年更名為中國研究服務中心，為中國研究者提供無償服務的宗旨未變。如今，中心成為擁有當代中國國情研究最齊全的圖書館，其使用之方便為海內外學者稱道。中心收藏主要包括：
* 五十年代初至今的省級及全國性報紙、期刊、以及學術機構、政府部份出版的報刊的印刷版及電子版收藏
* 完整的全國、省、市級綜合及專類年鑒、統計資料
* 省、市、縣、鄉鎮級地方志，包括縣一級的土地、糧食、財稅、教育、水利等專門志
* 中、英文中國研究專著八萬多冊，尤多地區研究資料
* 各類光盤資料


### No 4
[Vol. 1 No. 4 December 1998](http://histweb.hkbu.edu.hk/contemporary/jourvol1_4/Jour4.html)

Category|Title|Author|University
---|---|---|---
學術演講|[清儒的考證、經世與制度重建－－從「以禮代理」談起](http://histweb.hkbu.edu.hk/contemporary/jourvol1_4/jourvol1_4main.htm#1)|張壽安|中央研究院近代史研究所
學術會議|[「嚴復與中國近代化」學術研討會紀要](http://histweb.hkbu.edu.hk/contemporary/jourvol1_4/jourvol1_4main.htm#2)|林啟彥|香港浸會大學歷史系
學術會議|[當代中國經濟改革和社會發展研討會紀要](http://histweb.hkbu.edu.hk/contemporary/jourvol1_4/jourvol1_4main.htm#3)|周佳榮|香港浸會大學歷史系
學術會議|[戊戌維新百周年紀念國際學術會議報導](http://histweb.hkbu.edu.hk/contemporary/jourvol1_4/jourvol1_4main.htm#4)||香港浸會大學歷史系資料室
學術會議|[近代中國海防國際學術會議報導](http://histweb.hkbu.edu.hk/contemporary/jourvol1_4/jourvol1_4main.htm#5)||香港浸會大學歷史系資料室
Seminar Abstract|[The "Causes" of the New Imperialism in the Nineteeth Century](http://histweb.hkbu.edu.hk/contemporary/jourvol1_4/jourvol1_4main.htm#6)|Cathy J.Potter|The Chinese University of Hong Kong
Departmental Activities|[Guest Lectures, 1998](http://histweb.hkbu.edu.hk/contemporary/jourvol1_4/jourvol1_4main.htm#7)
Departmental Activities|[Academic Conferences, 1997-1998](http://histweb.hkbu.edu.hk/contemporary/jourvol1_4/jourvol1_4main.htm#8)
Departmental Activities|[Seminars, 1998](http://histweb.hkbu.edu.hk/contemporary/jourvol1_4/jourvol1_4main.htm#9)
Departmental Activities|[Staff Publications, 1997-1998](http://histweb.hkbu.edu.hk/contemporary/jourvol1_4/jourvol1_4main.htm#10)


## Volume 2
### No 1
[Vol. 2 No. 1 January 1999](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_1/Jour5.htm)

Category|Title|Author|University
---|---|---|---
學術演講|[台灣歷史上的琉球難民遭風案](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_1/jourvol2_1main.htm#1)|楊彥杰|福建社會科學院台灣研究所
研究札記|[傳統中國都市之模式](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_1/jourvol2_1main.htm#2)|李金強|香港浸會大學歷史系
書刊評介|[黃仁宇教授新著兩種介紹](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_1/jourvol2_1main.htm#3)|周佳榮|香港浸會大學歷史系
書刊評介|[《中國歷史地理論叢》簡介](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_1/jourvol2_1main.htm#4)|王雙懷|陝西師範大學歷史學系
學術會議|[「林則徐、鴉片戰爭與香港」國際研討會紀要](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_1/jourvol2_1main.htm#5)||香港浸會大學歷史系資料室
史學新聞|[香港浸會大學歷史學系活動簡報](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_1/jourvol2_1main.htm#6)||香港浸會大學歷史系資料室
史學新聞|[香港中華文化促進中心活動簡報](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_1/jourvol2_1main.htm#7)||香港浸會大學歷史系資料室
Research Notes|[The Definition of "Modern China": On the Question of "When Did It Begin?"](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_1/jourvol2_1main.htm#8)|Yik-yi Chu|Department of History Hong Kong Baptist University
Book News|[Clara Wing-chung Ho (ed), Biographical Dictionary of Chinese Women, The Qing Period, 1644-1911](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_1/jourvol2_1main.htm#9)||
Book News|[Ricardo King Sang Mak, The Future of the Non-Western World in the Social Sciences of 19th Century England](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_1/jourvol2_1main.htm#10)||


#### 史學新聞
開辦「中國醫學史」科目 (見[中醫藥學院](https://scm.hkbu.edu.hk)開設的[中醫學學士及生物醫學理學士(榮譽)學位課程
](https://scm.hkbu.edu.hk/tc/education/undergraduate_programmes/bachelor_of_chinese_medicine_and_bachelor_of_science_Hons_in_biomedical_science/index.html))。

>香港浸會大學於1998年9月起開辦中國醫藥學位課程，是香港高等院校的創舉。歷史系配合校方的需要，同時開辦「中國醫學史」一科，作為該課程一年級學生的必修科目之一，由周佳榮博士、林啟彥博士、李金強博士合教。
>
>中國醫學史共有八講：第一講為〈導論：中國醫學史的一些問題〉；第二講為〈上古的醫學：先秦時期〉；第三至五講為〈中古的醫學〉，包括漢至魏晉時期、隋唐時期、宋元時期；第六講為〈近世的醫學：明清時期〉；第七講為〈近世以來醫學的發展〉，第八講為〈總結：走向二十一世紀的中國醫學〉。


### No 2
[Vol. 2 No. 2 April 1999](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_2/jour2.html)

Category|Title|Author|University
---|---|---|---
學術演講|[明代華南的社會風貌](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_2/jourvol2_2main.htm#1)|王雙懷|陝西師範大學歷史系 香港浸會大學歷史系訪問學人
書刊評介|[香港研究書目提要（1996-98）](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_2/jourvol2_2main.htm#2)|周佳榮|香港浸會大學歷史系
書刊評介|[簡介兩種有關中國近代海軍教育發展史的著述](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_2/jourvol2_2main.htm#3)|周子峰|香港浸會大學歷史系
Research Notes|[Translating Western Knowledge into Late Imperial China: Introduction to the Modern Chinese Scientific Terminologies Project](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_2/jourvol2_2main.htm#4)|Natascha Vittinghoff-Gentz|Gottingen University
Academic Conference|[A Report on the International Symposium on Sino-German Relations since 1800](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_2/jourvol2_2main.htm#5)|Ricardo K.S. Mak|Department of History Hong Kong Baptist University


#### 書刊評介
香港研究書目提要（1996-98）

書名|作者|頁數|出版日期|出版機構
---|---|---|---|---
《香港雜記‧外二種》|清】陳鏸勳撰、莫世祥校注|243頁|1996年|廣州：暨南大學出版社
《香港開埠與關家》|關肇碩、容應萸著|123頁|1997年|香港：廣角鏡出版社
《香港史新編》上、下冊|王賡武主編|903頁|1997年|香港：三聯書店
《香港中文教育發展史》|王齊樂|380頁|1996年|香港：三聯書店
《戰後香港軌跡──民生苦樂》<br/>《戰後香港軌跡──社會掠影》|鍾文略攝影，周佳榮、鍾寶賢、黃文江編撰|131頁 + 134頁|1997年|香港：商務印書館
《香港大視野 ── 亞洲網絡中心》|【日】濱下武志著、馬宋芝譯|204頁|1997年|香港：商務印書館
《歷史的跫音 ── 歷代詩人詠香港》|胡從經編纂|361頁|1997年|香港：朝花出版社
《中國名人在香港 ──30、40年代在港活動紀實》|吳倫霓霞、余炎光編著|247頁|1997年|香港：香港教育圖書公司
《香港學》|李英明||1997年|台北：揚智文化事業股份有限公司
《粵港關係史(1840-1984)》|鄧開頌、陸曉敏主編|395頁|1997年|香港：麒麟書業有限公司
《香港全紀錄》卷一(遠古─1959年)|陳昕、郭志坤主編|450頁|1997年|香港：中華書局
《香港全紀錄》卷二(1960-1997年)|陳昕、郭志坤主編|493頁|1998年|香港：中華書局
《早期香港史研究資料選輯》上、下冊|馬金科主編|895頁|1998年|香港：三聯書店
《香港近現代文學書目(1840-1950)》|胡從經編纂|253頁|1998年|香港：朝花出版社
《香港書店巡禮》|徐振邦、方禮年、翁文英合著|253頁|1998年|香港：獲益出版事業有限公司
《香港的歷史與文物》|蕭國健|184頁|1997年|香港：明報出版社
《簡明香港歷史》|陳志華、黃家樑|174頁|1998年|香港：明報出版社
《簡明香港史》|劉蜀永主編|376頁|1998年|香港：三聯書店


### No 3
[Vol. 2 No. 3 July 1999](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_3/jour3.html)

Category|Title|Author|University
---|---|---|---
研究札記|[《古今圖書集成》版本淺談](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_3/jourvol2_3main.htm#1)|葉金菊|北京清華大學圖書館
研究札記|[日本學書目舉要－－以有關日本史的中英文著作為主](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_3/jourvol2_3main.htm#2)|周佳榮|香港浸會大學歷史系
書刊評介|[《明清社會文化生態》](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_3/jourvol2_3main.htm#3)|黃毓棟|香港大學中文系
Departmental Activities|[Guest Lectures, January-June 1999](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_3/jourvol2_3main.htm#4)||
Departmental Activities|[Academic Conference, January-June 1999](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_3/jourvol2_3main.htm#5)||
Departmental Activities|[Staff Activities, January-June 1999](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_3/jourvol2_3main.htm#6)||
Departmental Activities|[Staff Publications, January-June 1999](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_3/jourvol2_3main.htm#7)||
Book News|[Child-rearing in Ancient China](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_3/jourvol2_3main.htm#8) (中國古代育兒)|Clara Wing-chung Ho|
Book News|[Virtue, Talent, Beauty, and Power: Women in Ancient China](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_3/jourvol2_3main.htm#9) (德‧才‧色‧權──論中國古代女性)|Clara Wing-chung Ho|
Book News|[Chow Kai Wing, New Citizen and Revival: Major Themes in Modern Chinese Thought](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_3/jourvol2_3main.htm#10)||


#### 欽定古今圖書集成
線上閱覽
* [wikisource.org](https://zh.wikisource.org/wiki/%E6%AC%BD%E5%AE%9A%E5%8F%A4%E4%BB%8A%E5%9C%96%E6%9B%B8%E9%9B%86%E6%88%90 '欽定古今圖書集成')
* [ctext.org](https://ctext.org/library.pl?if=gb&res=81155 '民國二十三年中華書局影印')

[香港公共圖書館](https://www.hkpl.gov.hk)提供全文電子資料庫，具體見 [古今圖書集成 資料庫使用指南](http://www.hkpl.gov.hk/en/common/attachments/e-resources/e-databases/3folded_GJTSJC_UserGuide_20151022.pdf)。

Columbia University (哥倫比亞大學)收藏有一套 『欽定古今圖書集成』，具體見
* [Starr East Asian Library, Columbia University](http://sinology.teldap.tw/index.php/2012-02-15-02-34-37?task=detail&acaid=57&typeid=1)。
* [李鸿章最后一次交涉：《古今图书集成》是怎么来的？](https://cul.qq.com/a/20170104/004972.htm)


### No 4
[Vol. 2 No. 4 October 1999](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_4/journal8.html)

Category|Title|Author|University
---|---|---|---
學術演講|[關於武則天評價的幾個問題](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_4/jourvol2_4main.htm#1)|王雙懷|陝西師範大學歷史系
學術演講|[赤鱲角考古發展史](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_4/jourvol2_4main.htm#2)|林錦源|香港歷史博物館
研究述評|[《教育雜誌》與近代中國的歷史教學研究](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_4/jourvol2_4main.htm#3)|周佳榮|香港浸會大學歷史系
史學新聞|[香港的敦煌石窟畫展和出版活動](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_4/jourvol2_4main.htm#4)||香港浸會大學歷史系資料室
史學新聞|[新書匯報](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_4/jourvol2_4main.htm#5)||香港浸會大學歷史系資料室
Book News|[Coastal Defence and Maritime Economy of Modern China](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_4/jourvol2_4main.htm#6)|Lee Kam-keung, Lau Yee-cheung and Mak King-sang (eds.)|
Book News|[The HKBU Journal of Historical Studies: A Collection of Essays in Commemoration of the Twentieth Anniversary of the Department of History, Hong Kong Baptist University](http://histweb.hkbu.edu.hk/contemporary/Jourvol2_4/jourvol2_4main.htm#7)|Lee Kam-keung, Lau Yee-cheung and Mak King-sang (eds.)|

#### 新書匯報

書名|作者|頁數|出版日期|出版機構
---|---|---|---|---
《德‧才‧色‧權 ——論中國古代女性》|劉詠聰|397頁|1998年|台北：麥田出版
《妝臺與妝臺以外——中國女史研究論集》|黃嫣梨|208頁|1999年|香港：牛津大學出版社
《新民與復興 ——近代中國思想論》|周佳榮|298頁|1999年|香港：香港教育圖書公司
《王爾敏教授七十華誕暨榮休論文集》|李金強主編|339頁|1999年|香港：本書編輯委員會
《文明的憧憬 ——近代中國對民族與國家典範的追尋》|鮑紹霖|217頁|1999年|香港：中文大學出版社|181頁|1999年|香港：利文出版社


## Volume 3
### No 1
[Vol. 3 No. 1 January 2000](http://histweb.hkbu.edu.hk/contemporary/Jourvol3_1/journal9.htm)

Category|Title|Author|University
---|---|---|---
研究札記|[簡論《駱克報告書》](http://histweb.hkbu.edu.hk/contemporary/Jourvol3_1/jourvol3_1main.htm#1)|黃文江|香港浸會大學歷史系
學術會議|[記第二屆華人地區大學通識教育學術研討會](http://histweb.hkbu.edu.hk/contemporary/Jourvol3_1/jourvol3_1main.htm#2)||
學術會議|[「資訊科技與歷史教育」研討會紀錄](http://histweb.hkbu.edu.hk/contemporary/Jourvol3_1/jourvol3_1main.htm#3)||香港浸會大學歷史系資料室
書刊評介|[Chinese Women in the Imperial Past: New Perspectives By Zurndorfer Harriet T](http://histweb.hkbu.edu.hk/contemporary/Jourvol3_1/jourvol3_1main.htm#4)|盧嘉琪|香港浸會大學歷史系
新書介紹|[新書匯報](http://histweb.hkbu.edu.hk/contemporary/Jourvol3_1/jourvol3_1main.htm#5)||
Academic Conferences|["Tranlating Western Knowledge Into Late Imperial China" Bericht uber die internationale Konferenz an der University Gotting vom 6-9. Dez. 1999](http://histweb.hkbu.edu.hk/contemporary/Jourvol3_1/jourvol3_1main.htm#6)|Nora Sausmika|Department of Political Sciences/ Asian Studies Duisburg University
Academic Conferences|[A Report on the Southwest Conference on Asian Studies and the Historical Society for 20th Century China](http://histweb.hkbu.edu.hk/contemporary/Jourvol3_1/jourvol3_1main.htm#7)|Yik-yi Chu|Department of History Hong Kong Baptist University
Departmental Activities|[Staff Activities, July-December 1999](http://histweb.hkbu.edu.hk/contemporary/Jourvol3_1/jourvol3_1main.htm#8)||

#### 新書匯報

書名|作者|頁數|出版日期|出版機構
---|---|---|---|---
《勸化金箴——清代善書研究》|游子安|314頁|1999年|天津：天津人民出版社
《澳門開埠初期史研究》|湯開建|319頁|1999年|北京：中華書局


### No 2
[Vol. 3 No. 2 April 2000](http://histweb.hkbu.edu.hk/contemporary/Jourvol3_2/journal10.htm)

Category|Title|Author|University
---|---|---|---
研究札記|[日本出版中國近代史研究書目提要（上編）](http://histweb.hkbu.edu.hk/contemporary/Jourvol3_2/jourvol3_2main.htm#1)|林啟彥編|香港浸會大學歷史系
書刊評介|[一個典型中國知識分子家庭－－吳荔明《梁啟超和他的兒女們》介紹](http://histweb.hkbu.edu.hk/contemporary/Jourvol3_2/jourvol3_2main.htm#2)|周佳榮|香港浸會大學歷史系
Academic Conference|[A Report on the International Conference on the Notion of Time in Historical Thinking](http://histweb.hkbu.edu.hk/contemporary/Jourvol3_2/jourvol3_2main.htm#3)|Mak King Sang|Department of History Hong Kong Baptist University


### No 3
[Vol. 3 No. 3 August 2000](http://histweb.hkbu.edu.hk/contemporary/Jourvol3_3/journal11.htm)

Category|Title|Author|University
---|---|---|---
研究札記|[中國農業的機遇與挑戰](http://histweb.hkbu.edu.hk/contemporary/Jourvol3_3/jourvol3_3main.htm#1)|王雙懷|陝西師範大學歷史系
研究札記|[日本歷史的分期問題](http://histweb.hkbu.edu.hk/contemporary/Jourvol3_3/jourvol3_3main.htm#2)|周佳榮|香港浸會大學歷史系
研究札記|[「口述歷史」(Oral History)的社會意義](http://histweb.hkbu.edu.hk/contemporary/Jourvol3_3/jourvol3_3main.htm#3)|鍾寶賢|香港浸會大學歷史系
書刊評介|[中韓關係史三種](http://histweb.hkbu.edu.hk/contemporary/Jourvol3_3/jourvol3_3main.htm#4)|范永聰|香港浸會大學歷史系
書刊評介|[新書匯報](http://histweb.hkbu.edu.hk/contemporary/Jourvol3_3/jourvol3_3main.htm#5)||香港浸會大學歷史系資料室
Seminar Abstract|[Prof. Jane Kate Leonard's Visit to Hong Kong Baptist University](http://histweb.hkbu.edu.hk/contemporary/Jourvol3_3/jourvol3_3main.htm#6)|


#### 新書匯報

書名|作者|頁數|出版日期|出版機構
---|---|---|---|---
《主體的追尋──中國婦女史研究析論》|葉漢明|407頁|1999年|香港：香港教育圖書公司
《經營文化──中國社會單元的管理與運作》|香港科技大學華南研究中心、華南研究會合編|370頁|1999年|香港：香港教育圖書公司
《近代史學與史學方法》|麥勁生|259頁|2000年|臺北：五南圖書出版公司


### No 4
[Vol. 3 No. 4 December 2000](http://histweb.hkbu.edu.hk/contemporary/Jourvol3_4/journal12.htm)

Category|Title|Author|University
---|---|---|---
院校巡禮|[遼寧社會科學院歷史研究所簡介](http://histweb.hkbu.edu.hk/contemporary/Jourvol3_3/jourvol3_4main.htm#1)|王冬芳|遼寧社會科學院歷史研究所
研究札記|[明史研究書籍提要](http://histweb.hkbu.edu.hk/contemporary/Jourvol3_3/jourvol3_4main.htm#2)|周佳榮|香港浸會大學歷史系
新書推介|[孫國棟《唐宋史論叢》](http://histweb.hkbu.edu.hk/contemporary/Jourvol3_3/jourvol3_4main.htm#3)|

#### 新書推介
![](http://histweb.hkbu.edu.hk/contemporary/Jourvol3_4/image_2.jpg "孫國棟《唐宋史論叢》")


## Volume 4
### No 1
[Vol. 4 No. 1 March 2001](http://histweb.hkbu.edu.hk/contemporary/Jourvol4_1/journal13.htm)

Category|Title|Author|University
---|---|---|---
研究札記|[篡弒者的女兒－－王莽女兒孝平皇后、曹操女兒獻穆皇后、楊堅女兒宣帝楊皇后](http://histweb.hkbu.edu.hk/contemporary/Jourvol4_1/jourvol4_1main.htm#1)|黃嫣梨|香港浸會大學歷史系
研究札記|[滿族崛起時代女性在社會與家庭中的地位](http://histweb.hkbu.edu.hk/contemporary/Jourvol4_1/jourvol4_1main.htm#2)|王冬芳|遼寧社會科學院歷史研究所
研究札記|[現代中國史學的成立（1900-1949年）－－從史學概論到研究方法](http://histweb.hkbu.edu.hk/contemporary/Jourvol4_1/jourvol4_1main.htm#3)|周佳榮|香港浸會大學歷史系
書刊評介|[國際華文書訊二則](http://histweb.hkbu.edu.hk/contemporary/Jourvol4_1/jourvol4_1main.htm#4)||香港浸會大學歷史系資料
史學新聞|[香港中國近代史學會通訊](http://histweb.hkbu.edu.hk/contemporary/Jourvol4_1/jourvol4_1main.htm#5)||香港中國近代史學會
史學新聞|[學界活動預告](http://histweb.hkbu.edu.hk/contemporary/Jourvol4_1/jourvol4_1main.htm#6)||香港浸會大學歷史系資料室


### No 2
[Vol. 4 No. 1 March 2001](http://histweb.hkbu.edu.hk/contemporary/Jourvol4_1/journal13.htm)

Category|Title|Author|University
---|---|---|---
社團巡禮|[孫中山文教福利基金會簡介](http://histweb.hkbu.edu.hk/contemporary/Jourvol4_2/jourvol4_2main.htm#1)||孫中山文教福利基金會提供
學界消息|[「孫中山與辛亥革命」徵文比賽](http://histweb.hkbu.edu.hk/contemporary/Jourvol4_2/jourvol4_2main.htm#2)||香港浸會大學歷史系資料室
人物研究|[文天祥、王守仁、舒位與龔自珍](http://histweb.hkbu.edu.hk/contemporary/Jourvol4_2/jourvol4_2main.htm#3)|王煜|香港中文大學哲學系
人物研究|[從「師夷之長技……」到「執西用中」－－嚴復學西方思想的發展](http://histweb.hkbu.edu.hk/contemporary/Jourvol4_2/jourvol4_2main.htm#4)|林平漢|福建師範大學嚴復研究所
研究述評|[二十世紀中國醫學史研究的回顧](http://histweb.hkbu.edu.hk/contemporary/Jourvol4_2/jourvol4_2main.htm#5)|周佳榮|香港浸會大學歷史系

#### 研究述評
二十世紀中國醫學史研究的回顧


### No 3
[Vol. 4 No. 3 September 2001](http://histweb.hkbu.edu.hk/contemporary/jourvol4_3/Journal15.htm)

Category|Title|Author|University
---|---|---|---
研究札記|[蔡元培著譯書籍解題](http://histweb.hkbu.edu.hk/contemporary/jourvol4_3/jourvol4_3main.htm#1)|周佳榮|香港浸會大學歷史系　
學報介紹|[《香港中國近代史學會會刊》第一至十期要目](http://histweb.hkbu.edu.hk/contemporary/jourvol4_3/jourvol4_3main.htm#2)||香港浸會大學歷史系資料室
學界消息|[「二十世紀中國之再詮譯」國際研討會](http://histweb.hkbu.edu.hk/contemporary/jourvol4_3/jourvol4_3main.htm#3)||
Research Notes|[The Business Concerns, Hong Kong, and Sino-American Relations](http://histweb.hkbu.edu.hk/contemporary/jourvol4_3/jourvol4_3main.htm#4)|Cindy Yik-yi Chu|Department of History Hong Kong Baptist University

#### 香港中國近代史學會會刊
《香港中國近代史學會會刊》第一至十期要目

>香港中國近代史學會成立於1985年，是香港地區一群從事中國近代史研究的學者所發起，會員包括香港各大專院校的史學工作者和研究生，亦有部份會員來自中國內地、台灣、日本、澳洲、美國、加拿大等地。該會的主要活動，除舉辦學術活動、聯絡各地學者外，亦致力於編印學術專著，其中一項重大的工作是出版《香港中國近代史學會會刊》。自1987年創刊以來，每隔一年或兩年出版一期，至1999年間，總共有十期。


### No 4
[Vol. 4 No. 4 December 2001](http://histweb.hkbu.edu.hk/contemporary/jourvol4_4/Journal16.htm)

Category|Title|Author|University
---|---|---|---
研究論文|[近代工商同業公會制度的現代性芻論](http://histweb.hkbu.edu.hk/contemporary/jourvol4_4/jourvol4_4main.htm#1)|彭南生|華中師範大學 中國近代史研究所　
史學家誌|[陳荊和教授著述目錄](http://histweb.hkbu.edu.hk/contemporary/jourvol4_4/jourvol4_4main.htm#2)|周佳榮|香港浸會大學歷史系
研究札記|[檔案使用者談檔案](http://histweb.hkbu.edu.hk/contemporary/jourvol4_4/jourvol4_4main.htm#3)|黃文江|香港浸會大學歷史系
史學新聞|[第四屆潮州學國際研討會紀要](http://histweb.hkbu.edu.hk/contemporary/jourvol4_4/jourvol4_4main.htm#4)||香港浸會大學歷史系資料室
史學新聞|[香港學界活動簡報](http://histweb.hkbu.edu.hk/contemporary/jourvol4_4/jourvol4_4main.htm#5)||香港浸會大學歷史系資料室
書刊評介|[浸大歷史系新著介紹](http://histweb.hkbu.edu.hk/contemporary/jourvol4_4/jourvol4_4main.htm#6)||香港浸會大學歷史系資料室
書刊評介|[華文史學新書介紹](http://histweb.hkbu.edu.hk/contemporary/jourvol4_4/jourvol4_4main.htm#7)||香港浸會大學歷史系資料室

#### 書刊評介
華文史學新書介紹

書名|作者|頁數|出版日期|出版機構
---|---|---|---|---
《海外華人的抗爭──對美抵制運動史實與史料》|黃賢強|282頁|2001年|新加坡：新加坡亞洲研究學會


## Volume 5
### No 1
[Vol. 5 No. 1 March 2002](http://histweb.hkbu.edu.hk/contemporary/jourvol5_1/Journal17.htm)

Category|Title|Author|University
---|---|---|---
院校巡禮|[華中師範大學中國近代史研究所簡介](http://histweb.hkbu.edu.hk/contemporary/jourvol5_1/jourvol5_1main.htm#1)|彭南生|華中師範大學 中國近代史研究所
研究札記|[中國婦女纏足之始的再探討](http://histweb.hkbu.edu.hk/contemporary/jourvol5_1/jourvol5_1main.htm#2)|王冬芳|遼寧社會科學院史研究所
史學家誌|[從《陳君葆日記》看陳寅恪在香港期間的活動](http://histweb.hkbu.edu.hk/contemporary/jourvol5_1/jourvol5_1main.htm#3)|周佳榮|香港浸會大學歷史系
新書介紹|[近刊香港高等教育圖史兩種](http://histweb.hkbu.edu.hk/contemporary/jourvol5_1/jourvol5_1main.htm#4)||香港浸會大學歷史系資料室


#### 史學家誌
《陳君葆日記》

* [淺論《陳君葆日記》](https://blog.boxun.com/hero/huzhiweiwenji/18_1.shtml)
* [《陳君葆日記全集》人物訂誤一則](http://nansha.schina.ust.hk/Article_DB/sites/default/files/pubs/news-048.02.pdf) (PDF)
* [好人陳君葆，我們不該忘記](http://paper.takungpao.com/resfile/2013-03-16/B11/B11.pdf) (大公報 PDF)

香港大學
* [香港大學馮平山圖書館特藏資料](https://lib.hku.hk/sites/all/files/files/hkspc/FPSL.pdf) (PDF)
* [陳君葆書信文件](http://archives.lib.cuhk.edu.hk/assets/PI/ChenJunbao.pdf) (PDF)

抗戰時期搶救陷區古籍
* [抗戰時期央圖留港遭日劫掠圖書尋獲過程之探討](https://nclfile.ncl.edu.tw/files/201612/c61ed0bf-eb90-4f86-a75e-0201aea1dab3.pdf) (PDF)
* [抗戰時期搶救陷區古籍諸說述評](http://www.gaya.org.tw/journal/m57/57-lib.pdf "張錦郎 國家圖書館退休人員") (PDF)
* [郑振铎与战时文献抢救及战后追索](http://www.wxpl.org/Admin/UploadFile/SiteContent/FJList/u13zcva1.pdf) (PDF)


#### 新書介紹
近刊香港高等教育圖史兩種

書名|作者|頁數|出版日期|出版機構
---|---|---|---|---
《與中大一同成長：香港中文大學與中國文化研究所圖史，1949-1997》||304頁|2000年|香港：香港中文大學中國文化研究所
《學府時光：香港大學的歷史面貌》<br/>(Past Visions of the Future: Some Perspectives on the History of The University of Hong Kong)|||2001年|香港：香港大學美術博物館，。


### Vol 2
[Vol. 5 No. 2 July 2002](http://histweb.hkbu.edu.hk/contemporary/jourvol5_2/Journal18.htm)

Category|Title|Author|University
---|---|---|---
專題演講|[近代中國自由主義的發展與若干問題](http://histweb.hkbu.edu.hk/contemporary/jourvol5_2/jourvol5_2main.htm#1)|
研究札記|[中國教會大學史論著提要](http://histweb.hkbu.edu.hk/contemporary/jourvol5_2/jourvol5_2main.htm#2)|
學界消息|[香港學界活動簡報](http://histweb.hkbu.edu.hk/contemporary/jourvol5_2/jourvol5_2main.htm#3)|


### Vol 3
[Vol. 5 No. 3 October 2002](http://histweb.hkbu.edu.hk/contemporary/jourvol5_3/Journal19.htm)

Category|Title|Author|University
---|---|---|---
專題演講|[近代以來中日關係的演變：回顧與展望](http://histweb.hkbu.edu.hk/contemporary/jourvol5_3/jourvol5_3main.htm#1)|林啟彥|香港浸會大學歷史系
研究札記|[辛亥年孫中山在倫敦化名攷](http://histweb.hkbu.edu.hk/contemporary/jourvol5_3/jourvol5_3main.htm#2)|李紓|南洋理工大學南洋商學院
新書介紹|[閱讀香港之一：新世紀出版香港專題史五種](http://histweb.hkbu.edu.hk/contemporary/jourvol5_3/jourvol5_3main.htm#3)|周佳榮|香港浸會大學歷史系
新書介紹|[郭振鐸、張笑梅主編《越南通史》評介](http://histweb.hkbu.edu.hk/contemporary/jourvol5_3/jourvol5_3main.htm#4)||香港浸會大學歷史系資料室
學界動向|[「21世紀世界與中國」學術研討會－－歷史文化、國學研究、中美關係簡介](http://histweb.hkbu.edu.hk/contemporary/jourvol5_3/jourvol5_3main.htm#5)||香港浸會大學歷史系資料室
學界動向|[第二屆近代中國海防國際研討會報導](http://histweb.hkbu.edu.hk/contemporary/jourvol5_3/jourvol5_3main.htm#6)||香港浸會大學歷史系資料室
學界動向|[香港學界活動簡報](http://histweb.hkbu.edu.hk/contemporary/jourvol5_3/jourvol5_3main.htm#7)||香港浸會大學歷史系資料室


#### 新書介紹
閱讀香港之一：新世紀出版香港專題史五種

書名|作者|頁數|出版日期|出版機構
---|---|---|---|---
《點滴話當年──香港供水一百五十年》|何佩然|250頁|2001年|香港：商務印書館
《光耀百年》|郭少棠|238頁|2001年|香港：中華電力有限公司
《道風百年──香港道教與道觀》|游子安主編|317頁|2002年|香港：蓬瀛仙館道教文化資料庫、利文出版社聯合出版
《香港土風舞歷史──一本屬於全香港土風舞界的書》|梁成安|175頁|2002年|香港：中華書局（發行）
《香港社會與文化史論集》|劉義章、黃文江合編|210頁|2002年|香港：香港中文大學聯合書


### Vol 4
[Vol. 5 No. 4 December 2002](http://histweb.hkbu.edu.hk/contemporary/jourvol5_4/Journal20.htm)

Category|Title|Author|University
---|---|---|---
專題演講|[孫中山的「文明調和論」與二十一世紀中國的外交格局](http://histweb.hkbu.edu.hk/contemporary/jourvol5_4/jourvol5_4main.htm#1)|周佳榮|香港浸會大學歷史系
專題演講|[濱下武志教授談亞洲僑匯問題](http://histweb.hkbu.edu.hk/contemporary/jourvol5_4/jourvol5_4main.htm#2)|鍾寶賢|香港浸會大學歷史學系
網絡介紹|[超越技術：網絡服務系統在歷史教學中的效應－－中山大學歷史系歷史教學網絡服務系統簡介](http://histweb.hkbu.edu.hk/contemporary/jourvol5_4/jourvol5_4main.htm#3)|趙立彬|中山大學歷史系
書刊介紹|[區志堅主編《馬鞍山風物誌》](http://histweb.hkbu.edu.hk/contemporary/jourvol5_4/jourvol5_4main.htm#4)||香港浸會大學歷史系資料室
書刊介紹|[湯泳詩《一個華南客家教會的研究－－從巴色會到香港崇真會》](http://histweb.hkbu.edu.hk/contemporary/jourvol5_4/jourvol5_4main.htm#5)||香港浸會大學歷史系資料室
書刊介紹|[華文史學新書資訊](http://histweb.hkbu.edu.hk/contemporary/jourvol5_4/jourvol5_4main.htm#6)||香港浸會大學歷史系資料室
書刊介紹|[《珠海學報》復刊號及新刊號目錄](http://histweb.hkbu.edu.hk/contemporary/jourvol5_4/jourvol5_4main.htm#7)||香港浸會大學歷史系資料室
書刊介紹|[近刊香港學界通訊簡介](http://histweb.hkbu.edu.hk/contemporary/jourvol5_4/jourvol5_4main.htm#8)||香港浸會大學歷史系資料室
史學新聞|[學人動向](http://histweb.hkbu.edu.hk/contemporary/jourvol5_4/jourvol5_4main.htm#9)||香港浸會大學歷史系資料室
史學新聞|[香港學界活動簡報](http://histweb.hkbu.edu.hk/contemporary/jourvol5_4/jourvol5_4main.htm#10)||香港浸會大學歷史系資料室


#### 書刊介紹
華文史學新書資訊

書名|作者|頁數|出版日期|出版機構
---|---|---|---|---
《自立與關懷──香港浸信教會百年史（1901-2001）》|李金強|252頁|2002年|香港：商務印書館
《利瑪竇入華及其他》|張錯|199頁|2002年|香港：香港城市大學出版社
《歷史地理》|香港城市大學中國文化中心編|192頁|2002年|香港：香港城市大學出版社
《香港日本文化協會四十周年特刊》|香港日本文化協會主編|233頁|2002年|香港：香港日本文化協會
《十八世紀禮學考證的思想活力──禮教論爭與禮秩重省》|張壽安|496頁|2001年|台北：中央研究院近代史研究所
《史學九章》|汪榮祖|367頁|2002年|台北：麥田出版
《商人精神的嬗變──近代中國商人觀念研究》|馬敏|337頁|2001年|武漢：華中師範大學出版社
《中間經濟：傳統與現代之間的中國近代手工業（1840-1936）》|彭南生|335頁|2002年|北京：高等教育出版社
《清代四大女詞人──轉型中的清代知識女性》|黃嫣梨|195頁|2002年|上海：漢語大詞典出版社
《揭開淇澳歷史之謎──1833年淇澳居民反侵略鬥爭研究文集》|楊水生、劉蜀永主編|240頁|2002年|北京：中央文獻出版社
《現階段的印尼華人族群》|廖建裕|138頁|2002年|新加坡：新加坡國立大學中文系、美國：八方文化企業公司聯合出版
《清季駐新加坡領事之探討（1877-1911）》|蔡佩蓉|214頁|2002年|新加坡：新加坡國立大學中文系、美國：八方文化企業公司


## Volume 6
### No 1
[Vol. 6 No. 1 March 2003](http://histweb.hkbu.edu.hk/contemporary/jourvol6_1/Journal21.htm)

Category|Title|Author|University
---|---|---|---
學術演講|[北京大學與歷史研究](http://histweb.hkbu.edu.hk/contemporary/jourvol6_1/jourvol6_1main.htm#1)|何芳川|北京大學亞太研究所
專題討論|[武則天的女性觀](http://histweb.hkbu.edu.hk/contemporary/jourvol6_1/jourvol6_1main.htm#2)|王雙懷|陝西師範大學歷史系
專題討論|[顧維鈞與辛亥革命](http://histweb.hkbu.edu.hk/contemporary/jourvol6_1/jourvol6_1main.htm#3)|沈潛|江蘇常熟高專人文系
教學探索|[在規範中理解學術－－"學術研討會"與中國現代史課程中的學術規範訓練](http://histweb.hkbu.edu.hk/contemporary/jourvol6_1/jourvol6_1main.htm#4)|趙立彬|中山大學歷史系副教授 香港浸會大學歷史系 2002-2003學年訪問學人
史學新聞|[香港浸會大學近代史研究中心2002年活動簡報](http://histweb.hkbu.edu.hk/contemporary/jourvol6_1/jourvol6_1main.htm#5)||香港浸會大學歷史系資料室
史學新聞|[香港浸會大學社會科學院研究支援網絡研討會](http://histweb.hkbu.edu.hk/contemporary/jourvol6_1/jourvol6_1main.htm#6)||香港浸會大學歷史系資料室
史學新聞|[香港學界活動簡報](http://histweb.hkbu.edu.hk/contemporary/jourvol6_1/jourvol6_1main.htm#7)||香港浸會大學歷史系資料室


### No 2
[Vol. 6 No. 2 September 2003](http://histweb.hkbu.edu.hk/contemporary/jourvol6_2/Jourvol22.htm)

Category|Title|Author|University
---|---|---|---
專題討論|西方的中國醫學史研究：成就、方法及展望|文樹德 (Paul U. Unschuld) 麥勁生譯|德國慕尼克大學醫學史研究所
新書介紹|從《明代華南農業地理研究》到《行會制度的近代命運》── 浸大歷史系訪問學人新著兩種介紹|周佳榮|香港浸會大學歷史系
新書介紹|香港出版史學書目提要||香港浸會大學歷史系資料室
史學新聞|“武漢青少年書畫攝影展”“五一”再亮台灣||武漢國民政府舊址紀念館提供
史學新聞|香港中國近代史學會通訊||香港中國近代史學會提供
史學新聞|香港近期展覽及講座||香港浸會大學歷史系資料室
史學新聞|《饒宗頤二十世紀學術文集》即將出版||香港浸會大學歷史系資料室
史學新聞|《近代中國基督教史研究集刊》第四期簡介||香港浸會大學歷史系資料室
史學新聞|《近代中國留學生國際學術研討會》海報||

![](http://histweb.hkbu.edu.hk/contemporary/jourvol6_2/Jourvol22.files/jourvol6_2main.files/jourvol6_2k.jpg)

#### 新書介紹

書名|作者|頁數|出版日期|出版機構|ISBN
---|---|---|---|---|---
《唐代歷史文化論稿》|王雙懷|451頁|2003年|香港：香港教育圖書公司|ISBN 962-948-14l-3
《北宋武將研究》|何冠環|590頁|2003年|香港：中華書局|ISBN 962-8820-71-0
《儒學傳統與思想變遷》|李焯然|256頁|2003年|香港：香港教育圖書公司|ISBN 962-948-142-1
《金宋史論叢》|陳學霖|276頁|2003年|香港：中文大學出版社|ISBN 962-996-097-4
《中國近代的國家與市場》|黎志剛|372頁|2003年|香港：香港教育圖書公司|ISBN 962-948-082-4


### No 3
[Vol. 6 No. 3 June 2004](http://histweb.hkbu.edu.hk/contemporary/jourvol6_3/jourvol23.htm)

Category|Title|Author|University
---|---|---|---
研究札記|羅香林擬作書目舉要|區志堅<br/>|香港理工大學香港專上學院<br/>香港大學中文系
新書介紹|考古與旅遊新書介紹|周佳榮|香港浸會大學歷史系
書評|評彭南生著：《行會制度的近代命運》|周子峰|香港浸會大學歷史學系 近代史研究中心
書評|香港百年歷史的折射──試評《香港中華總商會百年史》|丁潔|香港浸會大學歷史學系
學界動態|近代中國留學生國際學術研討會簡介||香港浸會大學歷史系資料室
學界動態|古蹟文化與旅遊開發研討會紀會||香港浸會大學歷史系資料室
學界動態|香港浸會大學近代史研究中心2003年活動簡報||香港浸會大學歷史系資料室
學府巡禮|中國內地開設歷史及相關學科的高等院校||香港浸會大學歷史系資料室
學府巡禮|《香港史家及歷史學研討會》海報||


#### 新書介紹
書名|作者|頁數|出版日期|出版機構
---|---|---|---|---
《東南考古研究》第三輯|鄧聰、吳春明主編|396頁|2003年12月|廈門：廈門大學出版社
《粵港考古與發現》|區家發|287頁|2004年|香港：三聯書店
《香港古蹟之旅──十八區文物徑研究彙編》|梁炳華|352頁|2004年|香港：中國歷史教育學會
《中國的世界文化與自然遺產》|羅哲文編|119頁|2003年|香港：和平圖書有限公司
《遍訪江山──中國文化之旅》|林翠芬|211頁|2004年|香港：水禾田製作室


### No 4
[Vol. 6 No. 4 September 2004](http://histweb.hkbu.edu.hk/contemporary/jourvol6_4/jourvol24.htm)

Category|Title|Author|University
---|---|---|---
教育史研究|[香港之中國教會大學史研究](http://histweb.hkbu.edu.hk/contemporary/jourvol6_4/jourvol6_4main.htm#1)|文兆堅|香港浸會大學歷史系
教育史研究|[中國教育史研究的新空間 ─《中國區域教育發展概論》評介](http://histweb.hkbu.edu.hk/contemporary/jourvol6_4/jourvol6_4main.htm#2)|甘穎軒|香港浸會大學歷史系
書刊介紹|[中國近現代出版史研究的再出發－簡介三種出版歷史料](http://histweb.hkbu.edu.hk/contemporary/jourvol6_4/jourvol6_4main.htm#3)|周佳榮|香港浸會大學歷史系
書刊介紹|[香港近刊中醫藥史著作評介](http://histweb.hkbu.edu.hk/contemporary/jourvol6_4/jourvol6_4main.htm#4)||香港浸會大學歷史系資料室
書刊介紹|[近期出版學刊消息](http://histweb.hkbu.edu.hk/contemporary/jourvol6_4/jourvol6_4main.htm#5)||香港浸會大學歷史系資料室
學界消息|[香港學界活動簡報（2004年上半年）](http://histweb.hkbu.edu.hk/contemporary/jourvol6_4/jourvol6_4main.htm#6)||香港浸會大學歷史系資料室
學界消息|[浸大歷史系活動簡報（2004年上半年）](http://histweb.hkbu.edu.hk/contemporary/jourvol6_4/jourvol6_4main.htm#7)||香港浸會大學歷史系資料室


## Volume 7
### No 1
[Vol. 7 No. 1 June 2005](http://histweb.hkbu.edu.hk/contemporary/jourvol7_1/jourvol25.htm)

Category|Title|Author|University
---|---|---|---
專題討論|[李氏朝鮮的醫女制度和著名醫女](http://histweb.hkbu.edu.hk/contemporary/jourvol7_1/jourvol7_1main.htm#1)|范永聰　區顯鋒|香港浸會大學歷史系
專題討論|[馬林在中國共產黨創建時期的功過](http://histweb.hkbu.edu.hk/contemporary/jourvol7_1/jourvol7_1main.htm#2)|徐莉君|武漢國民政府舊址紀念館
研究札記|[「蘇報案」論爭始末述評](http://histweb.hkbu.edu.hk/contemporary/jourvol7_1/jourvol7_1main.htm#3)|周佳榮|香港浸會大學歷史系
學界動向|[香港浸會大學近代史研究中心2004年活動簡報](http://histweb.hkbu.edu.hk/contemporary/jourvol7_1/jourvol7_1main.htm#4)||香港浸會大學歷史系資料室
學界動向|[香港學界近況（2004年下半年）](http://histweb.hkbu.edu.hk/contemporary/jourvol7_1/jourvol7_1main.htm#5)||香港浸會大學歷史系資料室


### No 2
[Vol. 7 No. 2 September 2005](http://histweb.hkbu.edu.hk/contemporary/jourvol7_2/jourvol26.htm)

Category|Title|Author|University
---|---|---|---
研究札記|[梁啟超研究的新開展－－從二十一世紀出發](http://histweb.hkbu.edu.hk/contemporary/jourvol7_2/jourvol7_2main.htm#1)|周佳榮|香港浸會大學歷史系
演講提要|[從檔案看清代女性－－以《刑科題本》為例](http://histweb.hkbu.edu.hk/contemporary/jourvol7_2/jourvol7_2main.htm#2)|定宜莊|中國社會科學院歷史研究所
會議報導|[香港史家與史學研討會紀要](http://histweb.hkbu.edu.hk/contemporary/jourvol7_2/jourvol7_2main.htm#3)|侯勵英<br/>區志堅|香港浸會大學歷史系<br/>香港理工大學香港專上學院
會議報導|[嚴復學術與思想研討會簡報](http://histweb.hkbu.edu.hk/contemporary/jourvol7_2/jourvol7_2main.htm#4)||香港浸會大學歷史系資料室
會議報導|[鄭和下西洋六百周年紀念國際學術研討會紀要](http://histweb.hkbu.edu.hk/contemporary/jourvol7_2/jourvol7_2main.htm#5)||香港浸會大學歷史系資料室
學界動向|[浸大近代史研究中心活動簡報（2005年上半年）](http://histweb.hkbu.edu.hk/contemporary/jourvol7_2/jourvol7_2main.htm#6)||香港浸會大學歷史系資料室
學界動向|[浸大歷史系學術活動匯報（2005年上半年）](http://histweb.hkbu.edu.hk/contemporary/jourvol7_2/jourvol7_2main.htm#7)||香港浸會大學歷史系資料室
學界動向|[香港學界近況（2005年上半年）](http://histweb.hkbu.edu.hk/contemporary/jourvol7_2/jourvol7_2main.htm#8)||香港浸會大學歷史系資料室
書刊資訊|[新世紀史學論著推介](http://histweb.hkbu.edu.hk/contemporary/jourvol7_2/jourvol7_2main.htm#9)||香港浸會大學歷史系資料室
書刊資訊|[《近代中國基督教史研究集刊》第六期目錄](http://histweb.hkbu.edu.hk/contemporary/jourvol7_2/jourvol7_2main.htm#10)|


### No 3
[Vol. 7 No. 3 March 2006](http://histweb.hkbu.edu.hk/contemporary/jourvol7_3/jourvol27.htm)

Category|Title|Author|University
---|---|---|---
史學傳承|[香港史家群像與史學新里程](http://histweb.hkbu.edu.hk/contemporary/jourvol7_3/jourvol7_3main.htm#1)|周佳榮|香港浸會大學歷史系
史學傳承|[新亞研究所師友雜記](http://histweb.hkbu.edu.hk/contemporary/jourvol7_3/jourvol7_3main.htm#2)|李金強|香港浸會大學歷史系
專題論文|[明代土木之變與朝鮮的對應](http://histweb.hkbu.edu.hk/contemporary/jourvol7_3/jourvol7_3main.htm#3)|范永聰|香港浸會大學歷史系
專題論文|[一案四史家──「朱延豐出國案」的考察](http://histweb.hkbu.edu.hk/contemporary/jourvol7_3/jourvol7_3main.htm#4)|梁晨|北京清華大學歷史系博士候選人<br/>香港浸會大學林思齊東西學術交流研究所駐校研究生(2005年9月-2006年1月)
書評|[評何國華著《陶行知教育學》](http://histweb.hkbu.edu.hk/contemporary/jourvol7_3/jourvol7_3main.htm#5)|甘穎軒|香港浸會大學歷史系
學界動向|[第四屆近代中國基督教史研討會紀要](http://histweb.hkbu.edu.hk/contemporary/jourvol7_3/jourvol7_3main.htm#6)||
學界動向|[浸大近代史研究中心學術匯報（2005年下半年）](http://histweb.hkbu.edu.hk/contemporary/jourvol7_3/jourvol7_3main.htm#7)||


### No 4
[Vol. 7 No. 4 September 2006](http://histweb.hkbu.edu.hk/contemporary/jourvol7_4/jourvol28.htm)

Category|Title|Author|University
---|---|---|---
研究札記|[香港報刊發展及研究述評](http://histweb.hkbu.edu.hk/contemporary/jourvol7_4/jourvol7_4main.htm#1)|周佳榮|香港浸會大學歷史系
書評|[一本由「商務人」撰寫的非一般「商務史」－－評李家駒著《商務印書館與近代知識文化的傳播》](http://histweb.hkbu.edu.hk/contemporary/jourvol7_4/jourvol7_4main.htm#2)|區志堅|香港理工大學香港專上學院
研究資訊|[香港近年出版中國近代史論文集綜合目錄](http://histweb.hkbu.edu.hk/contemporary/jourvol7_4/jourvol7_4main.htm#3)||香港浸會大學歷史系資料室
學界動向|[「陶行知與近代中國」研討會紀要](http://histweb.hkbu.edu.hk/contemporary/jourvol7_4/jourvol7_4main.htm#4)||香港浸會大學歷史系資料室
學界動向|[「轉型的中國」學術研討會摘要](http://histweb.hkbu.edu.hk/contemporary/jourvol7_4/jourvol7_4main.htm#5)||香港浸會大學歷史系資料室
學界動向|[浸大歷史系活動匯報（2005－2006年度）](http://histweb.hkbu.edu.hk/contemporary/jourvol7_4/jourvol7_4main.htm#6)||香港浸會大學歷史系資料室
學界動向|[香港學界活動匯報（2005－2006年度）](http://histweb.hkbu.edu.hk/contemporary/jourvol7_4/jourvol7_4main.htm#7)||香港浸會大學歷史系資料室
學界動向|[A Report of the International Symposium on University and Enlightenment: Historical Perspectives on Higher Education in China and the World](http://histweb.hkbu.edu.hk/contemporary/jourvol7_4/jourvol7_4main.htm#8)|Mak King Sang|






```bash
url='http://histweb.hkbu.edu.hk/contemporary/jourvol7_4/jourvol7_4menu.htm'

cat /tmp/menu | sed -r -n 's@[【】]@---@g;/^[[:blank:]]*$/d;p' | awk -v url="${url}" -v count=1 'BEGIN{printf("Category|Title|Author|University\n---|---|---|---\n")}{
    gsub("menu.","main.",url)
    if($1~/^---/){
        gsub("---","",$0)
        aa=$0
    }else{
        printf("%s|[%s](%s#%s)|\n",aa,$0,url,count)
        # printf("%s|%s|\n",aa,$0)
        # printf("%s#%s\n",url,count)
        count++
    }
}'
```


















## Change Log
* Mar 09, 2019 15:55 Sat -0500 EST
    * 初稿

[hkbu]:https://www.hkbu.edu.hk
[usc_cuhk]:http://www.usc.cuhk.edu.hk 'Universities Service Centre for China Studies'



<!-- End -->
