# Sinology Journal

Chinese Traditional Historiography

傳統中國史學相關學術期刊


論文、期刊平臺
* [questia][questia]
    * [Library of Academic and Scholarly Journals Online](https://www.questia.com/library/academic-journal-articles)

## Journal
* 當代史學 [Contemporary Historical Review](./journals/ContemporaryHistoricalReview.md)
* 東亞漢學研究 [The Sinological Research Society Of East Asia](./journals/TheSinologicalResearchSocietyOfEastAsia.md)
* [Historical Journal of Massachusetts](./journals/HistoricalJournalofMassachusetts.md)


<!-- 中央研究院近代史研究所 近代中國婦女史研究  http://www.mh.sinica.edu.tw/rwmch.aspx

http://www.mh.sinica.edu.tw/MHDocument/PublicationDetail/PublicationDetail_106.pdf



The Chinese Historical Review   中國歷史評論
https://tandfonline.com/toc/ytcr20/25/2?nav=tocList

Chinese Historical Review

http://www.chinesehistorians.org/chinese-historical-review/

>The Chinese Historical Review (ISSN 1547-402X) is a fully refereed and vigorously edited transnational journal of history and social sciences that is published biannually.

---
香港科技大學華南研究中心  http://schina.ust.hk/en

歷史人類學學刊
Journal of History and Anthropology
http://nansha.schina.ust.hk/Article_DB/en/journal
http://nansha.schina.ust.hk/Article_DB/zh/journal

田野與文獻   (提供pdf下載)
Fieldwork and Documents
http://nansha.schina.ust.hk/Article_DB/en/fieldwork-and-documents

http://nansha.schina.ust.hk/Article_DB/zh/fieldwork-and-documents -->




[questia]:https://www.questia.com "Online Research Library: Questia"


<!-- End -->
